import * as React from 'react';
import { View, Text, FlatList, Dimensions, StyleSheet, TouchableOpacity, Button, Image} from 'react-native';

const data = [
  {
    id: '3',
    name: 'Hồ Chí Minh',
    detail: 'Là một nhà cách mạng và chính khách người Việt Nam. Ông là người sáng lập Đảng Cộng sản Việt Nam, từng là Chủ tịch nước Việt Nam Dân chủ Cộng hoà từ 1945–1969, Thủ tướng Việt Nam Dân chủ Cộng hòa trong những năm 1945–1955, Tổng Bí thư Ban Chấp hành Trung ương Đảng Lao động Việt Nam từ 1956–1960, Chủ tịch Ban Chấp hành Trung ương Đảng Lao động Việt Nam từ năm 1951 cho đến khi qua đời....'
  }
];

const formatProduct = (data, numberColumn) => {
  const numberRows = Math.floor(data.length / numberColumn);

  let numberLastRow = data.length - (numberRows * numberColumn);
  while (numberLastRow !== numberColumn && numberLastRow !== 0) {
    data.push({ id: `blank-${numberLastRow}`, empty: true });
    numberLastRow++;
  }

  return data;
};

const numberColumn = 1;

export class Home2 extends React.Component {
  renderProduct = ({ item, index }) => {
    if (item.empty === true) {
      return <View style={[styles.item, styles.itemInvisible]} />;
    }
    return (
      <View
        style={styles.item}
      >
        <Image style={styles.image} source={require('./image/hcm.jpg')}/>
        <Text style={styles.textBtn}>{item.name}</Text>
        <Text style={styles.base}>{item.detail}</Text>
        <TouchableOpacity style={styles.btnAdd} title="Home3" onPress={() => this.props.navigation.navigate('Home3')}><Text style={styles.textBtn}>Next</Text></TouchableOpacity>
      </View>
    );
  };

  render() {
    return (
      <View style={styles.wraper}>
        <TouchableOpacity style={styles.btnVip} title="ADD VIP" onPress={() => this.props.navigation.navigate('Billing')}><Text style={styles.textBtn}>ADD VIP</Text></TouchableOpacity>
        <FlatList
          data={formatProduct(data, numberColumn)}
          style={styles.container}
          renderItem={this.renderProduct}
          numColumns={numberColumn}
        />
      </View>
        
    );
  }
}

const styles = StyleSheet.create({
  wraper: {
    width: '100%',
    height: '100%',
    backgroundColor: '#99CCFF',
  },
  container: {
    flex: 1,
    marginVertical: 20,
  },
  image: {
    height: 230,
    width: 180,
    margin: 15
  },
  base: {
    fontWeight: 'bold',
    fontSize: 17,
    lineHeight: 23
  },
  item: {
    backgroundColor: '#99CCFF',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    margin: 1,
    marginBottom: 1,
    height: '100%',
  },
  itemInvisible: {
    backgroundColor: 'transparent',
  },
  btnAdd: {
    marginTop: 45,
    padding: 15,
    width: 100,
    backgroundColor: '#004C99',
  },
  textBtn: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  btnVip1: {
    padding: 15,
    width: '100%',
    backgroundColor: '#D89460',
  },
  btnVip: {
    padding: 15,
    width: '100%',
    backgroundColor: '#004C99',
  }
});