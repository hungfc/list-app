import * as React from 'react';
import { View, Text, FlatList, Dimensions, StyleSheet, TouchableOpacity, Button, Image} from 'react-native';

const data = [
  {
    id: '1',
    name: 'Vladimir Putin',
    detail: 'Sinh ngày 7 tháng 10 năm 1952, là một chính trị gia người Nga và là cựu Thủ tướng của Liên bang Nga, là Tổng thống thứ hai của Nga từ 7 tháng 5 năm 2000 cho đến 7 tháng 5 năm 2008, là Tổng thống thứ tư của Nga từ 7 tháng 5 năm 2012...'
  }
];

const formatProduct = (data, numberColumn) => {
  const numberRows = Math.floor(data.length / numberColumn);

  let numberLastRow = data.length - (numberRows * numberColumn);
  while (numberLastRow !== numberColumn && numberLastRow !== 0) {
    data.push({ id: `blank-${numberLastRow}`, empty: true });
    numberLastRow++;
  }

  return data;
};

const numberColumn = 1;

export class Home extends React.Component {
  renderProduct = ({ item, index }) => {
    if (item.empty === true) {
      return <View style={[styles.item, styles.itemInvisible]} />;
    }
    return (
      <View
        style={styles.item}
      >
        <Image style={styles.image} source={require('./image/putin.jpg')}/>
        <Text style={styles.textBtn}>{item.name}</Text>
        <Text style={styles.base}>{item.detail}</Text>
        <TouchableOpacity style={styles.btnAdd} title="Home1" onPress={() => this.props.navigation.navigate('Home1')}><Text style={styles.textBtn}>Next</Text></TouchableOpacity>
      </View>
    );
  };

  render() {
    return (
      <View style={styles.wraper}>
        <TouchableOpacity style={styles.btnVip} title="ADD VIP" onPress={() => this.props.navigation.navigate('Billing')}><Text style={styles.textBtn}>ADD VIP</Text></TouchableOpacity>
        <FlatList
          data={formatProduct(data, numberColumn)}
          style={styles.container}
          renderItem={this.renderProduct}
          numColumns={numberColumn}
        />
      </View>
        
    );
  }
}

const styles = StyleSheet.create({
  wraper: {
    width: '100%',
    height: '100%',
    backgroundColor: '#99CCFF',
  },
  container: {
    flex: 1,
    marginVertical: 20,
  },
  image: {
    height: 230,
    width: 180,
    margin: 15
  },
  base: {
    fontWeight: 'bold',
    fontSize: 17,
    lineHeight: 23
  },
  item: {
    backgroundColor: '#99CCFF',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    margin: 1,
    marginBottom: 1,
    height: '100%',
  },
  itemInvisible: {
    backgroundColor: 'transparent',
  },
  btnAdd: {
    marginTop: 45,
    padding: 15,
    width: 100,
    backgroundColor: '#004C99',
  },
  textBtn: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  btnVip1: {
    padding: 15,
    width: '100%',
    backgroundColor: '#004C99',
  },
  btnVip: {
    padding: 15,
    width: '100%',
    backgroundColor: '#004C99',
  }
});