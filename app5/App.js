/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import type { Node } from 'react';

import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  FlatList
} from 'react-native';
import { ProductDetail, Billing } from './billing';
import { Home } from './home'
import { Home1 } from './home1'
import { Home2 } from './home2'
import { Home3 } from './home3'
import { Home4 } from './home4'

const Stack = createStackNavigator();
class App extends Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Home" options={{ title: 'Home' }} component={Home} />
          <Stack.Screen name="Home1" options={{ title: 'Home1' }} component={Home1} />
          <Stack.Screen name="Home2" options={{ title: 'Home2' }} component={Home2} />
          <Stack.Screen name="Home3" options={{ title: 'Home3' }} component={Home3} />
          <Stack.Screen name="Home4" options={{ title: 'Home4' }} component={Home4} />
          <Stack.Screen name="Billing" options={{ title: 'Billing' }} component={Billing} />
        </Stack.Navigator>
      </NavigationContainer>

    );
  }
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
