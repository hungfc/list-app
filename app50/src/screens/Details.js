import {Icon} from 'react-native-vector-icons/Ionicons';
import React from 'react';
import {View, Text, StyleSheet, Image, Button} from 'react-native';

export default function Details({route, navigation}) {
  return (
    <View style={{flex: 1, width: '100%'}}>
      <View
        style={[
          styles.imageHeaderView,
          {backgroundColor: route.params.details.bg_color},
        ]}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
        </View>
        <View
          style={{
            width: '100%',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Image
            source={route.params.details.image}
            style={{width: 260, height: 300}}
          />
        </View>
  
      </View>
      <View style={{height: '50%', backgroundColor: '#482E55'}}>
        <View
          style={{
            height: 350,
            backgroundColor: '#fff',
            borderBottomLeftRadius: 30,
            borderBottomRightRadius: 30,
            paddingHorizontal: 30,
            paddingTop: 30,
          }}>
          <View>
            <Text style={{fontSize: 25, fontWeight: 'bold'}}>
              {route.params.details.title}
            </Text>
            <View style={{flexDirection: 'row', marginVertical: 8}}>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              position: 'absolute',
              right: 30,
              top: 25,
            }}>
            <View style={styles.optionView}>
            </View>
            <View style={styles.optionView}>
            </View>
          </View>
          <Text style={{fontSize: 18, marginVertical: 10}}>
          The JP Headphone Executive headset is fitted with 2 soft ear cushions on both sides to help users feel comfortable, without pain in the ears when listening to music. Although not as modern as current products, but for customers who love simplicity, this product is a very interesting choice. Although not applying high technology with luxurious design as now, the sound emitted from this product is still appreciated.
          </Text>
        </View>

        <View
          style={{
            flexDirection: 'row',
            paddingHorizontal: 30,
            justifyContent: 'space-between',
            paddingTop: 25,
          }}>
          <Text style={{fontSize: 30, fontWeight: 'bold', color: '#fff'}}>
            {route.params.details.price}
          </Text>
          <View
            style={{
              height: 50,
              width: 120,
              backgroundColor: '#D16Eff',
              borderRadius: 20,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Button title="Add to Cart" color="#fff"/>
          </View>
        </View>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  imageHeaderView: {
    height: '50%',
    paddingHorizontal: 30,
    paddingTop: 40,
  },
  backBtnView: {
    height: 45,
    width: 45,
    borderRadius: 45 / 2,
    backgroundColor: '#414956',
    alignItems: 'center',
    justifyContent: 'center',
  },
  optionView: {
    height: 40,
    width: 40,
    borderRadius: 20,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.5,
    shadowRadius: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 5,
  },
});
