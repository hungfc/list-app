const learnMoreData = [
  {
    id: 'learnMore-1',
    title: 'Vinh Hạ Long',
    image: require('../images/boatbeach.png'),
  },
  {
    id: 'learnMore-2',
    title: 'Biển Vũng Tàu',
    image: require('../images/beach.png'),
  },
  {
    id: 'learnMore-3',
    title: 'Quảng Bình',
    image: require('../images/australia.png'),
  },
];

export default learnMoreData;
