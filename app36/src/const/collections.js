const collections = [
  {
    id: '1',
    name: 'Luna',
    image: require('../assets/collections/cyberPunk1.png'),
    price: '1.23',
    creator: 'Kenny',
    creatorImage: require('../assets/creators/creator1.jpg'),
  },
  {
    id: '2',
    name: 'Kira',
    image: require('../assets/collections/cyberPunk2.jpeg'),
    price: '1.03',
    creator: 'Lila',
    creatorImage: require('../assets/creators/creator2.jpg'),
  },
  {
    id: '3',
    name: 'Cyber',
    image: require('../assets/collections/cyberPunk3.jpeg'),
    price: '0.60',
    creator: 'Smith',
    creatorImage: require('../assets/creators/creator3.jpg'),
  },
  {
    id: '4',
    name: 'Mana',
    image: require('../assets/collections/cyberPunk4.jpeg'),
    price: '0.60',
    creator: 'David',
    creatorImage: require('../assets/creators/creator4.jpg'),
  },
  {
    id: '5',
    name: 'Jenny',
    image: require('../assets/collections/cyberPunk5.jpeg'),
    price: '0.78',
    creator: 'Kyo',
    creatorImage: require('../assets/creators/creator5.jpg'),
  },
];

export default collections;
