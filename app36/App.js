/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import { ThanhToan } from './billing';
import DetailsScreen from './src/views/screens/DetailsScreen';
import OnboardingScreen from './src/views/screens/OnboardingScreen';
import BottomNavigator from './src/views/navigators/BottomNavigator';

const Stack = createStackNavigator();
class App extends Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
        <Stack.Screen name="OnboardingScreen" component={OnboardingScreen} />
        <Stack.Screen name="HomeScreen" component={BottomNavigator} />
        <Stack.Screen name="DetailsScreen" component={DetailsScreen} />
          <Stack.Screen name="ThanhToan" options={{ title: 'ThanhToan' }} component={ThanhToan} />
        </Stack.Navigator>
      </NavigationContainer>

    );
  }
};

export default App;
