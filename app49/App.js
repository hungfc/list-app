import * as React from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from './Screens/HomeScreen';
import DetailScreen from './Screens/DetailScreen';
import {ThanhToan} from './billing/index';

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" options={{headerShown:false}} component={HomeScreen} />
        <Stack.Screen name="Detail" options={{headerShown:false}} component={DetailScreen} />
        <Stack.Screen name="ThanhToan" options={{headerShown:false}} component={ThanhToan} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;