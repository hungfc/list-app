import React from 'react';
import { View, Text, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
    export default function DetailScreen({ navigation }) {
        return (
            <View style={styles.container}>
                    <View style={{flexDirection:"row",padding:15,}}>
                        <View style={{flex:1,flexDirection:"column",justifyContent:"center"}}>
                        <Text onPress={() => {
                            navigation.navigate("ThanhToan");
                        }}>ADD VIP</Text>
                        </View>

                        <View style={{flex:1,flexDirection:"row",justifyContent:"space-between",alignContent:"center",alignItems:"center"}}>
                        <Icon name="bell-o" size={30}/>
                        <View style={{backgroundColor:"#dadee0",borderRadius:100,padding:10,alignSelf:"center"}}><Text style={{
                          
                            color:"#000",textAlign:"center"}}>Jenny</Text></View>
                        </View>
                    </View>
                    <Text style={{fontSize:24, padding:10,fontWeight: '700'}}>Description:</Text>
                    <Text style={{fontSize:18, padding:10}}>- Meeting room management is easy for everyone – global traffic lights really don't have room for explanation. Getting a meeting room is easy with two clicks or just use RFID or NFC to make a reservation.</Text>
                    <Text style={{fontSize:18, padding:10}}>- Make the most of your office time and space. Say goodbye to “stolen” rooms, interrupted meetings, and empty rooms due to lack of people. Say hello to a smoother workday with better meetings and increased productivity!</Text>
                    <Text style={{fontSize:18, padding:10}}>- Free up that extra time from your booking and make room available to coworkers by selecting “finish early”. Just two quick clicks and you are out of the room.</Text>
                    <Text style={{fontSize:18, padding:10}}>- Brand the room manager by adding your own company logo to the display. Change the color scheme to match your company profile.</Text>
                    <Text style={{fontSize:18, padding:10}}>- Need more time for your meeting? Don't worry, just select “extend meeting” and the room is yours for a while.</Text>
            </View>
        );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f2efe4',
     
    },

    members_textstyle:{
        fontSize:19,
        marginRight:5,
    },
    members_viewStyle:{
        flexDirection:"row",alignItems:"center"
    },

    btnStyles:{
        backgroundColor:"green",
        borderRadius:50,
        paddingHorizontal:40,

    }
});