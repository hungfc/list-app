import React, { Component,useState} from 'react';
import { View, Text, StyleSheet,ScrollView ,Image} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Octicons from 'react-native-vector-icons/Octicons';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Entypo from 'react-native-vector-icons/Entypo';
import { Button } from 'react-native-paper';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { useNavigation } from "@react-navigation/native";

ClubhouseRooms=()=>{

   const userDataChe=[
       {
          topic:"Personal Growth",
          title:"Personal Growth",
          img1:"https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8cGVyc29ufGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
          img2:"https://images.unsplash.com/flagged/photo-1570612861542-284f4c12e75f?ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8cGVyc29ufGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
          member1:"Baria",
          member2:"Kumar",
          member3:"Patel",
          total:"24",
          online:"9"
       },
       {
        topic:"Love is the Dogs",
        title:"Group of dog lovers",
        img1:"https://images.unsplash.com/photo-1529626455594-4ff0802cfb7e?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTI2fHxwZXJzb258ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        img2:"https://images.unsplash.com/photo-1488861859915-4b5a5e57649f?ixid=MnwxMjA3fDB8MHxzZWFyY2h8Nzl8fGluZGlhbiUyMHBlcnNvbnxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        member1:"Anna",
        member2:"Akital",
        member3:"Rohan",
        total:"48",
        online:"20"
     },
       {
        topic:"Bia CLub",
        title:"Group of people who like to drink beer",
        img1:"https://images.unsplash.com/photo-1500048993953-d23a436266cf?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTF8fHBlcnNvbnxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        img2:"https://images.unsplash.com/photo-1569124589354-615739ae007b?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MzJ8fHBlcnNvbnxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        member1:"Anna",
        member2:"Bhumi",
        member3:"Agarwal",
        total:"89",
        online:"58"
     },
     {
        topic:"Movies",
        title:"Group of people who like to watch movies",
        img1:"https://images.unsplash.com/photo-1561910775-891020e392e7?ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8aW5kaWFuJTIwcGVyc29ufGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        img2:"https://images.unsplash.com/photo-1599842057874-37393e9342df?ixid=MnwxMjA3fDB8MHxzZWFyY2h8Nnx8aW5kaWFuJTIwcGVyc29ufGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        member1:"Anna vala",
        member2:"Smit Anna",
        member3:"Sunil Anna",
        total:"78",
        online:"12"
     },
     {
        topic:"Fitness",
        title:"Fitness workout at home",
        img1:"https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixid=MnwxMjA3fDB8MHxzZWFyY2h8ODV8fHBlcnNvbnxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        img2:"https://images.unsplash.com/photo-1607346256330-dee7af15f7c5?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTI0fHxwZXJzb258ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        member1:"Pagi",
        member2:"Shivani",
        member3:"Raj",
        total:"123",
        online:"58"
     },
   ];

return userDataChe.map(data=>{

    return(
        <View style={{elevation:2,flexDirection:"column",backgroundColor:"#fff",marginBottom:20,marginHorizontal:15,borderRadius:15}} >
        <View style={{padding:10}}>
        <View style={{flexDirection:"row"}}>
        <Text style={{fontSize:14,marginRight:8,textTransform:"uppercase"}}>{data.topic}</Text>
        </View>


        <View style={{flexDirection:"row"}}>
        <Text style={{fontSize:17,fontWeight:"bold",marginRight:8,}}>{data.title}</Text> 
        </View>
        </View>


        <View style={{flexDirection:"row",justifyContent:"center",paddingBottom:10}}>
           <View style={{flexDirection:"column",marginRight:18}}>
           <Image style={{height:60,borderColor:"red",width:55,marginLeft:10,borderRadius:21}} 
                source={{uri:data.img1}}/>
          
           <Image style={{height:60,width:60,marginLeft:40,marginTop:-30,borderRadius:21}}
            source={{uri:data.img2}}/>
        </View>

           <View style={{flexDirection:"column",marginHorizontal:0}}>
                    <View style={styles.members_viewStyle}>
                        <Text style={styles.members_textstyle}>{data.member1}</Text>
                        <AntDesign color="grey" name="message1" size={14}/>
                    </View>

                    <View style={styles.members_viewStyle}>
                        <Text style={styles.members_textstyle}>{data.member2}</Text>
                        <AntDesign color="grey" name="message1" size={14}/>
                    </View>

                    <View style={styles.members_viewStyle}>
                        <Text style={styles.members_textstyle}>{data.member3}</Text>
                        <AntDesign color="grey" name="message1" size={14}/>
                    </View>

                        <View style={{flexDirection:"row",alignItems:"center",marginTop:8}}>
                            <Text style={{fontSize:16,fontWeight:"normal",color:"grey",marginRight:5}}>{data.total}</Text>
                                <Ionicons name="person" size={14} color="grey"/>
                                <Text style={{fontSize:16,marginLeft:5,color:"grey"}}>/</Text>

                                <Text style={{fontSize:16,fontWeight:"normal",color:"grey",marginLeft:5}}>{data.online}</Text>
                                <AntDesign style={{marginLeft:5}} color="grey" name="message1" size={14}/>
                        </View>
                </View>
         
        </View>

    </View>
    )
})

}

// create a component
// class HomeScreen extends Component {
    export default function HomeScreen({ navigation }) {
    
    // render() {
        // const navigation = useNavigation();
        return (
            <View style={styles.container}>
                    <View style={{flexDirection:"row",padding:15,}}>
                        <View style={{flex:1,flexDirection:"column",justifyContent:"center"}}>
                        <Text onPress={() => {
                            navigation.navigate("ThanhToan");
                        }}>ADD VIP</Text>
                        </View>

                        <View style={{flex:1,flexDirection:"row",justifyContent:"space-between",alignContent:"center",alignItems:"center"}}>
                        <Icon name="bell-o" size={30}/>
                        <View style={{backgroundColor:"#dadee0",borderRadius:100,padding:10,alignSelf:"center"}}><Text style={{
                          
                            color:"#000",textAlign:"center"}}>Jenny</Text></View>
                        </View>
                    </View>


                                <ScrollView>
                              <ClubhouseRooms/>
                                </ScrollView>



                                <View style={{marginBottom:15,backgroundColor: 'transparent',marginHorizontal:20,flexDirection:"row",justifyContent:"flex-end",alignItems:"center"}}>
                                <View style={{flexDirection:"column"}}></View>
                                <View style={{flexDirection:"column",marginRight:20}}>
                                <Button uppercase={false} onPress={() => {
                            navigation.navigate("Detail");
                        }} style={{ backgroundColor:"#05c46b",
                                            borderRadius:50,
                                            paddingVertical:5,
                                        
                                            marginBottom:0,
                                                paddingHorizontal:20,}}
                                            icon="plus" mode="contained">
                                    <Text style={{fontSize:18}}>About Manager Room</Text>
                                </Button>
                                </View>
                                <View style={{flexDirection:"column"}}></View>
                                </View>


            </View>
        );
    // }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f2efe4',
     
    },

    members_textstyle:{
        fontSize:19,
        marginRight:5,
    },
    members_viewStyle:{
        flexDirection:"row",alignItems:"center"
    },

    btnStyles:{
        backgroundColor:"green",
        borderRadius:50,
        paddingHorizontal:40,

    }
});

// export default HomeScreen;