import {
  Alert,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import RNIap, {
  InAppPurchase,
  PurchaseError,
  SubscriptionPurchase,
  acknowledgePurchaseAndroid,
  consumePurchaseAndroid,
  finishTransaction,
  finishTransactionIOS,
  purchaseErrorListener,
  purchaseUpdatedListener,
  
} from 'react-native-iap';
import React, { Component } from 'react';

import Button from 'apsl-react-native-button';

// App Bundle > com.dooboolab.test

const itemSkus = [
  'com.freezebook.nipyxlplxi.1',
  'com.freezebook.nipyxlplxi.2',
  'com.freezebook.nipyxlplxi.3',
  'com.freezebook.nipyxlplxi.4',
  'com.freezebook.nipyxlplxi.5',
  'com.freezebook.nipyxlplxi.6',
];

const itemSubs = [
  'com.freezebook.nipyxlplxi.sub.1', // subscription
  'com.freezebook.nipyxlplxi.sub.2',
  'com.freezebook.nipyxlplxi.sub.3',
  'com.freezebook.nipyxlplxi.sub.4',
  'com.freezebook.nipyxlplxi.sub.5',
];

let purchaseUpdateSubscription;
let purchaseErrorSubscription;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: Platform.select({
      ios: 0,
      android: 4,
    }),
    paddingTop: Platform.select({
      ios: 0,
      android: 4,
    }),
    backgroundColor: 'white',
  },
  header: {
    flex: 20,
    flexDirection: 'row',
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerTxt: {
    fontSize: 26,
    color: 'green',
  },
  content: {
    flex: 80,
    flexDirection: 'column',
    justifyContent: 'center',
    alignSelf: 'stretch',
    alignItems: 'center',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  btn: {
    height: 48,
    width: 240,
    alignSelf: 'center',
    backgroundColor: '#00c40f',
    borderRadius: 0,
    borderWidth: 0,
  },
  txt: {
    fontSize: 16,
    color: 'white',
  },
});

export class Billing extends Component {
  constructor(props) {
    super(props);

    this.state = {
      unconsumed:null,
      productList: [],
      productSubList: [],
      receipt: '',
      availableItemsMessage: '',
    };
  }

  async componentDidMount() {
    try {
      await RNIap.initConnection();
      await RNIap.flushFailedPurchasesCachedAsPendingAndroid();
    } catch (err) {
      console.warn(err.code, err.message);
    }

    purchaseUpdateSubscription = purchaseUpdatedListener(
      async (purchase) => {
        console.info('purchase', purchase);
        const receipt = purchase.transactionReceipt
          ? purchase.transactionReceipt
          : purchase.originalJson;
        console.info(receipt);
        if (receipt) {
          try {
            const ackResult = await finishTransaction(purchase,true);
            console.info('ackResult', ackResult);
          } catch (ackErr) {
            console.warn('ackErr', ackErr);
          }

          this.setState({ receipt }, () => this.goNext());
        }
      },
    );

    purchaseErrorSubscription = purchaseErrorListener(
      (error) => {
        console.log('purchaseErrorListener', error);
        Alert.alert('purchase error', JSON.stringify(error));
      },
    );
    this.getItems();
    this.getSubscriptions();
  }

  componentWillUnmount() {
    if (purchaseUpdateSubscription) {
      purchaseUpdateSubscription.remove();
      purchaseUpdateSubscription = null;
    }
    if (purchaseErrorSubscription) {
      purchaseErrorSubscription.remove();
      purchaseErrorSubscription = null;
    }
    RNIap.endConnection();
  }

  goNext = async() => {

    Alert.alert('Thanh toán thành công. Receipt: ', this.state.receipt);
  };

  getItems = async () => {
    try {
      const products = await RNIap.getProducts(itemSkus);
      // const products = await RNIap.getSubscriptions(itemSkus);
      console.log('Products', products);
      this.setState({ productList: products });
    } catch (err) {
      console.warn(err.code, err.message);
    }
  };

  getSubscriptions = async () => {
    try {
      const products = await RNIap.getSubscriptions(itemSubs);
      console.log('Products sub', products);
      this.setState({ productSubList: products });
    } catch (err) {
      console.warn(err.code, err.message);
    }
  };

  getAvailablePurchases = async () => {
    try {
      console.log('init')
      console.info(
        'Get available purchases (non-consumable or unconsumed consumable)',
      );
      const purchases = await RNIap.getAvailablePurchases();
      console.info('Available purchases :: ', purchases);
      //Alert.alert('Available purchases :: ', JSON.stringify(purchases))
      
      if (purchases && purchases.length > 0) {
        this.consumePurchaseAndroid(purchases);
        // this.setState({
        //   unconsumed: purchases,
        // });
      }
    } catch (err) {
      console.warn(err.code, err.message);
      Alert.alert(err.message);
    }
  };

  async consumePurchaseAndroid(purchases){
    for(var i=0;i<purchases.length;i++){
      var pur=purchases[i];
      await RNIap.finishTransaction(pur,true);
    }
      
  }

  // Version 3 apis
  requestPurchase = async (sku) => {
    try {
      var s = await RNIap.requestPurchase(sku, false);
      console.log(s)
    } catch (err) {
      console.warn(err.code, err.message);
    }
  };

  requestSubscription = async (sku) => {
    try {
      RNIap.requestSubscription(sku);
    } catch (err) {
      Alert.alert(err.message);
    }
  };

  render() {
    const { productList, receipt, availableItemsMessage, productSubList } = this.state;
    const receipt100 = receipt.substring(0, 100);

    return (
      <View style={styles.container}>
        {/* <View style={styles.header}>
            <Text style={styles.headerTxt}>react-native-iap V3</Text>
          </View> */}
        <View style={styles.content}>
          <ScrollView style={{ alignSelf: 'stretch' }}>
            {/* <Button
                onPress={this.getAvailablePurchases}
                activeOpacity={0.5}
                style={styles.btn}
                textStyle={styles.txt}>
                Get available purchases
              </Button> */}
            {/* <Button
                onPress={()=> this.getItems()}
                activeOpacity={0.5}
                style={styles.btn}
                textStyle={styles.txt}>
                Get Products ({productList.length})
              </Button> */}
            {/* <Text style={{margin: 5, fontSize: 15, alignSelf: 'center'}}>
                {availableItemsMessage}
              </Text>
  
              <Text style={{margin: 5, fontSize: 9, alignSelf: 'center'}}>
                {receipt100}
              </Text> */}

            {productList && productList.length > 0 ? <Text style={{ textAlign: "center", fontSize: 16, fontWeight: 'bold' }}>List products</Text> : null}
            {productList.map((product, i) => {
              return (
                <View
                  key={i}
                  style={{
                    flexDirection: 'column',
                    backgroundColor: '#cecece',
                    borderBottomWidth: 0.7,
                    margin: 4
                  }}>
                  <Text
                    style={{
                      fontSize: 16,
                      color: 'black',
                      alignSelf: 'center',
                      paddingHorizontal: 0,
                      marginVertical: 10
                    }}>
                    {/* {JSON.stringify(product)} */}
                    {"Product " +  (i +1)+ " :"} {product.originalPriceAndroid} {product.currency}
                  </Text>
                  <Button
                    // onPress={()=> this.requestPurchase(product.productId)}
                    onPress={() =>
                      this.requestPurchase(product.productId)
                    }
                    activeOpacity={0.5}
                    style={styles.btn}
                    textStyle={styles.txt}>
                    Buy product
                  </Button>
                </View>
              );
            })}
            {productSubList && productSubList.length > 0 ? <Text style={{ textAlign: "center", fontSize: 16, fontWeight: 'bold' }}>Subscriptions</Text> : null}
            {productSubList.map((product, i) => {
              return (
                <View
                  key={i}
                  style={{
                    flexDirection: 'column',
                    backgroundColor: '#cecece',
                    borderBottomWidth: 0.7,
                    margin: 4
                  }}>
                  <Text
                    style={{
                      fontSize: 16,
                      color: 'black',
                      alignSelf: 'center',
                      paddingHorizontal: 0,
                      marginVertical: 10
                    }}>
                    {/* {JSON.stringify(product)} */}
                    {"Sub package " + (i +1)+ " :"} {product.originalPriceAndroid} {product.currency}
                  </Text>
                  <Button
                    // onPress={()=> this.requestPurchase(product.productId)}
                    onPress={() =>
                      this.requestSubscription(product.productId)
                    }
                    activeOpacity={0.5}
                    style={styles.btn}
                    textStyle={styles.txt}>
                    Buy sub
                  </Button>
                </View>
              );
            })}
          </ScrollView>
        </View>
      </View>
    );
  }
}
