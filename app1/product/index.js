import React from 'react';
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Image,
    Button
} from 'react-native';
import { TouchableHighlight } from 'react-native-gesture-handler';
const bookImage=require("../image/book.jpg");
Number.prototype.format = function(n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};
export class Product extends React.Component {
    // constructor(props){
    //     this.su
    // }
    capFirst(string) {
        if(!string){
            string ="";
        }
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
    
    getRandomInt(min, max) {
          return Math.floor(Math.random() * (max - min)) + min;
    }
    
    generateName(){
        var name1=['abandoned',"able","absolute","adorable","adventurous","academic","acceptable","acclaimed","accomplished","accurate"];
        var name2=["people","history","way","art","world","information","map","family","government","health","system","computer"];
                
        var name = this.capFirst(name1[this.getRandomInt(0, name1.length + 1)]) + ' ' + this.capFirst(name2[this.getRandomInt(0, name2.length + 1)]);
        return name;
    
    }
    randomIntFromInterval() { // min and max included 
        return Math.floor(Math.random() * 100)
    }
    render() {
        return (<View style={{ margin: 4, width: '46%' }}>
            <Image style={{ width: '98%', height: 100, }}
                resizeMode={"contain"}
                source={bookImage}>
            </Image>
            <Text>{this.generateName()}</Text>
            <Text style={{ color: 'green' }}>Price: {this.randomIntFromInterval().format()} USD</Text>
            <Button onPress={() => {
                this.props.navigation.navigate('Billing')
            }} title={"Buy"}>

            </Button>
        </View>)
    }
}
