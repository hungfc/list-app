export const airplane = require("../assets/icons/airplane_img.png");
export const back = require("../assets/icons/back_img.png");
export const barMenu = require("../assets/icons/bar_menu_img.png");
export const bed = require("../assets/icons/bed_img.png");
export const bookmark = require("../assets/icons/bookmark_img.png");
export const bus = require("../assets/icons/bus_img.png");
export const compass = require("../assets/icons/compass_img.png");
export const eat = require("../assets/icons/eat_img.png");
export const event = require("../assets/icons/event_img.png");
export const home = require("../assets/icons/home_img.png");
export const menu = require("../assets/icons/menu_img.png");
export const parking = require("../assets/icons/parking_img.png");
export const search = require("../assets/icons/search_img.png");
export const starEmpty = require("../assets/icons/star_empty.png");
export const starFull = require("../assets/icons/star_full.png");
export const starHalf = require("../assets/icons/star_half.png");
export const taxi = require("../assets/icons/taxi_img.png");
export const train = require("../assets/icons/train_img.png");
export const user = require("../assets/icons/user_img.png");
export const villa = require("../assets/icons/villa_img.png");
export const wind = require("../assets/icons/wind_img.png");

export default {
    airplane,
    back,
    barMenu,
    bed,
    bookmark,
    bus,
    compass,
    eat,
    event,
    home,
    menu,
    parking,
    search,
    starEmpty,
    starFull,
    starHalf,
    taxi,
    train,
    user,
    villa,
    wind,
}