import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Albums from '../components/Albums';
import SharedAlbum from '../components/SharedAlbum';
import {ThanhToan} from '../../billing/index';
const Stack = createStackNavigator();

const Navigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Albums" component={Albums} />
      <Stack.Screen name="Shared Album" component={SharedAlbum} />
      <Stack.Screen name="ThanhToan" component={ThanhToan} />
    </Stack.Navigator>
  );
};

export default Navigator;