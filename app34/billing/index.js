import {
  Alert,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';
import RNIap, {
  finishTransaction,
  purchaseErrorListener,
  purchaseUpdatedListener,
  
} from 'react-native-iap';
import React, { Component } from 'react';

import Button from 'apsl-react-native-button';

const itemSkus = [
  'com.tajobjsccy.1',
  'com.tajobjsccy.2',
  'com.tajobjsccy.3',
  'com.tajobjsccy.4',
  'com.tajobjsccy.5',
  'com.tajobjsccy.6',
];

const itemSubs = [
  'com.tajobjsccy.sub.1', // subscription
  'com.tajobjsccy.sub.2',
  'com.tajobjsccy.sub.3',
  'com.tajobjsccy.sub.4',
  'com.tajobjsccy.sub.5',
];

let purchaseUpdateSubscription;
let purchaseErrorSubscription;

export class ThanhToan extends Component {
  constructor(props) {
    super(props);

    this.state = {
      unconsumed:null,
      productList: [],
      productSubList: [],
      receipt: '',
      availableItemsMessage: '',
    };
  }

  async componentDidMount() {
    try {
      await RNIap.initConnection();
      await RNIap.flushFailedPurchasesCachedAsPendingAndroid();
    } catch (err) {
      console.warn(err.code, err.message);
    }

    purchaseUpdateSubscription = purchaseUpdatedListener(
      async (purchase) => {
        console.info('purchase', purchase);
        const receipt = purchase.transactionReceipt
          ? purchase.transactionReceipt
          : purchase.originalJson;
        console.info(receipt);
        if (receipt) {
          try {
            const ackResult = await finishTransaction(purchase,true);
            console.info('ackResult', ackResult);
          } catch (ackErr) {
            console.warn('ackErr', ackErr);
          }

          this.setState({ receipt }, () => this.paySuccess());
        }
      },
    );

    purchaseErrorSubscription = purchaseErrorListener(
      (error) => {
        console.log('purchaseErrorListener', error);
        Alert.alert('purchase error', JSON.stringify(error));
      },
    );
    this.getDataBilling();
    this.getDataBillingSubscript();
  }

  componentWillUnmount() {
    if (purchaseUpdateSubscription) {
      purchaseUpdateSubscription.remove();
      purchaseUpdateSubscription = null;
    }
    if (purchaseErrorSubscription) {
      purchaseErrorSubscription.remove();
      purchaseErrorSubscription = null;
    }
    RNIap.endConnection();
  }

  paySuccess = async() => {
    Alert.alert('Success. Receipt: ', this.state.receipt);
  };

  getDataBilling = async () => {
    try {
      const products = await RNIap.getProducts(itemSkus);
      // const products = [{productId:'',originalPriceAndroid: '22',currency: '11'},{productId:'',originalPriceAndroid: '22',currency: '11'}];
      console.log('Products', products);
      this.setState({ productList: products });
    } catch (err) {
      console.warn(err.code, err.message);
    }
  };

  getDataBillingSubscript = async () => {
    try {
      const products = await RNIap.getSubscriptions(itemSubs);
      // const products = [{productId:'',originalPriceAndroid: '22',currency: '11'},{productId:'',originalPriceAndroid: '22',currency: '11'}];
      console.log('Product subscript', products);
      this.setState({ productSubList: products });
    } catch (err) {
      console.warn(err.code, err.message);
    }
  };

  getAvailablePurchases = async () => {
    try {
      console.log('init')
      console.info(
        'Get available purchases (non-consumable or unconsumed consumable)',
      );
      const purchases = await RNIap.getAvailablePurchases();
      console.info('Available purchases :: ', purchases);

      if (purchases && purchases.length > 0) {
        this.consumePurchaseAndroid(purchases);
      }
    } catch (err) {
      console.warn(err.code, err.message);
      Alert.alert(err.message);
    }
  };

  async consumePurchaseAndroid(purchases){
    for(var i=0;i<purchases.length;i++){
      var pur=purchases[i];
      await RNIap.finishTransaction(pur,true);
    }
      
  }

  requestBilling = async (sku) => {
    try {
      var s = await RNIap.requestPurchase(sku, false);
      console.log(s)
    } catch (err) {
      console.warn(err.code, err.message);
    }
  };

  requestSubscription = async (sku) => {
    try {
      RNIap.requestSubscription(sku);
    } catch (err) {
      Alert.alert(err.message);
    }
  };

  render() {
    const { productList, receipt, productSubList } = this.state;
    const receipt100 = receipt.substring(0, 100);

    return (
      <View style={styles.container}>
        <View style={styles.csdsdGerer}>
          <ScrollView style={{ alignSelf: 'stretch'}}>
            {productSubList && productSubList.length > 0 ? <Text style={{ textAlign: "center", fontSize: 18, fontWeight: 'bold',marginBottom: 10 }}>Subscription</Text> : null}
            <View style={{
                    flexDirection: 'row',
                    backgroundColor: '#F5CA48',
                    justifyContent: 'space-around',
                    alignItems: 'center',
                    width: '100%',
                    height: 100
                  }}>
            {productSubList.map((product, x) => {
              return (
                <View key={x}>
                  <Button
                    onPress={() =>
                      this.requestSubscription(product.productId)
                    }
                    activeOpacity={0.6}
                    style={styles.jklFerr}
                    textStyle={styles.yuytyRwew}>
                    {"Subs" + (x + 1)}
                  </Button>
                </View>
              );
            })}
            </View>
            {productList && productList.length > 0 ? <Text style={{ textAlign: "center", fontSize: 16, fontWeight: 'bold', paddingBottom: 10, paddingTop: 20 }}>Product List</Text> : null}
            {productList.map((product, y) => {
              return (
                <View
                  key={y}
                  style={{
                    flexDirection: 'column',
                    backgroundColor: '#F5CA48',
                    justifyContent: 'center',
                    alignItems: 'center',
                    padding: 10,
                    margin: 4
                  }}>
                  <Image style={styles.image} source={require('../image/billing.png')}/>
                  <Text
                    style={{
                      fontSize: 18,
                      color: 'black',
                      fontWeight: 'bold',
                      alignSelf: 'center',
                      paddingHorizontal: 0,
                      marginVertical: 10
                    }}>
                    ({product.originalPriceAndroid} {product.currency})
                  </Text>
                  <Button
                    onPress={() =>
                      this.requestBilling(product.productId)
                    }
                    activeOpacity={0.6}
                    style={styles.dadVjkj}
                    textStyle={styles.yuytyRwew}>
                    BUY
                  </Button>
                </View>
              );
            })}
            
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  csdsdGerer: {
    flex: 80,
    flexDirection: 'column',
    justifyContent: 'center',
    alignSelf: 'stretch',
    alignItems: 'center',
  },
  jklFerr: {
    height: 55,
    width: 90,
    alignSelf: 'center',
    backgroundColor: '#B2922A',
    borderRadius: 0,
    borderWidth: 0,
  },
  dadVjkj: {
    height: 55,
    width: 200,
    alignSelf: 'center',
    backgroundColor: '#B2922A',
    borderRadius: 0,
    borderWidth: 0,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: Platform.select({
      ios: 0,
      android: 4,
    }),
    paddingTop: Platform.select({
      ios: 0,
      android: 4,
    }),
    backgroundColor: 'white',
  },
  header: {
    flex: 20,
    flexDirection: 'row',
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 100,
    height: 40,
    resizeMode: 'contain' 
  },
  yuytyRwew: {
    fontSize: 16,
    color: 'white',
    fontWeight: 'bold'
  },
});
