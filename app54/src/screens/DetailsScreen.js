import React, { useState } from 'react';
import { StyleSheet, View, ImageBackground, Image, Text, TouchableOpacity } from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign'
const DetailsScreen = (props) => {
    const [isCast, setIsCast] = useState(false);

    return (
        <View style={styles.container} >

            <ImageBackground style={{ height: "100%", justifyContent: "flex-end", }} source={require("../assets/main9.jpg")}>
                <View style={{
                    height: "55%",
                    borderTopLeftRadius: 24.0,
                    borderTopRightRadius: 24.0,
                    backgroundColor: "#262626",
                }}>
                    <AntDesign color="white" size={50} name="playcircleo" style={{ alignSelf: "flex-end", right: 24.0, bottom: "7%", position: "relative" }} />
                    <View style={{ flexDirection: "row", justifyContent: "space-between", marginRight: 12.0 }}>
                        <Text style={{ color: "white", fontWeight: "700", marginLeft: 12.0, fontSize: 20 }}>Description</Text>
                    </View>
                    <Text style={{ color: "grey", fontWeight: "700", marginLeft: 12.0, fontSize: 14, marginRight: 20.0, marginTop: 12.0 }}>Bách Luyện Thành Tiên - Nếu bạn đã từng đọc Phàm Nhân Tu Tiên thì chắc hẳn bạn sẽ không lạ gì với mô típ tu luyện của một phàm nhân bước trên con đường tiên đạo đầy gập ghềnh. Lâm Hiên trong tác phẩm là một người như vậy, không sở hữu linh căn cũng đồng nghĩa với việc vô duyên với tiên đạo nhưng Lâm Hiên là một người có tính cách quật cường, cố chấp!</Text>
                    <View style={{ flexDirection: "row" }}>
                        <TouchableOpacity onPress={() => {
                            setIsCast(false);
                        }}>
                            <View>
                                <Text style={isCast == false ? styles.selectedText : styles.unselectedText}>Author</Text>
                                {isCast == false
                                    ?
                                    <View style={{ alignSelf: "center", top: 6.2, backgroundColor: "#08E280", width: 40, height: 2, marginLeft: 12.0 }}>

                                    </View> : <View></View>

                                }
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => {
                            setIsCast(true);
                        }}>
                            <View>
                                <Text style={isCast ? styles.selectedText : styles.unselectedText}>Description</Text>
                                {isCast
                                    ?
                                    <View style={{ alignSelf: "center", top: 6.2, backgroundColor: "#08E280", width: 40, height: 2 }}>

                                    </View> : <View></View>

                                }
                            </View>
                        </TouchableOpacity>

                    </View>
                    <View style={{ marginTop: 24.0, marginHorizontal: 12.0, flexDirection: "row", justifyContent: "space-between" }}>
                        <Image style={styles.bottomImage} source={require("../assets/4.jpg")} />
                        <Image style={styles.bottomImage} source={require("../assets/5.jpg")} />
                        <Image style={styles.bottomImage} source={require("../assets/6.jpg")} />

                    </View>
                </View>
            </ImageBackground>
        </View>
    );
}
const styles = StyleSheet.create({

    selectedText: { color: "white", fontWeight: "700", marginLeft: 12.0, fontSize: 18.0, marginTop: 12.0 },
    unselectedText: { color: "grey", fontWeight: "700", marginLeft: 12.0, fontSize: 18.0, marginTop: 12.0 },
    bottomImage: { height: 130, width: 100, borderRadius: 12 }

});

export default DetailsScreen;