import React from 'react';
import { Image, StyleSheet, Text, View, ScrollView, TouchableOpacity } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons'

const HomeScreen = ({props, navigation}) => {

    return (
        <View style={{ flex: 1, backgroundColor: "#262626" }}>
            <View>
                <View style={[styles.row, { marginTop: 12.0, marginHorizontal: 12.0 }]}>
                    <Ionicons name="md-filter-outline" color="#08E280" size={35} style={{ top: 6 }} />
                    <Text style={[styles.bottomDiscoverText, { color: "#08E280", fontWeight: "700" }]}>HTstory</Text>
                    <Text style={{color: "#08E280", fontWeight: "700"}} onPress={() => navigation.navigate('ThanhToan')}>ADD VIP</Text>
                </View>
                <Image style={{ height: "40%", width: "95%", borderRadius: 12.0 }} source={require("../assets/main.jpg")} />
                <View style={[styles.row, { marginTop: 12.0, marginHorizontal: 12.0 }]}>
                    <Text style={[styles.bottomDiscoverText, { color: "white", fontWeight: "700", marginLeft: 0.0, fontSize: 20 }]}>Continue Reading</Text>
                    <Text style={[styles.bottomDiscoverText, { color: "grey", fontWeight: "700", fontSize: 14, top: 6.0 }]}>See All</Text>
                </View>
                <ScrollView horizontal showsHorizontalScrollIndicator={false} style={{ height: 100 }}>
                    <TouchableOpacity activeOpacity={0.5} onPress={() => {
                        navigation.navigate("Details");
                    }}>
                        <View>
                            <Image style={{ height: 120, width: 250, margin: 12.0, borderRadius: 12.0 }} source={require("../assets/main1.jpeg")} />
                            <View style={{ flexDirection: "row", width: "50%" }}>
                                <Text style={[styles.bottomDiscoverText, { color: "white", fontWeight: "700", marginLeft: 12.0, fontSize: 14 }]}>Hero</Text>
                                <Text style={[styles.bottomDiscoverText, { color: "#08E280", fontSize: 12, top: 2 }]}>Ep 2</Text>

                            </View>
                        </View>
                    </TouchableOpacity>

                    <View>
                        <Image style={{ height: 120, width: 250, margin: 12.0, borderRadius: 12.0 }} source={require("../assets/main2.jpeg")} />
                        <View style={{ flexDirection: "row", width: "50%" }}>
                            <Text style={[styles.bottomDiscoverText, { color: "white", fontWeight: "700", marginLeft: 12.0, fontSize: 14 }]}>Akira</Text>
                            <Text style={[styles.bottomDiscoverText, { color: "#08E280", fontSize: 12, top: 2 }]}>Adin</Text>

                        </View>
                    </View>
                </ScrollView>
            </View>
            <View style={[styles.row, { marginTop: 12.0, marginHorizontal: 12.0 }]}>
                <Text style={[styles.bottomDiscoverText, { color: "white", fontWeight: "700", marginLeft: 0.0, fontSize: 20 }]}>Trending</Text>
                <Text style={[styles.bottomDiscoverText, { color: "grey", fontWeight: "700", fontSize: 14, top: 6.0 }]}>SeeAll</Text>
            </View>
            <View style={[styles.row, { marginTop: 12.0, marginHorizontal: 12.0 }]}>
                <Image style={styles.bottomImage} source={require("../assets/main3.jpeg")} />
                <Image style={styles.bottomImage} source={require("../assets/main4.jpeg")} />
                <Image style={styles.bottomImage} source={require("../assets/main5.jpeg")} />

            </View>


        </View>

    );
}

const styles = StyleSheet.create({

    row: {
        flexDirection: "row",
        justifyContent: "space-between",
    },
    bottomDiscoverText: {
        color: "#59616A",
        fontSize: 30,
        marginLeft: 17.0,
    },
    bottomImage: { height: 130, width: 100, borderRadius: 12 }

});

export default HomeScreen;
