import * as React from 'react';
import { View, Text, FlatList, StyleSheet, Image, Button, TouchableOpacity,} from 'react-native';

const data = [
  {
    id: '1',
    name: 'NY Yankees',
    img: require('./image/image-1.jpg'),
    color: 'Black',
    size: 'S',
    price: '5$',
    material: 'Cotton'
  },
  {
      id: '2',
      name: 'VOGUE',
      img: require('./image/image-2.jpg'),
      color: '1600x900',
      size: 'M',
      price: '2$',
      material: 'Cotton'
  },
  {
      id: '3',
      name: 'LEVIS',
      img: require('./image/image-3.jpg'),
      color: 'White',
      size: 'S',
      price: '1$',
      material: 'Cotton'
  },
  {
      id: '4',
      name: 'NIKE',
      img: require('./image/image-4.jpg'),
      color: 'Grey',
      size: 'L',
      price: '5$',
      material: 'Cotton'
  },
  {
      id: '5',
      name: 'VOGUE BLACK',
      img: require('./image/image-5.jpg'),
      color: 'Blue',
      size: 'XL',
      price: '4$',
      material: 'Cotton'
  },
  {
      id: '6',
      name: 'Torano',
      img: require('./image/image-7.jpg'),
      color: 'Black',
      size: 'S',
      price: '5$',
      material: 'Cotton'
  },
  {
      id: '7',
      name: 'Aud',
      img: require('./image/image-7.jpg'),
      color: 'White',
      size: 'M',
      price: '3$',
      material: 'Cotton'
  },
  {
      id: '8',
      name: 'Lambor',
      img: require('./image/image-8.jpg'),
      color: 'White',
      size: 'XXL',
      price: '2$',
      material: 'Cotton'
  },
  {
      id: '9',
      name: 'GAP',
      img: require('./image/image-9.jpg'),
      color: 'Black',
      size: 'L',
      price: '6$',
      material: 'Cotton'
  },
  {
      id: '10',
      name: 'Conver',
      img: require('./image/image-10.jpg'),
      color: 'Black',
      size: 'M',
      price: '2$',
      material: 'Cotton'
  },
  {
      id: '11',
      name: 'Batman',
      img: require('./image/image-11.jpg'),
      color: 'Black',
      size: 'XL',
      price: '3$',
      material: 'Cotton'
  },
  {
      id: '12',
      name: 'Kenzo',
      img: require('./image/image-12.jpg'),
      color: 'Black',
      size: 'L',
      price: '1$',
      material: 'Cotton'
  },
];

const clearData = (data, numColumns) => {
  const numberOfFullRows = Math.floor(data.length / numColumns);

  let numberElementsLastRow = data.length - (numberOfFullRows * numColumns);
  while (numberElementsLastRow !== numColumns && numberElementsLastRow !== 0) {
    data.push({ id: `blank-${numberElementsLastRow}`, empty: true });
    numberElementsLastRow++;
  }

  return data;
};

const numColumns = 2;

export class Home extends React.Component {
  renderItem = ({ item, index }) => {
    if (item.empty === true) {
      return <View style={[styles.item, styles.invisibleItem]} />;
    }
    return (
      <View
        style={styles.item}
      >
        <Image style={styles.img} source={item.img}/>
        <Text numberOfLines={1} style={styles.textItem}>{item.name}</Text>
        <Text style={{ color: 'green', marginBottom: 2 }}>Price: {item.price}</Text>
        <View style={[{ width: "60%", margin: 10, backgroundColor: "red" }]}>
          <Button title="Detail" color="#009999" onPress={() => this.props.navigation.navigate('Detail',{pItem:item})}/>
        </View>
      </View>
    );
  };

  render() {
    return (
      <View style={styles.wrap}>
        <TouchableOpacity style={styles.btnAdd} title="UP VIP" onPress={() => this.props.navigation.navigate('Billing')}><Text style={styles.btnText}>UP VIP</Text></TouchableOpacity>
        <FlatList
          data={clearData(data, numColumns)}
          style={styles.container}
          renderItem={this.renderItem}
          numColumns={numColumns}
        />
      </View>
        
    );
  }
}

const styles = StyleSheet.create({
  wrap: {
    width: '100%',
    height: '100%'
  },
  container: {
    flex: 1,
    marginVertical: 20,
  },
  img: {
    width: '90%',
    height: '70%'
  },
  item: {
    backgroundColor: '#CCFFFF',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    margin: 1,
    marginBottom: 1,
    height: 400,
  },
  invisibleItem: {
    backgroundColor: 'transparent',
  },
  btnText: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  textItem: {
    color: '#000',
    fontWeight: 'bold',
    marginBottom: 6,
    marginTop: 5
  },
  btnAdd: {
    padding: 15,
    backgroundColor: '#009999',
  }
});