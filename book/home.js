
      /**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';

import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
  Text,
  useColorScheme,
  View,
  FlatList,
  Button
} from 'react-native';
import {Product} from './product';
import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
// import Button from 'apsl-react-native-button';
export class Home extends Component {
  render(){
    return (
      
        <View style={{width:'100%',height:'100%'}}>
          <Button 
          activeOpacity={0.5}
          style={[styles.btn,{width:'100%'}]}
          onPress={()=>{
            this.props.navigation.navigate('Billing')
          }}
          title="Billing">
                
          </Button>
        <FlatList
          style={{}}
          numColumns={2}
          columnWrapperStyle={{
            flex: 1,
            justifyContent: 'center',
          }}
          data={[{},{},{},{},{},{},{},{},{},{},{},{}]}
          renderItem={(item)=>{
            return(<Product navigation={this.props.navigation} item={item}></Product>)
          }}
          keyExtractor={item => item.id}
      />
        </View>
    );
  }
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
  btn: {
    height: 35,
    alignSelf: 'center',
    backgroundColor: '#00c40f',
    borderRadius: 0,
    borderWidth: 0,
  },
  txt: {
    fontSize:19,
  },
});

