import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    View,
    Button,
    TextInput,
    Alert
} from 'react-native';

export class CheckTime extends Component {

    state = {
        myTextInput: '',
        users: ['HN-HCM 10:39AM','HN-DN 7:45PM','HCM-DL 4:25PM','DN-DL 9:25PM']
    }

    onChangeDataInput = (event) => {
        this.setState({
            myTextInput:event
        })
    }

    onAddNewUser = () => {
        this.setState(prevState => {
            return {
                myTextInput: '',
                users: [prevState.myTextInput, ...prevState.users]
            }
        })

        Alert.alert('Thêm Thành Công')
    }
    render() {
        return (
            <View style={styles.inputssWraper}>
                <Button 
                activeOpacity={0.5}
                color='#5B2C6F'
                style={[styles.btn,{width:'100%', marginBottom: 50}]}
                onPress={()=>{
                this.props.navigation.navigate('Billing')
                }}
                title="MUA VIP">
                </Button>
                <View style={styles.formStyleWrap}>
                    <TextInput
                        value={this.state.myTextInput}
                        style={styles.input}
                        onChangeText={this.onChangeDataInput}
                    />
                    <TouchableOpacity style={[{ width: "30%", margin: 10, padding: 14, backgroundColor: '#5B2C6F' }]}
                        title="Add Time"
                        onPress={this.onAddNewUser}
                    ><Text style={styles.textssBtn}>Add Time</Text>
                    </TouchableOpacity>
                </View>
                {
                    this.state.users.map( item => (
                        <Text style={styles.users} key={item}>{item}</Text>
                    ))
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    inputssWraper: {
      width: '100%',
      backgroundColor:'#D2B4DE',
      height: '100%'
    },
    formStyleWrap: {
        display: 'flex',
        width: '100%',
        height: 100,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    input: {
        fontSize:20,
        padding: 10,
        borderWidth: 1,
        width: '60%',
        backgroundColor: '#fff'
    },
    users: {
        padding: 10,
        marginBottom:10,
        backgroundColor: '#fff',
        borderWidth: 1,
        fontSize: 30,
        borderColor:'#cecece'
    },
    textssBtn: {
      color: 'white',
      fontSize: 18,
      fontWeight: 'bold',
      textAlign: 'center'
    },
    btnAdd: {
      padding: 15,
      backgroundColor: '#5B2C6F',
    },
    btn: {
        width: '100%'
    }
  });
