
      /**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
  Image
} from 'react-native';
export class Home extends Component {
  render(){
    return (
        <View style={{width:'100%',height:'100%',justifyContent:'space-between', backgroundColor: '#D2B4DE', alignItems: 'center'}}>
          <TouchableOpacity style={styles.btnssThem} title="ADD VIP" onPress={() => this.props.navigation.navigate('ThanhToan')}><Text style={styles.textssButton}>MUA VIP</Text></TouchableOpacity>
          <Image style={styles.image} source={require('./image/tauhoa.png')}/>
          <TouchableOpacity style={styles.btnssThem} title="XEM GIỜ" onPress={() => this.props.navigation.navigate('CheckTime')}><Text style={styles.textssButton}>XEM GIỜ</Text></TouchableOpacity>
        </View>
    );
  }
};
       
const styles = StyleSheet.create({
  btn: {
    height: 35,
    width: '100%',
    alignSelf: 'center',
    backgroundColor: '#00c40f',
    borderRadius: 0,
    borderWidth: 0,
  },
  txt: {
    fontSize:19,
  },
  image: {
    width: 330,
    height: 330
  },
  textssButton: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  btnssThem: {
    padding: 15,
    width: '100%',
    backgroundColor: '#5B2C6F',
  }
});