
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';

import {
    StyleSheet,
    TouchableOpacity,
    Text,
    Image,
    View,
} from 'react-native';

export class Detail extends Component {
    render() {
        const{pItem} =this.props.route.params;
        return (
            <View style={{width: '100%', height: 250}}>
              <View style={styles.layoutWrap}>
                <View style={{width: '40%'}}>
                  <Image style={{width: '100%', height: 250}} source={pItem.img}/>
                </View>
                <View style={{width: '60%', paddingLeft: 10}}>
                  <Text style={styles.textBase} numberOfLines={2}>Name T shirt: {pItem.name}</Text>
                  <Text style={styles.textBase}>Color: {pItem.color}</Text>
                  <Text style={styles.textBase}>Size: {pItem.size}</Text>
                  <Text style={styles.textBase}>Material: {pItem.material}</Text>
                </View>
              </View>
              <TouchableOpacity style={styles.btntxtAdd} title="ADD VIP" onPress={() => this.props.navigation.navigate('Billing')}><Text style={styles.btnText}>BUY T SHIRT</Text></TouchableOpacity>
            </View>
        );
    }
};

const styles = StyleSheet.create({
    textBase: {
      fontWeight: 'bold',
      fontSize: 18,
    },
    layoutWrap: {
      flexDirection: 'row',
      paddingRight:10,
      paddingLeft: 10,
      paddingTop: 10
    },
    btnText: {
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center'
      },
      btntxtAdd: {
        padding: 15,
        backgroundColor: '#3492ef',
        width: '90%',
        alignSelf: 'center',
        marginTop: 20
      }
});