import * as React from 'react';
import { View, Text, FlatList, Dimensions, StyleSheet, TouchableOpacity, Button, Image} from 'react-native';

const data = [
  {
    id: '3',
    name: 'Zinédine Zidane',
    detail: 'Là cựu cầu thủ và là huấn luyện viên bóng đá người Pháp gốc Algérie. Zidane từng cùng Đội tuyển bóng đá quốc gia Pháp lên ngôi vô địch FIFA World Cup lần đầu tiên năm 1998 và ngôi vô địch UEFA Euro năm 2000. Ở cấp CLB, ông tỏa sáng trong màu áo của Juventus và Real Madrid, giúp 2 câu lạc bộ này giành nhiều danh hiệu ở châu Âu cũng như quốc gia....'
  }
];

const convertData = (data, numberColumn) => {
  const numberRows = Math.floor(data.length / numberColumn);

  let numberLastRow = data.length - (numberRows * numberColumn);
  while (numberLastRow !== numberColumn && numberLastRow !== 0) {
    data.push({ id: `blank-${numberLastRow}`, empty: true });
    numberLastRow++;
  }

  return data;
};

const numberColumn = 1;

export class Zidane extends React.Component {
  getDataProduct = ({ item, index }) => {
    if (item.empty === true) {
      return <View style={[styles.item, styles.itemHidden]} />;
    }
    return (
      <View
        style={styles.item}
      >
        <Image style={styles.image} source={require('./image/zidane.jpg')}/>
        <Text style={styles.textBtn}>{item.name}</Text>
        <Text style={styles.base}>{item.detail}</Text>
        <TouchableOpacity style={styles.btntxtAdd} title="Rooney" onPress={() => this.props.navigation.navigate('Rooney')}><Text style={styles.textBtn}>Next</Text></TouchableOpacity>
      </View>
    );
  };

  render() {
    return (
      <View style={styles.wraperDiv}>
        <TouchableOpacity style={styles.btnVip} title="ADD VIP" onPress={() => this.props.navigation.navigate('Billing')}><Text style={styles.textBtn}>ADD VIP</Text></TouchableOpacity>
        <FlatList
          data={convertData(data, numberColumn)}
          style={styles.container}
          renderItem={this.getDataProduct}
          numColumns={numberColumn}
        />
      </View>
        
    );
  }
}

const styles = StyleSheet.create({
  wraperDiv: {
    width: '100%',
    height: '100%',
    backgroundColor: '#F1948A',
  },
  container: {
    flex: 1,
    marginVertical: 20,
  },
  image: {
    height: 230,
    width: 180,
    margin: 15
  },
  base: {
    fontWeight: 'bold',
    fontSize: 17,
    lineHeight: 23
  },
  item: {
    backgroundColor: '#F1948A',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    margin: 1,
    marginBottom: 1,
    height: '100%',
  },
  itemHidden: {
    backgroundColor: 'transparent',
  },
  btntxtAdd: {
    marginTop: 45,
    padding: 15,
    width: 100,
    backgroundColor: '#D35400',
  },
  textBtn: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  textbtnVip1: {
    padding: 15,
    width: '100%',
    backgroundColor: '#D89460',
  },
  btnVip: {
    padding: 15,
    width: '100%',
    backgroundColor: '#D35400',
  }
});