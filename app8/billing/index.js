import {
  Alert,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';
import RNIap, {
  finishTransaction,
  purchaseErrorListener,
  purchaseUpdatedListener,
} from 'react-native-iap';
import React, { Component } from 'react';

import Button from 'apsl-react-native-button';

const itemSkus = [
  'com.jrurqyhddk.1',
  'com.jrurqyhddk.2',
  'com.jrurqyhddk.3',
  'com.jrurqyhddk.4',
  'com.jrurqyhddk.5',
  'com.jrurqyhddk.6',
];

const itemSubs = [
  'com.jrurqyhddk.sub.1', // subs
  'com.jrurqyhddk.sub.2',
  'com.jrurqyhddk.sub.3',
  'com.jrurqyhddk.sub.4',
  'com.jrurqyhddk.sub.5',
];

let purchaseUpdateSubscription;
let purchaseErrorSubscription;
export class Billing extends Component {
  constructor(props) {
    super(props);

    this.state = {
      unconsumed:null,
      productList: [],
      productSubList: [],
      receipt: '',
      availableItemsMessage: '',
    };
  }

  async componentDidMount() {
    try {
      await RNIap.initConnection();
      await RNIap.flushFailedPurchasesCachedAsPendingAndroid();
    } catch (err) {
      console.warn(err.code, err.message);
    }

    purchaseUpdateSubscription = purchaseUpdatedListener(
      async (purchase) => {
        console.info('purchase', purchase);
        const receipt = purchase.transactionReceipt
          ? purchase.transactionReceipt
          : purchase.originalJson;
        console.info(receipt);
        if (receipt) {
          try {
            const ackResult = await finishTransaction(purchase,true);
            console.info('ackResult', ackResult);
          } catch (ackErr) {
            console.warn('ackErr', ackErr);
          }

          this.setState({ receipt }, () => this.payThanhCong());
        }
      },
    );

    purchaseErrorSubscription = purchaseErrorListener(
      (error) => {
        console.log('purchaseErrorListener', error);
        Alert.alert('purchase error', JSON.stringify(error));
      },
    );
    this.getThanhToan();
    this.getThanhToanSubs();
  }

  componentWillUnmount() {
    if (purchaseUpdateSubscription) {
      purchaseUpdateSubscription.remove();
      purchaseUpdateSubscription = null;
    }
    if (purchaseErrorSubscription) {
      purchaseErrorSubscription.remove();
      purchaseErrorSubscription = null;
    }
    RNIap.endConnection();
  }

  payThanhCong = async() => {
    Alert.alert('Succes. Receipt: ', this.state.receipt);
  };

  getThanhToan = async () => {
    try {
      const products = await RNIap.getProducts(itemSkus);
      // const products = [{productId:'',originalPriceAndroid: '22',currency: '11'},{productId:'',originalPriceAndroid: '22',currency: '11'}];
      console.log('Products', products);
      this.setState({ productList: products });
    } catch (err) {
      console.warn(err.code, err.message);
    }
  };

  getThanhToanSubs = async () => {
    try {
      const products = await RNIap.getSubscriptions(itemSubs);
      // const products = [{productId:'',originalPriceAndroid: '22',currency: '11'},{productId:'',originalPriceAndroid: '22',currency: '11'}];
      this.setState({ productSubList: products });
    } catch (err) {
      console.warn(err.code, err.message);
    }
  };

  getPurchases = async () => {
    try {
      console.info(
        'Get available purchases (non-consumable or unconsumed consumable)',
      );
      const purchases = await RNIap.getPurchases();
      console.info('Available purchases :: ', purchases);

      if (purchases && purchases.length > 0) {
        this.consumePurchaseAndroid(purchases);
      }
    } catch (err) {
      Alert.alert(err.message);
    }
  };

  async consumePurchaseAndroid(purchases){
    for(var i=0;i<purchases.length;i++){
      var pur=purchases[i];
      await RNIap.finishTransaction(pur,true);
    }
      
  }

  requestBilling = async (sku) => {
    try {
      var s = await RNIap.requestPurchase(sku, false);
    } catch (err) {
      console.warn(err.code, err.message);
    }
  };

  requestSubscription = async (sku) => {
    try {
      RNIap.requestSubscription(sku);
    } catch (err) {
      Alert.alert(err.message);
    }
  };

  render() {
    const { productList, receipt, productSubList } = this.state;
    const receipt100 = receipt.substring(0, 100);

    return (
      <View style={styles.container}>
        <View style={styles.detailContent}>
          <ScrollView style={{ alignSelf: 'stretch'}}>
            {productSubList && productSubList.length > 0 ? <Text style={{ textAlign: "center", fontSize: 18, fontWeight: 'bold',marginBottom: 10 }}>Subscription</Text> : null}
            <View style={{
                    alignItems: 'center',
                    width: '100%',
                    height: 100,
                    flexDirection: 'row',
                    backgroundColor: '#F1948A',
                    justifyContent: 'space-around'
                  }}>
            {productSubList.map((product, x) => {
              return (
                <View key={x}>
                  <Button
                    onPress={() =>
                      this.requestSubscription(product.productId)
                    }
                    activeOpacity={0.6}
                    style={styles.productBtn1}
                    textStyle={styles.textStyles}>
                    {"Subs" + (x + 1)}
                  </Button>
                </View>
              );
            })}
            </View>
            {productList && productList.length > 0 ? <Text style={{ textAlign: "center", fontSize: 16, fontWeight: 'bold', paddingBottom: 10, paddingTop: 20 }}>Product List</Text> : null}
            {productList.map((product, y) => {
              return (
                <View
                  key={y}
                  style={{
                    flexDirection: 'column',
                    backgroundColor: '#F1948A',
                    justifyContent: 'center',
                    alignItems: 'center',
                    padding: 10,
                    margin: 4
                  }}>
                  <Image style={styles.image} source={require('../image/icon.png')}/>
                  <Text
                    style={{
                      fontSize: 18,
                      color: 'black',
                      fontWeight: 'bold',
                      alignSelf: 'center',
                      paddingHorizontal: 0,
                      marginVertical: 10
                    }}>
                    ({product.originalPriceAndroid} {product.currency})
                  </Text>
                  <Button
                    onPress={() =>
                      this.requestBilling(product.productId)
                    }
                    activeOpacity={0.6}
                    style={styles.productBtn}
                    textStyle={styles.textStyles}>
                    BUY
                  </Button>
                </View>
              );
            })}
            
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: Platform.select({
      ios: 0,
      android: 4,
    }),
    paddingTop: Platform.select({
      ios: 0,
      android: 4,
    }),
    backgroundColor: 'white',
  },
  header: {
    flex: 20,
    flexDirection: 'row',
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 100,
    height: 40,
    resizeMode: 'contain' 
  },
  detailContent: {
    flex: 80,
    flexDirection: 'column',
    justifyContent: 'center',
    alignSelf: 'stretch',
    alignItems: 'center',
  },
  productBtn1: {
    height: 55,
    width: 90,
    alignSelf: 'center',
    backgroundColor: '#D35400',
    borderRadius: 0,
    borderWidth: 0,
  },
  productBtn: {
    height: 55,
    width: 200,
    alignSelf: 'center',
    backgroundColor: '#D35400',
    borderRadius: 0,
    borderWidth: 0,
  },
  textStyles: {
    fontSize: 16,
    color: 'white',
    fontWeight: 'bold'
  },
});