import * as React from 'react';
import { View, Text, FlatList, Dimensions, StyleSheet, TouchableOpacity, Button, Image} from 'react-native';

const data = [
  {
    id: '2',
    name: 'Ronaldinho',
    detail: 'Là một cựu cầu thủ bóng đá chuyên nghiệp người Brasil chơi ở vị trí tiền đạo & tiền vệ tấn công. Được coi là một trong những cầu thủ xuất sắc nhất trong thế hệ của mình và được nhiều người coi là một trong những cầu thủ vĩ đại nhất mọi thời đại, Ronaldinho đã giành được 2 giải Cầu thủ xuất sắc nhất năm của FIFA và 1 Quả bóng vàng...'
  }
];

const convertData = (data, numberColumn) => {
  const numberRows = Math.floor(data.length / numberColumn);

  let numberLastRow = data.length - (numberRows * numberColumn);
  while (numberLastRow !== numberColumn && numberLastRow !== 0) {
    data.push({ id: `blank-${numberLastRow}`, empty: true });
    numberLastRow++;
  }

  return data;
};

const numberColumn = 1;

export class Ronaldinho extends React.Component {
  getDataProduct = ({ item, index }) => {
    if (item.empty === true) {
      return <View style={[styles.item, styles.itemHidden]} />;
    }
    return (
      <View
        style={styles.item}
      >
        <Image style={styles.image} source={require('./image/ronaldinho.jpg')}/>
        <Text style={styles.textBtn}>{item.name}</Text>
        <Text style={styles.base}>{item.detail}</Text>
        <TouchableOpacity style={styles.btntxtAdd} title="Ronaldo" onPress={() => this.props.navigation.navigate('Ronaldo')}><Text style={styles.textBtn}>Next</Text></TouchableOpacity>
      </View>
    );
  };

  render() {
    return (
      <View style={styles.wraperDiv}>
        <TouchableOpacity style={styles.btnVip} title="ADD VIP" onPress={() => this.props.navigation.navigate('Billing')}><Text style={styles.textBtn}>ADD VIP</Text></TouchableOpacity>
        <FlatList
          data={convertData(data, numberColumn)}
          style={styles.container}
          renderItem={this.getDataProduct}
          numColumns={numberColumn}
        />
      </View>
        
    );
  }
}

const styles = StyleSheet.create({
  wraperDiv: {
    width: '100%',
    height: '100%',
    backgroundColor: '#F1948A',
  },
  container: {
    flex: 1,
    marginVertical: 20,
  },
  image: {
    height: 230,
    width: 180,
    margin: 15
  },
  base: {
    fontWeight: 'bold',
    fontSize: 17,
    lineHeight: 23
  },
  item: {
    backgroundColor: '#F1948A',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    margin: 1,
    marginBottom: 1,
    height: '100%',
  },
  itemHidden: {
    backgroundColor: 'transparent',
  },
  btntxtAdd: {
    marginTop: 45,
    padding: 15,
    width: 100,
    backgroundColor: '#D35400',
  },
  textBtn: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  textbtnVip1: {
    padding: 15,
    width: '100%',
    backgroundColor: '#D89460',
  },
  btnVip: {
    padding: 15,
    width: '100%',
    backgroundColor: '#D35400',
  }
});