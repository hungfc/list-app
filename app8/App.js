/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import type { Node } from 'react';

import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  FlatList
} from 'react-native';
import { ProductDetail, Billing } from './billing';
import { Messi } from './messi'
import { Ronaldinho } from './ronaldinho'
import { Ronaldo } from './ronaldo'
import { Zidane } from './zidane'
import { Rooney } from './rooney'

const Stack = createStackNavigator();
class App extends Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Messi" options={{ title: 'Messi' }} component={Messi} />
          <Stack.Screen name="Ronaldinho" options={{ title: 'Ronaldinho' }} component={Ronaldinho} />
          <Stack.Screen name="Ronaldo" options={{ title: 'Ronaldo' }} component={Ronaldo} />
          <Stack.Screen name="Zidane" options={{ title: 'Zidane' }} component={Zidane} />
          <Stack.Screen name="Rooney" options={{ title: 'Rooney' }} component={Rooney} />
          <Stack.Screen name="Billing" options={{ title: 'Billing' }} component={Billing} />
        </Stack.Navigator>
      </NavigationContainer>

    );
  }
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
