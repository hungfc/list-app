import { StyleSheet } from "react-native";
import NoNames from "../../utilities/NoNames";

export default StyleSheet.create({
  container: {
    position: "absolute",
    width: "100%",
    height: "100%"
  },
  vbnvLio: {
    padding: 20
  },
  loginContainer: {
    backgroundColor: "#000000AC",
    padding: 30
  },
  loginText: {
    color: "white"
  },
  loginTextField: {
    color: "#000000",
    backgroundColor: NoNames.dataWhite,
    fontSize: 16,
    lineHeight: 21,
    height: 35,
    paddingLeft: 10,
    borderRadius: 3
  },
  headerDangNhap: {
    fontSize: 33,
    textAlign: "center",
    color: NoNames.dataWhite,
    padding: 30,
    textShadowColor: "rgba(0, 0, 0, 1)",
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10
  },
  loginButton: {
    backgroundColor: NoNames.btnBgColor,
    padding: 15,
    borderRadius: 3
  },
  loginButtonText: {
    color: NoNames.dataWhite,
    textAlign: "center",
    fontWeight: "bold"
  }
});
