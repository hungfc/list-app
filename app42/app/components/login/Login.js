import React, { Component } from "react";
import {
  Text,
  View,
  ScrollView,
  TextInput,
  TouchableHighlight
} from "react-native";
import styles from "./LoginStyles";
import Background from "../background/Background";
import NoNames from "../../utilities/NoNames";

class Login extends Component<{}> {
  render() {
    return (
      <View style={styles.container}>
        <Background look={NoNames.bgDangNhap} />
        <ScrollView style={styles.vbnvLio}>
          <Text style={styles.headerDangNhap}>{NoNames.headerDangNhap}</Text>
          <View style={styles.loginContainer}>
            <Text style={styles.loginText}>Click thể thay đổi background</Text>
            <View style={{ margin: 7 }} />
            <TouchableHighlight
              style={styles.loginButton}
              activeOpacity={1}
              underlayColor={NoNames.qweqBjhkl}
              onPress={this.props.sadsdDqweqw}
            >
              <Text style={styles.loginButtonText}>
                Add Background
              </Text>
            </TouchableHighlight>
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default Login;
