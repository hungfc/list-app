// @flow

import { StyleSheet } from "react-native";
import Datadetails from "../../utilities/Datadetails";

export default StyleSheet.create({
  backgroundView: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    width: Datadetails.getWidth,
    height: Datadetails.getHeight,
    backgroundColor: "rgba(0,0,0,0)",
    overflow: "hidden"
  },

  backgroundImage: {
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    height: Datadetails.getHeight,
    width: Datadetails.getWidth,
    position: "absolute",
    resizeMode: "repeat"
  }
});
