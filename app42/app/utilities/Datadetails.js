import { Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

const Datadetails = {
  getWidth: width < height ? width : height,
  getHeight: width < height ? height : width
};

export default Datadetails;
