/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';

import { ThanhToan } from './billing';
import { Home } from './home'
import Main from './main'
import Color from './color'

const Stack = createStackNavigator();
class App extends Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Home" options={{ title: 'Gioi Thieu' }} component={Home} />
          <Stack.Screen name="Main" component={Main} />
          <Stack.Screen name="ThanhToan" options={{ title: 'ThanhToan' }} component={ThanhToan} />
          <Stack.Screen name="Color" options={{ title: 'Color' }} component={Color} />
        </Stack.Navigator>
      </NavigationContainer>

    );
  }
};

export default App;
