import React, { Component } from "react";
import { Platform, StyleSheet, Text, View } from "react-native";
import Login from "./app/components/login/Login";
import Secured from "./app/components/secured/Secured";

export default class Main extends Component {
  state = {
    isLoggedIn: false
  };
  render() {
    if (this.state.isLoggedIn)
      return (
        <Secured onLogoutPress={() => this.setState({ isLoggedIn: false })} />
      );
    else
      return <Login sadsdDqweqw={() => this.setState({ isLoggedIn: true })} />;
  }
}