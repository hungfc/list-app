/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';

import HomeScreen from './src/screens/HomeScreen'
import TransferScreen from './src/screens/TransferScreen';
import {ThanhToan} from './billing/index'

const Stack = createStackNavigator();
class App extends Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="HomeScreen" options={{ title: 'HomeScreen' }} component={HomeScreen} />
          <Stack.Screen name="TransferScreen" component={TransferScreen} />
          <Stack.Screen name="ThanhToan" options={{ title: 'ThanhToan' }} component={ThanhToan} />
        </Stack.Navigator>
      </NavigationContainer>

    );
  }
};

export default App;
