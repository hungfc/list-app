import { StyleSheet} from 'react-native';
import values from '../../constants/values';
import colors from '../../constants/colors';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 20,
        backgroundColor: colors.background
    },
    haisuhdiausDwerwer: {
        paddingHorizontal: values.horizontalPadding,
    },
    addbtn: {
        backgroundColor: '#F6AFB0',
        padding: 20,
        textAlign: 'center',
        marginBottom: 15
    },
    addbtntext: {
        textAlign: "center",
        color: '#fff',
        fontWeight: "700"
    }
})
export default styles;