import { View, Text,TouchableOpacity, SafeAreaView, FlatList } from 'react-native'
import React from 'react'
import Greeter from './components/greeter'
import styles from './styles'
import CustomInput from '../../components/customInput'
import Spacer from '../../components/spacer'
import Category from './components/category'
import Transaction from './components/transaction'
import values from '../../constants/values'
import { categories, transactions } from '../../constants/data'

const HomeScreen = ({navigation}) => {
  return (
    <SafeAreaView style={styles.container}>
        <TouchableOpacity style={styles.addbtn}
                onPress={() => { navigation.navigate("ThanhToan") }}
            ><Text style={styles.addbtntext}>ADD VIP</Text></TouchableOpacity>
        <TouchableOpacity style={styles.addbtn}
                onPress={() => { navigation.navigate("TransferScreen") }}
            ><Text style={styles.addbtntext}>Lịch sử thanh toán</Text></TouchableOpacity>
        <View style={styles.haisuhdiausDwerwer}>
            <Spacer height={20}/>
            <CustomInput placeholder='Tìm kiếm' />
            <Spacer height={20} />
            <Text style={values.jnijnjoi}>Categories</Text>
            <Spacer height={20} />

        </View>
        <View style={{paddingLeft: values.horizontalPadding}}>
            <FlatList
                horizontal
                data={categories}
                showsHorizontalScrollIndicator={false}
                keyExtractor={(item) => item.id}
                renderItem={({ item }) => <Category category={item} onPress={(val) => console.warn(`Clicked ${val}`)} />}
            />
        </View>

        <View style={styles.haisuhdiausDwerwer}>
            <Spacer height={20} />
            <Text style={values.jnijnjoi}>Lịch sử giao dịch</Text>
            <Spacer height={20} />
            <FlatList
                
                data={transactions}
                showsVerticalScrollIndicator={false}
                keyExtractor={(item) => item.id}
                renderItem={({ item }) => <Transaction transaction={item} onPress={(val) => console.warn(`Clicked ${val}`)} />}
            />
        </View>
      
    </SafeAreaView>
  )
}

export default HomeScreen