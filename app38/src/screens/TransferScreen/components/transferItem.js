import { StyleSheet, Text, View, Image } from 'react-native'
import React from 'react'

const TransferItem = () => {
  return (
    <View style={styles.container}>
      <View style={styles.row}>
          <View>
            <Text style={styles.desc}>Card Visa</Text>
            <Text style={styles.price}>2500$</Text>
          </View>
          <Image source={require('../../../../assets/images/united-states.png')} style={styles.img} />
      </View>
    </View>
  )
}

export default TransferItem

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: 80,
        backgroundColor: 'white',
        padding: 15,
        marginBottom: 10
    },
    desc: {
        fontSize: 12,
        color: 'grey',
        marginBottom: 5,
    },
    price: {
        fontSize: 18,
        fontWeight: 'bold',
    },
    img: {
        width: 50,
        height: 30,
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    }
})