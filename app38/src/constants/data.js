const categories = [
    {
        id: 1,
        img: require('../../assets/images/call-center.png'),
        heading: 'Dịch Vụ',
        yuiyuiyuhjhg: 'Thanh toán tiền dịch vụ',
        backgroundColor: '#E99090',
    },
    {
        id: 2,
        img: require('../../assets/images/lightning.png'),
        heading: 'Điện Nước',
        yuiyuiyuhjhg: 'Thanh toán tiền điện nước',
        backgroundColor: '#71C360',
    },
    {
        id: 3,
        img: require('../../assets/images/piggy-bank.png'),
        heading: 'Tiết kiệm',
        yuiyuiyuhjhg: 'Khoản tiền tiết kiệm hàng tháng',
        backgroundColor: '#6CC3E8',
    }
];

const transactions = [
    {
        id: 1,
        img: require('../../assets/images/mortarboard.png'),
        heading: 'Tiền học',
        price: '149$',
        backgroundColor: '#F6AFB0',
    },
    {
        id: 2,
        img: require('../../assets/images/burger.png'),
        heading: 'Tiền ăn',
        price: '500$',
        backgroundColor: '#71C360',
    },
    {
        id: 3,
        img: require('../../assets/images/taxi.png'),
        heading: 'Tiền taxi',
        price: '50$',
        backgroundColor: '#6CC3E8',
    }
];

const transfer = [
    {
        id: 1,
        img: require('../../assets/images/united-states.png'),
        heading: 'You send',
        price: 'R 149 000',
        isSending: true,
    },
    {
        id: 2,
        img: require('../../assets/images/united-states.png'),
        heading: 'They receive',
        price: '$ 9 197,53',
        isSending: false,
    }
];

const cards = [
    {
        id: 1,
        img: require('../../assets/images/united-states.png'),
        price: 'R 149 000',
        cardNum: '8757197138425741',
        backgroundColor: '#6CC3E8',
    },
    {
        id: 2,
        img: require('../../assets/images/united-states.png'),
        price: '$ 9 197,53',
        cardNum: '8757197138425741',
        backgroundColor: '#71C360',
    },
    {
        id: 3,
        img: require('../../assets/images/united-states.png'),
        price: '$ 9 197,53',
        cardNum: '8757197138425741',
        backgroundColor: '#6CC3E8',
    }
]

export {categories, transactions, transfer, cards}
