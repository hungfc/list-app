import React from 'react';
import IndexFile from './src';
import {NavigationContainer} from '@react-navigation/native';

export default class App extends React.Component {
  render() {
      return (
          <NavigationContainer>
              <IndexFile />
          </NavigationContainer>
      );
  }
}