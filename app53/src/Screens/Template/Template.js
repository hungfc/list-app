import React, {Component} from 'react';
import {View, Text, StyleSheet, SafeAreaView, Dimensions} from 'react-native';
const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

class Template extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    static navigationOptions = {
        headerShown: false,
    };
    render() {
        return (
            <SafeAreaView>
                <View>
                    <Text style={{padding:20, fontSize:25}}>The App gives you detailed information about temperature, humidity, chance of rain,... exactly at each time of day. And give notifications when the weather has bad developments such as strong wind, heavy rain, blizzard, sandstorm, dust, etc. You can easily monitor the global weather with detailed information such as wind speed, temperature, rain, lightning, wave height, etc. with NOAA's leading standard weather forecast models: ECMWF, GFS every day, and 7 consecutive days and give warnings when the weather bad developments.</Text>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({});

export default Template;
