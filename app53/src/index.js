
import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import HomeScreen from './Screens/Home/Home';
import Template from './Screens/Template/Template';
import {ThanhToan} from '../billing/index'

const Stack = createStackNavigator();
function Router() {
    return (
        <Stack.Navigator
            headerMode="none"
            screenOptions={{
                gestureEnabled: true,
                gestureDirection: 'horizontal'
            }}>
            <Stack.Screen name="Home" component={HomeScreen} />
            <Stack.Screen name="Template" component={Template} />
            <Stack.Screen name="ThanhToan" component={ThanhToan} />
        </Stack.Navigator>
    );
}
export default Router;
