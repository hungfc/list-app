/* eslint-disable react-hooks/rules-of-hooks */
import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  TextInput,
  ToastAndroid,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Button} from 'react-native-elements';
import {useStateValue} from '../stateProvider';
import {ThanhToan} from '../../billing/index'

const welcomeScreen = ({navigation}) => {
  const [{}, dispatch] = useStateValue();
  const [Name, setName] = useState('');
  return (
    <View style={styles.welcomeScreen1}>
      <TouchableOpacity style={styles.hjkhkGrtrty} title="ADD VIP" onPress={() => navigation.navigate('ThanhToan')}><Text style={styles.dfgdfgBsdfsd}>MUA VIP</Text></TouchableOpacity>
      <View style={styles.welcomeScreen}>
        <Image style={styles.logo} source={require('../../assets/logo.png')} />
        <Text style={styles.welcomeText}>Chào mừng đến với Quiz App</Text>
        <Text style={styles.welcomeText}>Nhập tên của bạn để tiếp tục ...</Text>
        <TextInput
          style={styles.inputBox}
          value={Name}
          onChangeText={setName}
          placeholder="Nhập tên"
          placeholderTextColor="black"
        />
        <Button
          title="Next"
          type="clear"
          onPress={() => {
            if (Name === '') {
              ToastAndroid.show(
                'Enter your name to proceed...',
                ToastAndroid.CENTER,
              );
            } else {
              dispatch({
                type: 'SET_USER',
                user: Name,
              });
              navigation.navigate('QuestionScreen');
            }
          }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  welcomeScreen: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: "#E5CCFF"
  },
  welcomeScreen1: {
    flex: 1,
  },
  logo: {
    height: 200,
    width: 200,
  },
  welcomeText: {
    fontSize: 25,
    marginTop: 15,
    textAlign: 'center',
  },
  inputBox: {
    width: '85%',
    height: 40,
    margin: 12,
    borderBottomWidth: 3,
    borderBottomColor: '#3700B3',
    marginTop: 60,
    color: 'black',
  },
  hjkhkGrtrty: {
    padding: 15,
    width: '100%',
    backgroundColor: '#9933FF',
  },
  dfgdfgBsdfsd: {
    color: '#fff',
    fontWeight: 'bold',
    textAlign: 'center'
  }
});
export default welcomeScreen;
