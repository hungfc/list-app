import React from 'react';
import {StyleSheet,TouchableOpacity, Text, View, Linking} from 'react-native';
import {Divider} from 'react-native-elements';
import PieChart from 'react-native-pie-chart';
import {Button} from 'react-native-elements';
import {useStateValue} from '../stateProvider';
const congratScreen = ({navigation}) => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const [{user, score}] = useStateValue();
  const widthAndHeight = 150;
  const series = [score, 10 - score];
  const sliceColor = ['#00FF00'];
  return (
    <View style={styles.cogratsScreen}>
      <Text style={styles.congratsText}>
        Chúc Mừng {user}, Bạn đã đạt được {score} điểm!!!
      </Text>
      <PieChart
        widthAndHeight={widthAndHeight}
        series={series}
        sliceColor={sliceColor}
        doughnut={true}
        coverRadius={0.55}
        coverFill={'#FFF'}
      />
      <Divider width={100} />
      <TouchableOpacity style={styles.hjkhkGrtrty} title="ADD VIP" onPress={() => navigation.navigate('ThanhToan')}><Text style={styles.dfgdfgBsdfsd}>MUA VIP</Text></TouchableOpacity>
      <TouchableOpacity style={styles.hjkhkGrtrty} title="ADD VIP" onPress={() => navigation.navigate('WelcomeScreen')}><Text style={styles.dfgdfgBsdfsd}>HOME PAGE</Text></TouchableOpacity>
    </View>
  );
};

export default congratScreen;

const styles = StyleSheet.create({
  cogratsScreen: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: "#E5CCFF"
  },
  congratsText: {
    fontSize: 26,
    textAlign: 'center',
  },
  hjkhkGrtrty: {
    padding: 15,
    width: '100%',
    marginBottom: 10,
    backgroundColor: '#9933FF',
  },
  dfgdfgBsdfsd: {
    color: '#fff',
    fontWeight: 'bold',
    textAlign: 'center'
  }
});
