import Login from '../screens/Login';
import Register from '../screens/Register'
import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import {ThanhToan} from "../../billing/index";

const Stack = createStackNavigator();

const screenOptionStyle = {
  headerShown: false,
};
const HomeStackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>

      <Stack.Screen name="Login" component={Login} />

      <Stack.Screen name="Register" component={Register} />
      
      <Stack.Screen name="ThanhToan" component={ThanhToan} />
    </Stack.Navigator>
  );
};

export default HomeStackNavigator;
