import React from 'react';
import {Text,View,Image, TextInput} from 'react-native';
export default class Register extends React.Component{

    render(){
        const {navigate} = this.props.navigation
        return(
            <View style={{backgroundColor:"#FFF",height:"100%"}}>
                <Image source ={require('../images/main.jpg')}
                    style={{width:"100%",height:"43%"}}
                />
                <Text
                 style={{
                     fontSize:30,
                     fontFamily:"SemiBold",
                     alignSelf:"center",
                     marginHorizontal:25,
                 }}
                >Cải thiện sức khỏe con người</Text>

                <Text
                style={{
                    fontFamily:"SemiBold",
                    marginHorizontal:25,
                    textAlign:'center',
                    marginTop:5,
                    opacity:0.4
                }}
                >
                    Tình trạng ô nhiễm môi trường khiến sức khỏe chúng ta bị ảnh hưởng và ngày một suy yếu hơn do thường xuyên hít phaỉ khói bụi độc hại và ảnh hưởng của ô nhiễm ánh sáng, tiếng ồn. Các bệnh liên quan đến hệ tim mạch, hô hấp như suy tim, cao huyết áp, hen suyễn, thị lực và thính giác suy giảm,… đều là những căn bệnh có liên quan đến tình trạng này.
                    Được sống trong môi trường yên tĩnh, thoáng mát, sạch sẽ và trong lành sẽ khiến sức khỏe trở nên tốt hơn. Và trồng nhiều cây xanh là biện pháp tự nhiên và lâu dài để cải thiện tình trạng này.
                </Text>

                <View style={{
                    marginHorizontal:55,
                    alignItems:"center",
                    justifyContent:"center",
                    marginTop:30,
                    backgroundColor:"#00716F",
                    paddingVertical:10,
                    borderRadius:23
                }}>
                    <Text onPress={()=>navigate('Login')} style={{
                        color:"white",
                        fontFamily:"SemiBold"
                    }}>Next</Text>
                </View>
              
            </View>
        )
    }
}
