import React from 'react';
import {Text,View,Image, TextInput} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';

export default class Login extends React.Component{

    render(){
        const {navigate} = this.props.navigation
        return(
            <View style={{backgroundColor:"#FFF",height:"100%"}}>
                <Image source ={require('../images/detail.jpg')}
                
                    style={{width:"100%",height:"43%"}}
                />
                <Text
                 style={{
                     fontSize:30,
                     fontFamily:"SemiBold",
                     alignSelf:"center",
                 }}
                >Giảm ô nhiễm môi trường</Text>

                <Text
                style={{
                    fontFamily:"SemiBold",
                    marginHorizontal:25,

                    textAlign:'center',

                    marginTop:5,
                    opacity:0.4
                }}
                >
                    Tình trạng ô nhiễm môi trường đang là vấn đề hết sức đáng lo ngại ở hầu hết các quốc gia trên thế giới. Hiện tượng trái đất nóng lên, ô nhiễm nặng về tiếng ồn, ánh sáng,khói bụi… gây nhiều ảnh hưởng cho đời sống và sức khỏe người dân.Nguyên nhân chủ yếu là do sự phát triển nhanh chóng về kinh tế cũng như chặt phá rừng.

Trồng nhiều cây xanh giúp cung cấp một lượng lớn oxy cho chúng ta thở. Trung bình cứ một cây xanh có thể cung cấp đủ lượng oxy cho 04 người. Đồng thời chúng cũng hấp thụ C02, amoniac, S02, Nox, bụi bẩn,… từ đó làm giảm các khí độc hại bị thải ra môi trường, giúp không khí trở nên trong lành hơn.
                </Text>

                <View style={{
                    marginHorizontal:55,
                    alignItems:"center",

                    justifyContent:"center",

                    marginTop:30,
                    backgroundColor:"#00716F",
                    paddingVertical:10,

                    borderRadius:23
                }}>
                    <Text onPress={()=>navigate('Register')} style={{

                        color:"white",
                        fontFamily:"SemiBold"

                    }}>Next</Text>
                </View>
                <View style={{
                    marginHorizontal:55,
                    alignItems:"center",

                    justifyContent:"center",

                    marginTop:10,
                    backgroundColor:"#00716F",

                    paddingVertical:10,
                    borderRadius:23
                }}>
                    <Text onPress={()=>navigate('ThanhToan')} style={{
                        color:"white",
                        fontFamily:"SemiBold"
                    }}>VIP</Text>
                </View>
            </View>
        )
    }
}