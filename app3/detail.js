import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    useColorScheme,
    View,
    FlatList,
    Button,
    TextInput,
    Alert
  } from 'react-native';

export class Detail extends Component {

    state = {
        myTextInput: '',
        users: ['10:39','7:45','14:25']
    }

    onChangeInput = (event) => {
        this.setState({
            myTextInput:event
        })
    }

    onAddUser = () => {
        this.setState(prevState => {
            return {
                myTextInput: '',
                users: [prevState.myTextInput, ...prevState.users]
            }
        })

        Alert.alert('Add Thành Công')
    }
    render() {
        return (
            <View style={styles.inputWraper}>
                <Button 
                activeOpacity={0.5}
                style={[styles.btn,{width:'100%', marginBottom: 50}]}
                onPress={()=>{
                this.props.navigation.navigate('Billing')
                }}
                title="ADD VIP">
                </Button>
                <View style={styles.formWrap}>
                    <TextInput
                        value={this.state.myTextInput}
                        style={styles.input}
                        onChangeText={this.onChangeInput}
                    />
                    <TouchableOpacity style={[{ width: "30%", margin: 10, padding: 14, backgroundColor: '#3492ef' }]}
                        title="Add Time"
                        onPress={this.onAddUser}
                    ><Text style={styles.textBtn}>Add Time</Text>
                    </TouchableOpacity>
                </View>
                {
                    this.state.users.map( item => (
                        <Text style={styles.users} key={item}>{item}</Text>
                    ))
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    inputWraper: {
      width: '100%',
      backgroundColor:'#cae4e3',
      height: '100%'
    },
    formWrap: {
        display: 'flex',
        width: '100%',
        height: 100,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    input: {
      width: '60%',
      fontSize:20,
      padding: 10,
      borderWidth: 1,
      backgroundColor: '#fff'
    },
    users: {
      borderWidth: 1,
      fontSize: 30,
      borderColor:'#cecece',
      padding: 10,
      marginBottom:10,
      backgroundColor: '#fff'
    },
    textBtn: {
      color: 'white',
      fontSize: 18,
      fontWeight: 'bold',
      textAlign: 'center'
    },
    btnAdd: {
      padding: 15,
      backgroundColor: '#3492ef',
    },
    btn: {
        width: '100%'
    }
  });
