
      /**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';

import {
  StyleSheet,
  TouchableOpacity,
  Text,
  useColorScheme,
  View,
  FlatList,
  Button,
  Image
} from 'react-native';
export class Home extends Component {
  render(){
    return (
        <View style={{width:'100%',height:'100%',justifyContent:'space-between', backgroundColor: '#cae4e3', alignItems: 'center'}}>
          <TouchableOpacity style={styles.btnThem} title="ADD VIP" onPress={() => this.props.navigation.navigate('Billing')}><Text style={styles.textButton}>BUY VIP</Text></TouchableOpacity>
          <Image style={styles.image} source={require('./image/clock.png')}/>
          <TouchableOpacity style={styles.btnThem} title="SET ALARM" onPress={() => this.props.navigation.navigate('Detail')}><Text style={styles.textButton}>SET ALARM</Text></TouchableOpacity>
        </View>
    );
  }
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  btn: {
    height: 35,
    width: '100%',
    alignSelf: 'center',
    backgroundColor: '#00c40f',
    borderRadius: 0,
    borderWidth: 0,
  },
  txt: {
    fontSize:19,
  },
  image: {
    width: 300,
    height: 300
  },
  textButton: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  itemText: {
    color: '#000',
    fontWeight: 'bold',
    marginBottom: 6,
    marginTop: 5
  },
  btnThem: {
    padding: 15,
    width: '100%',
    backgroundColor: '#3492ef',
  }
});

