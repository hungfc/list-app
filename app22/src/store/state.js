export default state = {
  categories: [
    {
      title: 'DevOps',
      noOfJobs: 48,
    },
    {
      title: 'Software Development',
      noOfJobs: 23,
    },
    {
      title: 'UI/UX Design',
      noOfJobs: 88,
    },
  ],
  jobs: [
    {
      id: 'A123',
      company: {
        name: 'Spotify',
      },
      role: 'Senior UI Designer',
      salary: '$50/hr',
      location: 'Remote',
    },
    {
      id: 'B567',
      company: {
        name: 'Google',
      },
      role: 'Product Manager',
      salary: '$100/hr',
      location: 'Full time',
    },
    {
      id: 'C789',
      company: {
        name: 'Coca-cola',
      },
      role: 'React Developer',
      salary: '$50/hr',
      location: 'Remote',
    },
    {
      id: 'D215',
      company: {
        name: 'Behance',
      },
      role: 'C# Developer',
      salary: '$100/hr',
      location: 'Full time',
    },
  ],
  bookmarks: [
    {
      id: 'A123',
      company: {
        name: 'Spotify',
      },
      role: 'UI Designer',
      salary: '$50/hr',
      location: 'Remote',
    },
  ],
};
