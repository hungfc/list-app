import React, {useState} from 'react';
import Home from './components/Home';
import Weather from './components/Weather';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {ThanhToan} from './billing/index';

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Home" options={{ title: 'Home' }} component={Home} />
          <Stack.Screen name="Weather" component={Weather} />
          <Stack.Screen name="ThanhToan" options={{ title: 'ThanhToan' }} component={ThanhToan} />
        </Stack.Navigator>
      </NavigationContainer>
  );
}
