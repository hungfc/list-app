import React, { Component } from 'react';

import {
     StyleSheet,
     TouchableOpacity,
     Text,
     View
 } from 'react-native';
 
export class Detail extends Component {
     render() {
         const{pData} =this.props.route.params;
         return (
             <View style={{width: '100%', height: '100%', backgroundColor: '#Bbe0bd'}}>
               <View style={styles.layoutWrap}>
                 <View style={{width: '100%', paddingLeft: 10}}>
                   <Text style={styles.base}>{pData.detail}</Text>
                 </View>
               </View>
               <TouchableOpacity style={styles.addBtn} title="MUA VIP" onPress={() => this.props.navigation.navigate('Billing')}><Text style={styles.btnText}>MUA VIP ĐỂ XEM ĐẦY ĐỦ</Text></TouchableOpacity>
             </View>
         );
     }
 };
 
 const styles = StyleSheet.create({
    base: {
      fontWeight: 'bold',
      fontSize: 18,
    },
    layoutWrap: {
      flexDirection: 'row',
      paddingRight:10,
      paddingLeft: 10,
      paddingTop: 10,
    },
    btnText: {
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    addBtn: {
      padding: 15,
      backgroundColor: '#106f16',
      width: '90%',
      alignSelf: 'center',
      marginTop: 20
    }
 });