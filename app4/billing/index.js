import {
  Alert,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';
import RNIap, {
  InAppPurchase,
  PurchaseError,
  SubscriptionPurchase,
  acknowledgePurchaseAndroid,
  consumePurchaseAndroid,
  finishTransaction,
  finishTransactionIOS,
  purchaseErrorListener,
  purchaseUpdatedListener,
  
} from 'react-native-iap';
import React, { Component } from 'react';

import Button from 'apsl-react-native-button';

const itemSkus = [
  'com.haesxkegce.1',
  'com.haesxkegce.2',
  'com.haesxkegce.3',
  'com.haesxkegce.4',
  'com.haesxkegce.5',
  'com.haesxkegce.6',
];

const itemSubs = [
  'com.haesxkegce.sub.1', // subscription
  'com.haesxkegce.sub.2',
  'com.haesxkegce.sub.3',
  'com.haesxkegce.sub.4',
  'com.haesxkegce.sub.5',
];

let purchaseUpdateSubscription;
let purchaseErrorSubscription;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: Platform.select({
      ios: 0,
      android: 4,
    }),
    paddingTop: Platform.select({
      ios: 0,
      android: 4,
    }),
    backgroundColor: 'white',
  },
  header: {
    flex: 20,
    flexDirection: 'row',
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 100,
    height: 40,
    resizeMode: 'contain' 
  },
  contentDetail: {
    flex: 80,
    flexDirection: 'column',
    justifyContent: 'center',
    alignSelf: 'stretch',
    alignItems: 'center',
  },
  btnProduct1: {
    height: 55,
    width: 90,
    alignSelf: 'center',
    backgroundColor: '#106f16',
    borderRadius: 0,
    borderWidth: 0,
  },
  btnProduct: {
    height: 55,
    width: 200,
    alignSelf: 'center',
    backgroundColor: '#2547be',
    borderRadius: 0,
    borderWidth: 0,
  },
  txtStyles: {
    fontSize: 16,
    color: 'white',
    fontWeight: 'bold'
  },
});

export class Billing extends Component {
  constructor(props) {
    super(props);

    this.state = {
      unconsumed:null,
      productList: [],
      productSubList: [],
      receipt: '',
      availableItemsMessage: '',
    };
  }

  async componentDidMount() {
    try {
      await RNIap.initConnection();
      await RNIap.flushFailedPurchasesCachedAsPendingAndroid();
    } catch (err) {
      console.warn(err.code, err.message);
    }

    purchaseUpdateSubscription = purchaseUpdatedListener(
      async (purchase) => {
        console.info('purchase', purchase);
        const receipt = purchase.transactionReceipt
          ? purchase.transactionReceipt
          : purchase.originalJson;
        console.info(receipt);
        if (receipt) {
          try {
            const ackResult = await finishTransaction(purchase,true);
            console.info('ackResult', ackResult);
          } catch (ackErr) {
            console.warn('ackErr', ackErr);
          }

          this.setState({ receipt }, () => this.goSuccess());
        }
      },
    );

    purchaseErrorSubscription = purchaseErrorListener(
      (error) => {
        console.log('purchaseErrorListener', error);
        Alert.alert('purchase error', JSON.stringify(error));
      },
    );
    this.getBilling();
    this.getBillingSubscript();
  }

  componentWillUnmount() {
    if (purchaseUpdateSubscription) {
      purchaseUpdateSubscription.remove();
      purchaseUpdateSubscription = null;
    }
    if (purchaseErrorSubscription) {
      purchaseErrorSubscription.remove();
      purchaseErrorSubscription = null;
    }
    RNIap.endConnection();
  }

  goSuccess = async() => {

    Alert.alert('Thanh toán thành công. Receipt: ', this.state.receipt);
  };

  getBilling = async () => {
    try {
      const products = await RNIap.getProducts(itemSkus);
      console.log('Products', products);
      this.setState({ productList: products });
    } catch (err) {
      console.warn(err.code, err.message);
    }
  };

  getBillingSubscript = async () => {
    try {
      const products = await RNIap.getSubscriptions(itemSubs);
      console.log('Product subscript', products);
      this.setState({ productSubList: products });
    } catch (err) {
      console.warn(err.code, err.message);
    }
  };

  getAvailablePurchases = async () => {
    try {
      console.log('init')
      console.info(
        'Get available purchases (non-consumable or unconsumed consumable)',
      );
      const purchases = await RNIap.getAvailablePurchases();
      console.info('Available purchases :: ', purchases);

      if (purchases && purchases.length > 0) {
        this.consumePurchaseAndroid(purchases);
      }
    } catch (err) {
      console.warn(err.code, err.message);
      Alert.alert(err.message);
    }
  };

  async consumePurchaseAndroid(purchases){
    for(var i=0;i<purchases.length;i++){
      var pur=purchases[i];
      await RNIap.finishTransaction(pur,true);
    }
      
  }

  requestBilling = async (sku) => {
    try {
      var s = await RNIap.requestPurchase(sku, false);
      console.log(s)
    } catch (err) {
      console.warn(err.code, err.message);
    }
  };

  requestSubscription = async (sku) => {
    try {
      RNIap.requestSubscription(sku);
    } catch (err) {
      Alert.alert(err.message);
    }
  };

  render() {
    const { productList, receipt, availableItemsMessage, productSubList } = this.state;
    const receipt100 = receipt.substring(0, 100);

    return (
      <View style={styles.container}>
        <View style={styles.contentDetail}>
          <ScrollView style={{ alignSelf: 'stretch'}}>
            {productSubList && productSubList.length > 0 ? <Text style={{ textAlign: "center", fontSize: 18, fontWeight: 'bold',marginBottom: 10 }}>Subscription list</Text> : null}
            <View style={{
                    flexDirection: 'row',
                    backgroundColor: '#60d867',
                    justifyContent: 'space-around',
                    alignItems: 'center',
                    width: '100%',
                    height: 100
                  }}>
            {productSubList.map((product, x) => {
              return (
                <View key={x}>
                  <Button
                    onPress={() =>
                      this.requestSubscription(product.productId)
                    }
                    activeOpacity={0.6}
                    style={styles.btnProduct1}
                    textStyle={styles.txtStyles}>
                    {"Subs" + (x + 1)}
                  </Button>
                </View>
              );
            })}
            </View>
            {productList && productList.length > 0 ? <Text style={{ textAlign: "center", fontSize: 16, fontWeight: 'bold', paddingBottom: 10, paddingTop: 20 }}>Product list</Text> : null}
            {productList.map((product, y) => {
              return (
                <View
                  key={y}
                  style={{
                    flexDirection: 'column',
                    backgroundColor: '#60d867',
                    justifyContent: 'center',
                    alignItems: 'center',
                    padding: 10,
                    margin: 4
                  }}>
                  <Image style={styles.image} source={require('../image/billing.png')}/>
                  <Text
                    style={{
                      fontSize: 18,
                      color: 'black',
                      fontWeight: 'bold',
                      alignSelf: 'center',
                      paddingHorizontal: 0,
                      marginVertical: 10
                    }}>
                    ({product.originalPriceAndroid} {product.currency})
                  </Text>
                  <Button
                    onPress={() =>
                      this.requestBilling(product.productId)
                    }
                    activeOpacity={0.6}
                    style={styles.btnProduct}
                    textStyle={styles.txtStyles}>
                    BUY PRODUCT
                  </Button>
                </View>
              );
            })}
            
          </ScrollView>
        </View>
      </View>
    );
  }
}
