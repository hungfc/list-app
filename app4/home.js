import * as React from 'react';
import { View, Text, FlatList, Dimensions, StyleSheet, TouchableOpacity,} from 'react-native';

const data = [
  {
    id: '1',
    name: 'Hồ Chí Minh',
    detail: 'Hồ Chí Minh (chữ Nho: 胡志明; 19 tháng 5 năm 1890 – 2 tháng 9 năm 1969), tên khai sinh là Nguyễn Sinh Cung (chữ Nho: 阮生恭), là một nhà cách mạng và chính khách người Việt Nam. Ông là người sáng lập ra Đảng Cộng sản Việt Nam, từng là Chủ tịch nước Việt Nam Dân chủ Cộng hoà từ 1945 -1969, Thủ tướng Việt Nam Dân chủ Cộng hòa trong những năm 1945–1955, Tổng Bí thư Ban Chấp hành Trung ương Đảng Lao động Việt Nam từ 1956 - 1960, Chủ tịch Ban Chấp hành Trung ương Đảng Lao động Việt Nam từ năm 1951 đến khi qua đời. Ở Việt Nam, ông thường được gọi là Bác Hồ...'
  },
  {
      id: '2',
      name: 'Võ Nguyên Giáp',
      detail: 'Võ Nguyên Giáp (25 tháng 8 năm 1911 – 4 tháng 10 năm 2013), tên khai sinh là Võ Giáp,[a] còn được gọi là tướng Giáp hoặc anh Văn, là một nhà lãnh đạo quân sự và chính trị gia người Việt Nam. Ông là Đại tướng đầu tiên, Tổng Tư lệnh tối cao của Quân đội nhân dân Việt Nam, một trong những người thành lập Việt Nam Dân chủ Cộng hòa, được Chính phủ Việt Nam đánh giá là "người học trò xuất sắc và gần gũi của Chủ tịch Hồ Chí Minh",[1][2] là chỉ huy trưởng của các chiến dịch trong Chiến tranh Đông Dương (1946–1954) đánh đuổi Thực dân Pháp, Chiến tranh Việt Nam (1960–1975), thống nhất đất nước và Chiến tranh biên giới Việt – Trung (1979) chống quân Trung Quốc tấn công biên giới phía Bắc...'
  },
  {
      id: '3',
      name: 'Lê Thái Tổ',
      detail: 'Lê Thái Tổ (chữ Hán: 黎太祖 10 tháng 9, 1385[2] – 5 tháng 10, 1433) tên khai sinh: Lê Lợi (黎利) là một nhà chính trị, nhà lãnh đạo quân sự, người đã thành lập một đội quân người Việt và lãnh đạo đội quân này chiến đấu chống lại sự chiếm đóng của quân đội nhà Minh từ năm 1418 đến lúc đánh đuổi hoàn toàn quân Minh ra khỏi Đại Việt vào năm 1428 sau đó xây dựng và tái thiết lại đất nước. Ông cũng thành công với các chiến dịch quân sự đánh dẹp các tù trưởng ở biên giới phía Bắc Đại Việt và quân đội Ai Lao. Ông được coi là anh hùng, vị vua huyền thoại của Đại Việt với tài năng quân sự, khả năng cai trị và lòng nhân ái đối với nhân dân...'
  },
  {
      id: '4',
      name: 'Phan Bội Châu',
      detail: 'Phan Bội Châu sinh ngày 26 tháng 12 năm 1867 tại làng Đan Nhiễm, xã Xuân Hòa, huyện Nam Đàn, tỉnh Nghệ An.Cha ông là Phan Văn Phổ, mẹ là Nguyễn Thị Nhàn. Ông nổi tiếng thông minh từ bé, năm 6 tuổi học 3 ngày thuộc hết Tam Tự Kinh, 7 tuổi ông đã đọc hiểu sách Luận Ngữ, 13 tuổi ông thi đỗ đầu huyện.Thuở thiếu thời ông đã sớm có lòng yêu nước. Năm 17 tuổi, ông viết bài Hịch Bình Tây Thu Bắc đem dán ở cây đa đầu làng để hưởng ứng việc Bắc Kỳ khởi nghĩa kháng Pháp. Năm 19 tuổi (1885), ông cùng bạn là Trần Văn Lương lập đội Sĩ tử Cần Vương chống Pháp, nhưng bị đối phương kéo tới khủng bố nên phải giải tán...'
  },
  {
      id: '5',
      name: 'Nguyễn Du',
      detail: 'Nguyễn Du (chữ Hán: 阮攸; 3 tháng 1 năm 1766 – 1820[1]) tên tự là Tố Như (素如), hiệu là Thanh Hiên (清軒), biệt hiệu là Hồng Sơn lạp hộ (鴻山獵戶), Nam Hải điếu đồ (南海釣屠), là một nhà thơ, nhà văn hóa lớn thời Lê mạt Nguyễn sơ ở Việt Nam. Ông được người Việt kính trọng tôn xưng là "Đại thi hào dân tộc"[2] và được UNESCO vinh danh là Danh nhân văn hóa thế giới...'
  },
  {
      id: '6',
      name: 'Trần Hưng Đạo',
      detail: 'Trần Hưng Đạo (chữ Hán:陳興道); 1231? – 1300), tên thật là Trần Quốc Tuấn (chữ Hán:陳國峻), tước hiệu Hưng Đạo đại vương, là một nhà chính trị, nhà quân sự, tôn thất hoàng gia Đại Việt thời Trần. Ông được biết đến trong lịch sử Việt Nam với việc chỉ huy quân đội đánh tan hai cuộc xâm lược của quân Nguyên – Mông năm 1285 và năm 1288. Phần lớn tài liệu nghiên cứu lịch sử và cả dân gian thời sau thường dùng tên gọi vắn tắt là "Trần Hưng Đạo" thay cho cách gọi đầy đủ là "Hưng Đạo đại vương Trần Quốc Tuấn", vốn bao gồm tước hiệu được sắc phong cho ông. Ông là 1 trong 14 vị anh hùng tiêu biểu của dân tộc Việt Nam...'
  }
];

const formatProduct = (data, numberColumn) => {
  const numberRows = Math.floor(data.length / numberColumn);

  let numberLastRow = data.length - (numberRows * numberColumn);
  while (numberLastRow !== numberColumn && numberLastRow !== 0) {
    data.push({ id: `blank-${numberLastRow}`, empty: true });
    numberLastRow++;
  }

  return data;
};

const numberColumn = 1;

export class Home extends React.Component {
  renderProduct = ({ item, index }) => {
    if (item.empty === true) {
      return <View style={[styles.item, styles.itemInvisible]} />;
    }
    return (
      <View
        style={styles.item}
      >
        <TouchableOpacity style={styles.btnAdd} title="Detail" onPress={() => this.props.navigation.navigate('Detail',{pData:item})}><Text style={styles.textBtn}>{item.name}</Text></TouchableOpacity>
      </View>
    );
  };

  render() {
    return (
      <View style={styles.wraper}>
        <TouchableOpacity style={styles.btnVip} title="ADD VIP" onPress={() => this.props.navigation.navigate('Billing')}><Text style={styles.textBtn}>BUY VIP</Text></TouchableOpacity>
        <FlatList
          data={formatProduct(data, numberColumn)}
          style={styles.container}
          renderItem={this.renderProduct}
          numColumns={numberColumn}
        />
      </View>
        
    );
  }
}

const styles = StyleSheet.create({
  wraper: {
    width: '100%',
    height: '100%'
  },
  container: {
    flex: 1,
    marginVertical: 20,
  },
  item: {
    backgroundColor: '#60d867',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    margin: 1,
    marginBottom: 1,
    height: 100,
  },
  itemInvisible: {
    backgroundColor: 'transparent',
  },
  textBtn: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  btnVip1: {
    padding: 15,
    width: '100%',
    backgroundColor: '#D89460',
  },
  btnVip: {
    padding: 15,
    width: '100%',
    backgroundColor: '#106f16',
  }
});