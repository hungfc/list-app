
      /**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
  Image
} from 'react-native';
export class Home extends Component {
  render(){
    return (
        <View style={{width:'100%',height:'100%',justifyContent:'space-between', backgroundColor: '#20B6C5', alignItems: 'center'}}>
          <TouchableOpacity style={styles.ertDfgf} title="ADD VIP" onPress={() => this.props.navigation.navigate('ThanhToan')}><Text style={styles.bnmbnFrt}>ADD VIP</Text></TouchableOpacity>
          <Image style={styles.image} source={require('./image/date-time.jpg')}/>
          <TouchableOpacity style={styles.ertDfgf} title="CHECK DATE TIME" onPress={() => this.props.navigation.navigate('CheckDateTime')}><Text style={styles.bnmbnFrt}>CHECK DATE TIME</Text></TouchableOpacity>
        </View>
    );
  }
};
       
const styles = StyleSheet.create({
  bnmbnFrt: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  image: {
    width: 320,
    height: 270
  },
  ertDfgf: {
    padding: 15,
    width: '100%',
    backgroundColor: '#006666',
  }
});