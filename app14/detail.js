import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Button,
    Alert
} from 'react-native';

export class CheckDateTime extends Component {

    ShowCurrentDate=()=>{
 
        var date = new Date().getDate();
        var month = new Date().getMonth() + 1;
        var year = new Date().getFullYear();
   
        Alert.alert(date + '-' + month + '-' + year);
   
       }

       ShowCurrentTime=()=>{
 
        var hours = new Date().getHours();
        var minutes  = new Date().getMinutes();
        var seconds  = new Date().getSeconds();
   
        Alert.alert(hours + ':' + minutes + ':' + seconds);
   
       }
    render() {
        return (
            <View style={styles.MainContainer}>
                <View style={{margin:10}}>
                <Button title="Show Current Date" color="#006666" onPress={this.ShowCurrentDate} />
                </View>
          
          <View style={{margin:10}}>
          <Button title="Show Current Time" color="#006666" onPress={this.ShowCurrentTime} />
          </View>
          
       
      </View>
        )
    }
}

const styles = StyleSheet.create(
    {
      MainContainer: {
     
        justifyContent: 'center',
        flex: 1,
        backgroundColor: '#F5FCFF',
        margin: 10,
     
      }
     
    });
