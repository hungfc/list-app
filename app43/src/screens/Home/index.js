import React, {useState, useContext} from 'react';
import {
  View,
  Text,
  KeyboardAvoidingView,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import styles from './homeStyle.js';

const Home = ({ navigation }) => {
  const [data, setData] = useState({
    username: 'Akita',
  });

  return (
    <KeyboardAvoidingView style={styles.homeContainer}>
      <View style={styles.homeContent}>
        <View style={styles.homeHeader}>
          <View>
            <Text style={styles.usernameText}>Xin Chào {data?.username}</Text>
            <Text style={styles.findJobText}>Bạn đang tìm kiếm công việc</Text>
          </View>
          <View style={styles.profilePicWrapper}>
            <Image source={require('../../../image/img.jpg')} style={styles.profilePic} />
          </View>
        </View>
        <View style={styles.nameContainer}>
          <TouchableOpacity style={styles.btnAddvip} title="ADD VIP" onPress={() => navigation.navigate('ThanhToan')}><Text style={styles.label}>ADD VIP</Text></TouchableOpacity>
        </View>
        <View style={styles.searchContainer}>
          <TextInput placeholder="Search" style={styles.searchInput} />
          <TouchableOpacity style={styles.searchButton}>
            <Ionicons name="add-circle-outline" size={25} color="#fff" />
          </TouchableOpacity>
        </View>
        <View style={styles.container}>
          <View style={styles.section}>
            <Text style={styles.heading}>Công Việc Mới</Text>
            <View style={{display:'flex', flexDirection: 'row'}}>
            <TouchableOpacity style={styles.categoryContainer}>
              <Image source={require('../../../image/main.jpg')} style={styles.categoryImage} />
              <Text style={styles.categoryTitle} numberOfLines={1} ellipsizeMode="tail">
                PHP
              </Text>
              <Text
                style={styles.categoryNoOfJobs}
                numberOfLines={1}
                ellipsizeMode="tail">
                Jobs
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.categoryContainer}>
            <Image source={require('../../../image/main.jpg')} style={styles.categoryImage} />
              <Text style={styles.categoryTitle} numberOfLines={1} ellipsizeMode="tail">
                Javascript
              </Text>
              <Text
                style={styles.categoryNoOfJobs}
                numberOfLines={1}
                ellipsizeMode="tail">
                Jobs
              </Text>
            </TouchableOpacity>
            </View>
          </View>

          <View style={styles.section}>
            <Text style={styles.heading}>Công Việc Yêu Thích</Text>
            <TouchableOpacity
      style={styles.jobContainer}
      onPress={() => navigation.navigate('JobDetail')}>
      <Image source={require('../../../image/list.jpg')} style={styles.jobImage} />
      <View style={styles.jobInfo}>
        <Text style={styles.jobCompany} numberOfLines={1} ellipsizeMode="tail">
          Isobaaa
        </Text>
        <Text style={styles.jobRole} numberOfLines={1} ellipsizeMode="tail">
          java
        </Text>
        <View style={styles.flexRow}>
          <Text style={styles.jobSalary} numberOfLines={1} ellipsizeMode="tail">
            50 |
          </Text>
          <Text
            style={styles.jobLocation}
            numberOfLines={1}
            ellipsizeMode="tail">
            Remote
          </Text>
        </View>
      </View>

    </TouchableOpacity>
    <TouchableOpacity
      style={styles.jobContainer}
      onPress={() => navigation.navigate('JobDetail')}>
      <Image source={require('../../../image/list.jpg')} style={styles.jobImage} />
      <View style={styles.jobInfo}>
        <Text style={styles.jobCompany} numberOfLines={1} ellipsizeMode="tail">
          Isobaaa
        </Text>
        <Text style={styles.jobRole} numberOfLines={1} ellipsizeMode="tail">
          java
        </Text>
        <View style={styles.flexRow}>
          <Text style={styles.jobSalary} numberOfLines={1} ellipsizeMode="tail">
            50 |
          </Text>
          <Text
            style={styles.jobLocation}
            numberOfLines={1}
            ellipsizeMode="tail">
            Remote
          </Text>
        </View>
      </View>

    </TouchableOpacity>
    <TouchableOpacity
      style={styles.jobContainer}
      onPress={() => navigation.navigate('JobDetail')}>
      <Image source={require('../../../image/list.jpg')} style={styles.jobImage} />
      <View style={styles.jobInfo}>
        <Text style={styles.jobCompany} numberOfLines={1} ellipsizeMode="tail">
          Isobaaa
        </Text>
        <Text style={styles.jobRole} numberOfLines={1} ellipsizeMode="tail">
          java
        </Text>
        <View style={styles.flexRow}>
          <Text style={styles.jobSalary} numberOfLines={1} ellipsizeMode="tail">
            50 |
          </Text>
          <Text
            style={styles.jobLocation}
            numberOfLines={1}
            ellipsizeMode="tail">
            Remote
          </Text>
        </View>
      </View>

    </TouchableOpacity>
          </View>
        </View>
      </View>
    </KeyboardAvoidingView>
  );
}

export default Home;
