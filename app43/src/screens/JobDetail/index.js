import React, {useContext, useState} from 'react';
import {View, Text, TouchableOpacity, Image, ScrollView} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import styles from './jobDetailStyle.js';
const JobDetail = ({ navigation }) => {
  const handleBackButton = () => {
    navigation?.goBack();
  };

  return (
    <View style={styles.jobDetailContainer}>
      <View style={styles.jobDetailContent}>
        <View style={styles.jobDetailHeader}>
          <TouchableOpacity
            style={styles.jobDetailCircleContainer}
            onPress={() => handleBackButton()}>
            <AntDesign name="arrowleft" size={22} color="#333333" />
          </TouchableOpacity>
        </View>
        <View style={styles.jobDetaiRow1}>
          <Text
            style={styles.jobCompany}
            numberOfLines={1}
            ellipsizeMode="tail">
            Isobaaa
          </Text>
          <Text style={styles.jobRole} numberOfLines={1} ellipsizeMode="tail">
          Java Dev (Spring, ReactJS) All level
          </Text>
          <View style={styles.flexRow}>
            <Text
              style={styles.jobSalary}
              numberOfLines={1}
              ellipsizeMode="tail">
              100$ |
            </Text>
            <Text
              style={styles.jobLocation}
              numberOfLines={1}
              ellipsizeMode="tail">
              Remote
            </Text>
          </View>
        </View>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.jobDetailSection}>
            <Text style={styles.heading}>Mô tả</Text>
            <Text style={styles.jobInfoText}>Nghiên cứu phát triển nền tảng, ứng dụng và hệ sinh thái IoT: IoT Platform, M2M  eSIM Platform, Smart Home ecosystem</Text>
            <Text style={styles.heading}>Chi Tiết</Text>
            <Text style={styles.jobInfoText}>Có chuyên môn về phát triển phần mềm bằng Java và Spring, Microservices, Rest API, Có kinh nghiệm sử dụng các framework Spring, Angular, ReactJS, Hibernate, Có thể sử dụng một trong các hệ quản trị cơ sở dữ liệu Mysql, Oracle, Sql server và cơ sở dữ liệu NoSQLHiểu biết về webservice SOAP/Restful, chuẩn message JSON, XML, OOP, Design pattern ...
Hiểu biết thành thạo về các công cụ hỗ trợ phát triển như Git, Jira
Có tư duy logic và kỹ năng giải quyết vấn đề Nghiên cứu phát triển các phần mềm và nền tảng Chuyển đổi số Doanh nghiệp
Nghiên cứu phát triển nền tảng và hệ thống lõi viễn thông hiệu năng cao</Text>
          </View>
        </ScrollView>
        <View style={styles.bottomWrapper}>
          <TouchableOpacity style={styles.applyHereBtn}>
            <Text style={styles.applyHereText}>Jobs tốt</Text>
            <MaterialIcons name="double-arrow" color="#fff" size={20} />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

export default JobDetail;
