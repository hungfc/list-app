export default state = {
  categories: [
    {
      title: 'Java',
      noOfJobs: 48,
    },
    {
      title: 'java Development',
      noOfJobs: 23,
    },
    {
      title: 'Java',
      noOfJobs: 88,
    },
  ],
  jobs: [
    {
      id: '789',
      company: {
        name: 'PHP',
      },
      role: 'PHP',
      salary: '$50/hr',
      location: 'Remote',
    },
    {
      id: '345',
      company: {
        name: 'Javascript',
      },
      role: 'Javascript Manager',
      salary: '$100/hr',
      location: 'Full time',
    },
    {
      id: '789',
      company: {
        name: 'Pessi',
      },
      role: 'Node Developer',
      salary: '$50/hr',
      location: 'Remote',
    },
    {
      id: '1321',
      company: {
        name: 'Akira',
      },
      role: 'Golang Developer',
      salary: '$10/hr',
      location: 'Full time',
    },
  ],
  bookmarks: [
    {
      id: '123',
      company: {
        name: 'Java',
      },
      role: 'jaava',
      salary: '$5/hr',
      location: 'Remote',
    },
  ],
};
