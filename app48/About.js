import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
  Image,
  ScrollView
  
} from 'react-native';
const About = ({ navigation }) => {
    return (
        <ScrollView>
        <View style={{width:'100%',height:'100%',justifyContent:'space-between', backgroundColor: '#fff', alignItems: 'center'}}>

          <TouchableOpacity style={styles.btnAddd} title="ADD VIP" onPress={() => navigation.navigate('ThanhToan')}><Text style={styles.textBtnnn}>GET VIP</Text></TouchableOpacity>
          
          <Image style={styles.image} source={require('./image/main.jpg')}/>
          <Text style={{padding:10, fontSize:18}}>Detoxify the body, also known as detox. This method is becoming more and more popular. However, there are still some people who do not understand correctly about body cleansing as using products to help eliminate toxins or following a specific diet.

In fact, our body's natural detoxification mechanism will be through urine, sweat glands and liver to eliminate harmful substances in internal organs without having to apply a diet. there. Substances that are considered toxins that need to be eliminated from the body often include synthetic chemicals, pollutants, heavy metals and chemicals from processed foods.

However, sometimes the body's natural detoxification mechanism also needs to rest after hours of overwork. At that time, we need methods to purify the body to help reduce overload and maintain healthy detoxification organs.</Text>
          <TouchableOpacity style={styles.btnAddd} title="Detail" onPress={() => navigation.navigate('Home')}><Text style={styles.textBtnnn}>DETAIL</Text></TouchableOpacity>
        </View>
        </ScrollView>
    );
  
};
       
const styles = StyleSheet.create({
  btn: {
    height: 35,
    width: '100%',
    alignSelf: 'center',
    backgroundColor: '#00c40f',
    borderRadius: 0,
    borderWidth: 0,
  },
  txt: {
    fontSize:19,
  },
  image: {
    width: '100%',
    height: 200
  },
  textBtnnn: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  btnAddd: {
    padding: 15,
    width: '100%',
    backgroundColor: '#0080FF',
  }
});

export default About;