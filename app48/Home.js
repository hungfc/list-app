import React, { useState } from "react";
import { View, Text, Image, StatusBar } from "react-native";
import AppIntroSlider from "react-native-app-intro-slider";
import { COLORS, SIZES } from './src/constants/theme';

const slides = [
  {
    id: 1,
    title: 'Start up',
    description: '“Use this time to be physically active. You can move around the office, across desks, or walk out into the hallway every 20 minutes. If you cant arrange a commute, try to apply the 20-20-20 rule (Every 20 minutes, look 20 meters away from the screen, for about 20 seconds)."',
    image: require('./src/assets/onboardScreen1.png')
  },
  {
    id: 2,
    title: 'Lunch time',
    description: '“To avoid hunger and low blood pressure in the afternoon from eating too little, you can choose soups or salads made from cabbage vegetables, which contain heart-healthy fats. You can add some lean meat to the salad. After lunch, spend about 30 minutes walking."',
    image: require('./src/assets/onboardScreen2.png')
  },
  {
    id: 3,
    title: 'Sleep',
    description: '“One of the best ways to detox your body is to ensure quality sleep. Sleep is connected to weight loss, stress levels and overall health. To avoid pressure after a long day, you take a shower and give yourself some time to relax before going to bed. You can also do some yoga to help relax your body."',
    image: require('./src/assets/onboardScreen3.png')
  }
]

export default function Home() {
  const [showHomePage, setShowHomePage] = useState(true);

  StatusBar.setBarStyle('light-content', true);
  StatusBar.setBackgroundColor(COLORS.primary);

  const buttonLabel = (label) => {
    return(
      <View style={{
        padding: 12
      }}>
        <Text style={{
          color: COLORS.title,
          fontWeight: '600',
          fontSize: SIZES.h4,
        }}>
          {label}
        </Text>
      </View>
    )
  }
    return(
      <AppIntroSlider
        data={slides}
        renderItem={({item}) => {
          return(
            <View style={{
              flex: 1,
              alignItems: 'center',
              padding: 15,
              paddingTop: 100,
            }}>
              <Image
                source={item.image}
                style={{
                  width: SIZES.width - 80,
                  height: 400,
                }}
                resizeMode="contain"
              />
              <Text style={{
                fontWeight: 'bold',
                color: COLORS.title,
                fontSize: SIZES.h1,
              }}>
                {item.title}
              </Text>
              <Text style={{
                textAlign: 'center',
                paddingTop: 5,
                color: COLORS.title
              }}>
                {item.description}
              </Text>
            </View>
          )
        }}
        activeDotStyle={{
          backgroundColor: COLORS.primary,
          width: 30,
        }}
        showSkipButton
        renderNextButton={() => buttonLabel("Next")}
        renderSkipButton={() => buttonLabel("Skip")}
        renderDoneButton={() => buttonLabel("Done")}
        onDone={() => {
          setShowHomePage(true);
        }}
      />
    )
}