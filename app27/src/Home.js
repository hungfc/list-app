import React from 'react'
import { StyleSheet, Text, View, Image, TextInput,TouchableOpacity } from 'react-native'
import {useNavigation} from '@react-navigation/core'
import { Colors } from './colors'
import ListCourse from './ListCourse'

const Home = () => {
    const navigation = useNavigation();
    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Image style={styles.icon} source={require('./assets/menu.png')} />
                <Image source={require('./assets/avatar.png')} />
            </View>
            <TouchableOpacity style={styles.btnAddvip} title="ADD VIP" onPress={() => navigation.navigate('ThanhToan')}><Text style={styles.label}>ADD VIP</Text></TouchableOpacity>
            <View>
                <Text style={styles.heading}>Hey Alex, </Text>
                <Text style={styles.description}>Find a course you want to learn</Text>


                <View style={styles.searchContainer}>
                    <Image style={styles.icon} source={require('./assets/search.png')} />
                    <TextInput style={styles.input} placeholder="Search for anything" />
                </View>
            </View>
            <ListCourse />
        </View>
    )
}

export default Home

const styles = StyleSheet.create({
    header: {
       justifyContent: 'space-between',
       flexDirection: 'row'
    },
    container: {
        padding: 10,
        flex:1,
        backgroundColor: '#fff'
    },
    heading: {
        fontSize: 20,
        fontWeight: '500'
    },
    description: {
        fontSize: 15,
        marginVertical: 10
    },
    searchContainer: {
        flexDirection: 'row',
        padding: 12,
        paddingHorizontal: 16,

        backgroundColor: Colors.sliver,
        borderRadius: 20
    },
    input: {
        marginLeft: 10
    },
    icon: {
        width: 20,
        height: 20
    },
    btnAddvip: {
        borderRadius: 30,
        backgroundColor: '#E5FFCC',
        marginBottom: 20,
        width: "100%",
        marginTop: 10
      },
      label: {
        textAlign: 'center',
        fontSize: 18,
        fontWeight: '400',
        fontFamily: 'HelveticaNeue',
        padding: 20,
        color: '#000'
      }
})
