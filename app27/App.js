/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React from 'react';
 import { createStackNavigator } from '@react-navigation/stack';
 import { NavigationContainer } from '@react-navigation/native';
 import {
   SafeAreaView,
   StyleSheet,
   ScrollView,
   View,
   Text,
   StatusBar,
 } from 'react-native';
 import Home from './src/Home';
 import Detail from './src/Detail';
 import {ThanhToan} from './billing/index'
 
 const Stack = createStackNavigator();
 
 const App= () => {
   return (
     <>
       <NavigationContainer>
         <Stack.Navigator>
           <Stack.Screen name="Home" options={{ title: 'Home' }} component={Home} />
           <Stack.Screen name="Detail" options={{ title: 'Detail' }} component={Detail} />
           <Stack.Screen name="ThanhToan" options={{ title: 'ThanhToan' }} component={ThanhToan} />
         </Stack.Navigator>
       </NavigationContainer>
     </>
   );
 };
 
 
 export default App;
 