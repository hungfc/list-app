import React, { Component } from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import {colors} from '../../config/colors';
import {PrimaryButton} from '../../components/buttons/PrimaryButton';
export class GettingStarted extends Component {
  render() {
  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.btn} title="ADD VIP" onPress={() => this.props.navigation.navigate('ThanhToan')}><Text style={styles.label}>ADD VIP</Text></TouchableOpacity>
      <View style={styles.logoWrapper}>
        <Image
          style={styles.logoImage}
          source={require('../../../assets/images/logoWhite.png')}
        />
      </View>
      <View style={styles.welcomeMessage}>
        <Text style={styles.heading}>Hi Bach, Welcome</Text>
        <Text style={styles.subHead}>to Workflow Management</Text>
        <Text style={styles.description}>
          Explore the app, Find some peace of mind to prepare for meditation.
        </Text>
      </View>
      <View style={styles.welcomeImageWrapper}>
        <Image
          style={styles.ellipse4}
          source={require('../../../assets/images/Ellipse4.png')}></Image>
        <Image
          style={styles.ellipse3}
          source={require('../../../assets/images/Ellipse3.png')}></Image>
        <Image
          style={styles.ellipse2}
          source={require('../../../assets/images/Ellipse2.png')}></Image>
        <Image
          style={{
            position: 'absolute',
            top: '25%',
            alignSelf: 'center',
          }}
          source={require('../../../assets/images/Ellipse1.png')}></Image>
        <Image
          style={styles.yogi}
          source={require('../../../assets/images/yogi.png')}
        />

        <Image
          style={styles.cloud}
          source={require('../../../assets/images/cloud.png')}
        />
        <Image
          style={styles.partialCloud}
          source={require('../../../assets/images/partialCloud.png')}
        />
        <Image
          style={styles.smallEllipse1}
          source={require('../../../assets/images/smallEllipse1.png')}
        />
        <Image
          style={styles.smallEllipse2}
          source={require('../../../assets/images/smallEllipse2.png')}
        />

        <View style={styles.bgBtn}></View>
        <View style={styles.bgWrapper}>
          <PrimaryButton
            background={colors.whiteShadeBg}
            color={colors.heading}
            label={'GET STARTED'}
          />
        </View>
      </View>
    </View>
  );
        }
};

export const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
    padding: 15,
    backgroundColor: colors.primary,
  },
  logoWrapper: {
    marginTop: 30,
  },
  logoImage: {
    alignSelf: 'center',
  },
  btn: {
    borderRadius: 30,
    backgroundColor: '#6666FF'
  },
  label: {
    textAlign: 'center',
    fontSize: 18,
    fontWeight: '400',
    fontFamily: 'HelveticaNeue',
    padding: 20,
    color: '#fff'
  },
  welcomeMessage: {
    marginTop: 75,
  },
  heading: {
    fontFamily: 'HelveticaNeue',
    fontSize: 30,
    color: colors.whiteShade,
    fontWeight: '600',
    textAlign: 'center',
  },
  subHead: {
    fontFamily: 'HelveticaNeue',
    fontSize: 30,
    color: colors.whiteShade,
    marginTop: 10,
    textAlign: 'center',
  },
  description: {
    textAlign: 'center',
    color: colors.whiteShade,
    marginTop: 15,
    marginLeft: 20,
    marginRight: 20,
    fontSize: 16,
    lineHeight: 20,
  },
  welcomeImageWrapper: {
    position: 'absolute',
    bottom: '5%',
  },
  ellipse4: {
    marginTop: 30,
  },
  ellipse3: {
    position: 'absolute',
    top: '13%',
  },
  ellipse2: {
    position: 'absolute',
    top: '20%',
    alignSelf: 'center',
  },
  yogi: {
    position: 'absolute',
    top: '11%',
    alignSelf: 'center',
  },
  cloud: {
    right: 0,
    position: 'absolute',
    top: '15%',
  },
  partialCloud: {
    left: 0,
    position: 'absolute',
    top: '10%',
  },
  smallEllipse1: {
    left: 25,
    position: 'absolute',
    top: '10%',
  },
  smallEllipse2: {
    left: 20,
    position: 'absolute',
    top: '0%',
  },
  bgBtn: {
    backgroundColor: colors.primary,
    height: '40%',
    bottom: 0,
    width: '100%',
    position: 'absolute',
  },
  bgWrapper: {
    position: 'absolute',
    bottom: '10%',
    width: '90%',
    alignSelf: 'center',
  },
});
