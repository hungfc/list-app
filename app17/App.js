/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import {ChooseTopic} from './src/screens/ChooseTopic';
import {GettingStarted} from './src/screens/GettingStarted';
import {Login} from './src/screens/Login';
import { ThanhToan } from './billing';
const Stack = createStackNavigator();
class App extends Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Login" options={{ title: 'Login' }} component={Login} />
          <Stack.Screen name="ChooseTopic" options={{ title: 'ChooseTopic' }} component={ChooseTopic} />
          <Stack.Screen name="GettingStarted" component={GettingStarted} />
          <Stack.Screen name="ThanhToan" options={{ title: 'ThanhToan' }} component={ThanhToan} />
        </Stack.Navigator>
      </NavigationContainer>

    );
  }
};

export default App;