import React from 'react';
import {View, Text, StyleSheet, StatusBar, Image,TouchableOpacity} from 'react-native';
import colors from '../assets/colors/colors';

const Home = ({ navigation }) => {
  return (
    <View style={styles.slide}>
      <TouchableOpacity
              onPress={() =>
                navigation.navigate('ThanhToan')
              }><Text style={{ marginLeft: 230, fontFamily: "RobotoBold", backgroundColor: '#99CCFF', padding: 10, color: '#fff' }}>ADD VIP</Text>
                </TouchableOpacity>
        <Image source={require('../assets/images/Onboard1.png')} style={styles.image} />
        <View>
          <Text style={styles.title}>Save time by tracking your studies</Text>
          <Text style={styles.text}>Schedule your classes, assignments, quizzes and more</Text>
        </View>
        <View style={styles.rightTextWrapper}>
        <TouchableOpacity onPress={() =>
                navigation.navigate('Details')
              }>
          <Text style={styles.rightText}>Next</Text>
        </TouchableOpacity>
      </View>
      </View>
  );
};

const styles = StyleSheet.create({
  slide: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.white,
  },
  image: {
    marginVertical: 60,
  },
  title: {
    fontSize: 24,
    color: colors.black,
    textAlign: 'center',
    fontFamily: 'OpenSans-Bold',
    marginHorizontal: 60,
  },
  text: {
    fontSize: 14,
    color: colors.gray,
    textAlign: 'center',
    fontFamily: 'OpenSans-SemiBold',
    marginHorizontal: 60,
    marginTop: 20,
  },
  dotStyle: {
    backgroundColor: colors.blueFaded,
  },
  activeDotStyle: {
    backgroundColor: colors.blue,
  },
  rightTextWrapper: {
    width: 100,
    height: 60,
    marginRight: 20,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  rightText: {
    color: colors.blue,
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 14,
    padding: 10,
    backgroundColor: '#99CCFF'
  },
  leftTextWrapper: {
    width: 40,
    height: 40,
    marginLeft: 20,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  leftText: {
    color: colors.blue,
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 14,
  },
  doneButtonWrapper: {
    flex: 1,
    paddingLeft: 35,
    paddingRight: 50,
    paddingVertical: 10,
    borderRadius: 25,
    marginRight: -40,
  },
  doneButtonText: {
    fontSize: 14,
    fontFamily: 'OpenSans-SemiBold',
    textAlign: 'center',
    color: colors.white,
  },
});
export default Home;
