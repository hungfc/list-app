/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import type { Node } from 'react';

import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  FlatList
} from 'react-native';
import { ProductDetail, Billing } from './billing';
import { GioiThieu } from './gioithieu'
import { TrieuChung } from './trieuchung'
import { PhongChong } from './phongchong'
import { Home } from './home'
import { DieuTri } from './dieutri'

const Stack = createStackNavigator();
class App extends Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Home" options={{ title: 'Home' }} component={Home} />
          <Stack.Screen name="GioiThieu" options={{ title: 'GioiThieu' }} component={GioiThieu} />
          <Stack.Screen name="TrieuChung" options={{ title: 'TrieuChung' }} component={TrieuChung} />
          <Stack.Screen name="PhongChong" options={{ title: 'PhongChong' }} component={PhongChong} />
          <Stack.Screen name="DieuTri" options={{ title: 'DieuTri' }} component={DieuTri} />
          <Stack.Screen name="Billing" options={{ title: 'Billing' }} component={Billing} />
        </Stack.Navigator>
      </NavigationContainer>

    );
  }
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
