import * as React from 'react';
import { View, Text, FlatList, Dimensions, StyleSheet, TouchableOpacity, Button, Image} from 'react-native';

export class PhongChong extends React.Component {

  render() {
    return (
      <View style={styles.wraper}>
        <TouchableOpacity style={styles.btnVip} title="ADD VIP" onPress={() => this.props.navigation.navigate('Billing')}><Text style={styles.textBtn}>ADD VIP</Text></TouchableOpacity>
        <Text style={styles.textBtn}>Cách Phòng Chống</Text>
        <Text style={styles.base}>Điều quan trọng cần lưu ý là thực hiện các biện pháp phòng ngừa để giảm nguy cơ lây nhiễm. Mặc dù chưa được chứng minh sẽ bảo vệ được khỏi virus corona nhưng đeo khẩu trang là điều cần thiết sẽ giúp ngăn chặn cá nhân lây nhiễm cho người khác.</Text>
        <Text style={styles.base}>Điều quan trọng hơn là đảm bảo rằng chúng ta luôn giữ cho tay của mình sạch sẽ. Chúng ta chạm vào rất nhiều đồ vật trong ngày và chắc chắn chúng ta chạm vào mặt hoặc thậm chí chạm vào các thành viên trong gia đình. Đây là cách virus và vi khuẩn lây lan. Dưới đây là một số biện pháp phòng ngừa phổ biến khác mà bạn có thể thực hiện để giúp ngăn ngừa sự lây lan của virus gây bệnh viêm đường hô hấp.</Text>
        <Text style={styles.base}>Rửa tay thường xuyên bằng xà phòng và nước trong ít nhất 20 giây, đặc biệt là sau khi đi vệ sinh; trước khi ăn; và sau khi xì mũi, ho hoặc hắt hơi</Text>
        <Text style={styles.base}>Nếu không có sẵn xà phòng và nước, hãy sử dụng chất khử trùng tay chứa cồn với ít nhất 60% cồn. Luôn rửa tay bằng xà phòng và nước nếu tay bẩn, và có thể nhĩn thấy được vết bẩn.</Text>
        <Text style={styles.base}>Tránh chạm vào mắt, mũi và miệng bằng tay không rửa sạch.</Text>
        <Text style={styles.base}>Tránh tiếp xúc gần với người bị bệnh.</Text>
        <Text style={styles.base}>Che miệng khi ho hoặc hắt hơi bằng khăn giấy, sau đó bỏ khăn giấy vào thùng rác.</Text>
        <Text style={styles.base}>Khử trùng các đồ vật và bề mặt thường xuyên chạm vào.</Text>
        <Text style={styles.base}>Hãy chắc chắn rằng thịt và trứng được nấu chín kỹ.</Text>
        <Text style={styles.base}>Tránh hoặc hạn chế đi chợ.</Text>
      </View>
        
    );
  }
}

const styles = StyleSheet.create({
  wraper: {
    width: '100%',
    height: '100%',
    backgroundColor: '#EDBB99',
  },
  container: {
    flex: 1,
    marginVertical: 20,
  },
  image: {
    height: 230,
    width: 180,
    margin: 15
  },
  base: {
    fontWeight: 'bold',
    fontSize: 17,
    lineHeight: 23,
    padding: 15
  },
  item: {
    backgroundColor: '#EDBB99',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    margin: 1,
    marginBottom: 1,
    height: '100%',
  },
  itemInvisible: {
    backgroundColor: 'transparent',
  },
  btnAdd: {
    marginTop: 45,
    padding: 15,
    width: 100,
    backgroundColor: '#A04000',
  },
  textBtn: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  btnVip1: {
    padding: 15,
    width: '100%',
    backgroundColor: '#D89460',
  },
  btnVip: {
    padding: 15,
    marginBottom: 15,
    width: '100%',
    backgroundColor: '#A04000',
  }
});