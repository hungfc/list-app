import * as React from 'react';
import { View, Text, StyleSheet, TouchableOpacity} from 'react-native';

export class Home extends React.Component {
  render() {
    return (
      <View style={styles.wraper}>
        <TouchableOpacity style={styles.btnVip} title="ADD VIP" onPress={() => this.props.navigation.navigate('Billing')}><Text style={styles.textBtn}>ADD VIP</Text></TouchableOpacity>
        <View
        style={styles.item}
      >
        <TouchableOpacity style={styles.btnAdd} title="GioiThieu" onPress={() => this.props.navigation.navigate('GioiThieu')}><Text style={styles.textBtn}>Giới Thiệu</Text></TouchableOpacity>
        <TouchableOpacity style={styles.btnAdd} title="TrieuChung" onPress={() => this.props.navigation.navigate('TrieuChung')}><Text style={styles.textBtn}>Triệu Trứng</Text></TouchableOpacity>
        <TouchableOpacity style={styles.btnAdd} title="PhongChong" onPress={() => this.props.navigation.navigate('PhongChong')}><Text style={styles.textBtn}>Phòng chống</Text></TouchableOpacity>
        <TouchableOpacity style={styles.btnAdd} title="DieuTri" onPress={() => this.props.navigation.navigate('DieuTri')}><Text style={styles.textBtn}>Điều trị</Text></TouchableOpacity>
        </View>
      </View>
        
    );
  }
}

const styles = StyleSheet.create({
  wraper: {
    width: '100%',
    height: '100%',
    backgroundColor: '#EDBB99',
  },
  container: {
    flex: 1,
    marginVertical: 20,
  },
  image: {
    height: 230,
    width: 180,
    margin: 15
  },
  base: {
    fontWeight: 'bold',
    fontSize: 17,
    lineHeight: 23
  },
  item: {
    backgroundColor: '#EDBB99',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    margin: 1,
    marginBottom: 1,
    height: '100%',
  },
  itemInvisible: {
    backgroundColor: 'transparent',
  },
  btnAdd: {
    marginTop: 45,
    padding: 15,
    width: 300,
    backgroundColor: '#A04000',
  },
  textBtn: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  btnVip1: {
    padding: 15,
    width: '100%',
    backgroundColor: '#A04000',
  },
  btnVip: {
    padding: 15,
    width: '100%',
    backgroundColor: '#A04000',
  }
});