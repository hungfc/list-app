import * as React from 'react';
import { View, Text, FlatList, Dimensions, StyleSheet, TouchableOpacity, Button, Image} from 'react-native';

export class TrieuChung extends React.Component {
  render() {
    return (
      <View style={styles.wraper}>
        <TouchableOpacity style={styles.btnVip} title="ADD VIP" onPress={() => this.props.navigation.navigate('Billing')}><Text style={styles.textBtn}>ADD VIP</Text></TouchableOpacity>
        <Text style={styles.textBtn}>Triệu Chứng</Text>
        <Text style={styles.base}>Đối với các trường hợp được xác nhận nhiễm COVID-19,  các báo cáo cho thấy các triệu chứng bệnh khá đa dạng từ nhẹ hoặc không có triệu chứng biểu hiện rõ nét nào, cho đến những trường hợp bị ảnh hưởng khá nghiêm trọng. Các triệu chứng có thể bao gồm: Sốt, Ho, Khó thở, Mệt mỏi, Mất khứu giác. Trong một vài trường hợp, virus Corona không có triệu chứng sốt, cảm lạnh thông thường, nhưng vẫn tồn tại trong cơ thể.Một số người, kể cả những người có triệu chứng nhẹ hoặc không có triệu chứng có thể bị hội chứng hậu COVID - hoặc "di chứng COVID".Những người cao tuổi hoặc người có các bệnh lý nền nhất định có nguy cơ cao mắc bệnh nghiêm trọng do COVID-19.</Text>
      </View>
        
    );
  }
}

const styles = StyleSheet.create({
  wraper: {
    width: '100%',
    height: '100%',
    backgroundColor: '#EDBB99',
  },
  container: {
    flex: 1,
    marginVertical: 20,
  },
  image: {
    height: 230,
    width: 180,
    margin: 15
  },
  base: {
    fontWeight: 'bold',
    fontSize: 17,
    lineHeight: 23,
    padding: 15
  },
  item: {
    backgroundColor: '#EDBB99',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    margin: 1,
    marginBottom: 1,
    height: '100%',
  },
  itemInvisible: {
    backgroundColor: 'transparent',
  },
  btnAdd: {
    marginTop: 45,
    padding: 15,
    width: 100,
    backgroundColor: '#A04000',
  },
  textBtn: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  btnVip1: {
    padding: 15,
    width: '100%',
    backgroundColor: '#D89460',
  },
  btnVip: {
    padding: 15,
    marginBottom: 15,
    width: '100%',
    backgroundColor: '#A04000',
  }
});