import * as React from 'react';
import { View, Text, FlatList, Dimensions, StyleSheet, TouchableOpacity, Button, Image} from 'react-native';

export class GioiThieu extends React.Component {

  render() {
    return (
      <View style={styles.wraper}>
        <TouchableOpacity style={styles.btnVip} title="ADD VIP" onPress={() => this.props.navigation.navigate('Billing')}><Text style={styles.textBtn}>ADD VIP</Text></TouchableOpacity>
        <Text style={styles.textBtn}>Giới Thiệu</Text>
        <Text style={styles.base}>Có rất nhiều chủng loại virus Corona. Virus Corona là một nhóm loại virus gây ảnh hưởng đến hệ hô hấp của các loại động có vú, bao gồm cả con người.COVID-19 là một loại virus (cụ thể hơn là virus Corona) được xác định là nguyên nhân gây ra dịch bệnh suy hô hấp được phát hiện ở thành phố Vũ Hán, Trung Quốc. Ngay từ những ngày đầu, nhiều bệnh nhân trong vụ dịch ở Vũ Hán, Trung Quốc được cho là có mối liên hệ với một chợ buôn bán hải sản và động vật lớn tại địa phương, điều đó cho thấy có sự lây lan từ động vật sang người. Sau đó, ngày càng nhiều bệnh nhân được báo cáo là không tiếp xúc với chợ hải sản, chứng minh thêm cho việc có sự lây lan từ người sang người. Tại thời điểm này, mức độ lây nhiễm dễ dàng và bền vững từ người sang người của chủng virus này vẫn chưa được xác định.</Text>
      </View>
        
    );
  }
}

const styles = StyleSheet.create({
  wraper: {
    width: '100%',
    height: '100%',
    backgroundColor: '#EDBB99',
  },
  container: {
    flex: 1,
    marginVertical: 20,
  },
  image: {
    height: 230,
    width: 180,
    margin: 15
  },
  base: {
    fontWeight: 'bold',
    fontSize: 17,
    lineHeight: 23,
    padding: 15
  },
  item: {
    backgroundColor: '#EDBB99',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    margin: 1,
    marginBottom: 1,
    height: '100%',
  },
  itemInvisible: {
    backgroundColor: 'transparent',
  },
  btnAdd: {
    marginTop: 45,
    padding: 15,
    width: 100,
    backgroundColor: '#A04000',
  },
  textBtn: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  btnVip1: {
    padding: 15,
    width: '100%',
    backgroundColor: '#D89460',
  },
  btnVip: {
    padding: 15,
    marginBottom: 15,
    width: '100%',
    backgroundColor: '#A04000',
  }
});