import * as React from 'react';
import { View, Text, FlatList, Dimensions, StyleSheet, TouchableOpacity, Button, Image} from 'react-native';

export class DieuTri extends React.Component {

  render() {
    return (
      <View style={styles.wraper}>
        <TouchableOpacity style={styles.btnVip} title="ADD VIP" onPress={() => this.props.navigation.navigate('Billing')}><Text style={styles.textBtn}>ADD VIP</Text></TouchableOpacity>
        <Text style={styles.textBtn}>Cách Điều Trị</Text>
        <Text style={styles.base}>Theo các bác sĩ, các F0 không triệu chứng phải tuân thủ chặt chẽ quy định tự cách ly ở khu vực riêng biệt, tránh tiếp xúc với người xung quanh, sát khuẩn hàng ngày và đảm bảo kết nối thường xuyên với nhân viên y tế để được theo dõi. Ngoài ra, người bệnh cần lưu ý ăn uống đủ dinh dưỡng tăng cường sức đề kháng và chuẩn bị sẵn các loại thuốc thiết yếu</Text>
        <Text style={styles.base}>Các F0 điều trị tại nhà nói chung và F0 không triệu chứng nói riêng cần dự phòng một số loại thuốc và vật tư y tế cần thiết.</Text>
        <Text style={styles.base}>Theo đó, thuốc dự phòng gồm: Các thuốc hạ sốt: Efferalgan, Panadol...; Nhóm thuốc chữa ho; Nhóm thuốc tiêu chảy; Nước súc miệng; Cồn sát trùng; Các thuốc cho bệnh nền (nên chuẩn bị đủ cho 4 tuần); Thuốc xịt mũi các loại; Vitamine C, kẽm, các loại thảo dược trị cảm, ho.</Text>
        <Text style={styles.base}>Cùng với đó là nước uống thông thường, nước bù điện giải (rất quan trọng khi sốt và mắc COVID-19). Ngoài ra, người bệnh cần uống đủ lượng nước để duy trì sự ổn định của niêm mạc mũi, giảm kích ứng khó chịu khi ho, hắt hơi hay thậm chí là thở. Độ ẩm này giúp bề mặt niêm mạc dễ lành hơn và chống lại sự xâm nhập thêm của vi khuẩn.</Text>
      </View>
        
    );
  }
}

const styles = StyleSheet.create({
  wraper: {
    width: '100%',
    height: '100%',
    backgroundColor: '#EDBB99',
  },
  container: {
    flex: 1,
    marginVertical: 20,
  },
  image: {
    height: 230,
    width: 180,
    margin: 15
  },
  base: {
    fontWeight: 'bold',
    fontSize: 17,
    lineHeight: 23,
    padding: 15
  },
  item: {
    backgroundColor: '#EDBB99',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    margin: 1,
    marginBottom: 1,
    height: '100%',
  },
  itemInvisible: {
    backgroundColor: 'transparent',
  },
  btnAdd: {
    marginTop: 45,
    padding: 15,
    width: 100,
    backgroundColor: '#A04000',
  },
  textBtn: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  btnVip1: {
    padding: 15,
    width: '100%',
    backgroundColor: '#D89460',
  },
  btnVip: {
    padding: 15,
    marginBottom: 15,
    width: '100%',
    backgroundColor: '#A04000',
  }
});