import {
  Alert,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';
import RNIap, {
  InAppPurchase,
  PurchaseError,
  SubscriptionPurchase,
  acknowledgePurchaseAndroid,
  consumePurchaseAndroid,
  finishTransaction,
  finishTransactionIOS,
  purchaseErrorListener,
  purchaseUpdatedListener,
  
} from 'react-native-iap';
import React, { Component } from 'react';

import Button from 'apsl-react-native-button';

const itemSkus = [
  'com.backgroundimage.hxsaggnxem.1',
  'com.backgroundimage.hxsaggnxem.2',
  'com.backgroundimage.hxsaggnxem.3',
  'com.backgroundimage.hxsaggnxem.4',
  'com.backgroundimage.hxsaggnxem.5',
  'com.backgroundimage.hxsaggnxem.6',
];

const itemSubs = [
  'com.backgroundimage.hxsaggnxem.sub.1', // subscription
  'com.backgroundimage.hxsaggnxem.sub.2',
  'com.backgroundimage.hxsaggnxem.sub.3',
  'com.backgroundimage.hxsaggnxem.sub.4',
  'com.backgroundimage.hxsaggnxem.sub.5',
];

let purchaseUpdateSubscription;
let purchaseErrorSubscription;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: Platform.select({
      ios: 0,
      android: 4,
    }),
    paddingTop: Platform.select({
      ios: 0,
      android: 4,
    }),
    backgroundColor: 'white',
  },
  header: {
    flex: 20,
    flexDirection: 'row',
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerTxt: {
    fontSize: 26,
    color: 'green',
  },
  image: {
    width: 100,
    height: 40,
    resizeMode: 'contain' 
  },
  content: {
    flex: 80,
    flexDirection: 'column',
    justifyContent: 'center',
    alignSelf: 'stretch',
    alignItems: 'center',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  btn: {
    height: 55,
    width: 90,
    alignSelf: 'center',
    backgroundColor: '#00c40f',
    borderRadius: 0,
    borderWidth: 0,
  },
  btnProduct: {
    height: 55,
    width: 200,
    alignSelf: 'center',
    backgroundColor: '#2547be',
    borderRadius: 0,
    borderWidth: 0,
  },
  txt: {
    fontSize: 16,
    color: 'white',
    fontWeight: 'bold'
  },
});

export class Billing extends Component {
  constructor(props) {
    super(props);

    this.state = {
      unconsumed:null,
      productList: [],
      productSubList: [],
      receipt: '',
      availableItemsMessage: '',
    };
  }

  async componentDidMount() {
    try {
      await RNIap.initConnection();
      await RNIap.flushFailedPurchasesCachedAsPendingAndroid();
    } catch (err) {
      console.warn(err.code, err.message);
    }

    purchaseUpdateSubscription = purchaseUpdatedListener(
      async (purchase) => {
        console.info('purchase', purchase);
        const receipt = purchase.transactionReceipt
          ? purchase.transactionReceipt
          : purchase.originalJson;
        console.info(receipt);
        if (receipt) {
          try {
            const ackResult = await finishTransaction(purchase,true);
            console.info('ackResult', ackResult);
          } catch (ackErr) {
            console.warn('ackErr', ackErr);
          }

          this.setState({ receipt }, () => this.goNext());
        }
      },
    );

    purchaseErrorSubscription = purchaseErrorListener(
      (error) => {
        console.log('purchaseErrorListener', error);
        Alert.alert('purchase error', JSON.stringify(error));
      },
    );
    this.getItems();
    this.getSubscriptions();
  }

  componentWillUnmount() {
    if (purchaseUpdateSubscription) {
      purchaseUpdateSubscription.remove();
      purchaseUpdateSubscription = null;
    }
    if (purchaseErrorSubscription) {
      purchaseErrorSubscription.remove();
      purchaseErrorSubscription = null;
    }
    RNIap.endConnection();
  }

  goNext = async() => {

    Alert.alert('Thanh toán thành công. Receipt: ', this.state.receipt);
  };

  getItems = async () => {
    try {
      const products = await RNIap.getProducts(itemSkus);
      // const products = [{productId:'',originalPriceAndroid: '22',currency: '11'},{productId:'',originalPriceAndroid: '22',currency: '11'}];
      console.log('Products', products);
      this.setState({ productList: products });
    } catch (err) {
      console.warn(err.code, err.message);
    }
  };

  getSubscriptions = async () => {
    try {
      const products = await RNIap.getSubscriptions(itemSubs);
      console.log('Products sub', products);
      this.setState({ productSubList: products });
    } catch (err) {
      console.warn(err.code, err.message);
    }
  };

  getAvailablePurchases = async () => {
    try {
      console.log('init')
      console.info(
        'Get available purchases (non-consumable or unconsumed consumable)',
      );
      const purchases = await RNIap.getAvailablePurchases();
      console.info('Available purchases :: ', purchases);

      if (purchases && purchases.length > 0) {
        this.consumePurchaseAndroid(purchases);
      }
    } catch (err) {
      console.warn(err.code, err.message);
      Alert.alert(err.message);
    }
  };

  async consumePurchaseAndroid(purchases){
    for(var i=0;i<purchases.length;i++){
      var pur=purchases[i];
      await RNIap.finishTransaction(pur,true);
    }
      
  }

  // Version 3 api
  requestPurchase = async (sku) => {
    try {
      var s = await RNIap.requestPurchase(sku, false);
      console.log(s)
    } catch (err) {
      console.warn(err.code, err.message);
    }
  };

  requestSubscription = async (sku) => {
    try {
      RNIap.requestSubscription(sku);
    } catch (err) {
      Alert.alert(err.message);
    }
  };

  render() {
    const { productList, receipt, availableItemsMessage, productSubList } = this.state;
    

    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <ScrollView style={{ alignSelf: 'stretch'}}>
            {productSubList && productSubList.length > 0 ? <Text style={{ textAlign: "center", fontSize: 18, fontWeight: 'bold',marginBottom: 10 }}>Subscriptions</Text> : null}
            <View style={{
                    flexDirection: 'row',
                    backgroundColor: '#cae4e3',
                    justifyContent: 'space-around',
                    alignItems: 'center',
                    width: '100%',
                    height: 100
                  }}>
            {productSubList.map((product, i) => {
              return (
                <View key={i}>
                  <Button
                    onPress={() =>
                      this.requestSubscription(product.productId)
                    }
                    activeOpacity={0.6}
                    style={styles.btn}
                    textStyle={styles.txt}>
                    {"Sub" + (i + 1)}
                  </Button>
                </View>
              );
            })}
            </View>
            {productList && productList.length > 0 ? <Text style={{ textAlign: "center", fontSize: 16, fontWeight: 'bold', paddingBottom: 10, paddingTop: 20 }}>List products</Text> : null}
            {productList.map((product, i) => {
              return (
                <View
                  key={i}
                  style={{
                    flexDirection: 'column',
                    backgroundColor: '#cae4e3',
                    justifyContent: 'center',
                    alignItems: 'center',
                    padding: 10,
                    margin: 4
                  }}>
                  <Image style={styles.image} source={require('../image/gold.png')}/>
                  <Text
                    style={{
                      fontSize: 18,
                      color: 'black',
                      fontWeight: 'bold',
                      alignSelf: 'center',
                      paddingHorizontal: 0,
                      marginVertical: 10
                    }}>
                    ({product.originalPriceAndroid} {product.currency})
                  </Text>
                  <Button
                    onPress={() =>
                      this.requestPurchase(product.productId)
                    }
                    activeOpacity={0.6}
                    style={styles.btnProduct}
                    textStyle={styles.txt}>
                    BUY PRODUCT
                  </Button>
                </View>
              );
            })}
            
          </ScrollView>
        </View>
      </View>
    );
  }
}
