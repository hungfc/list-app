import * as React from 'react';
import { View, Text, FlatList, Dimensions, StyleSheet, Image, Button, TouchableOpacity,} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Detail from './detail';

const data = [
  {
    id: '1',
    name: 'Chevrolet',
    img: require('./image/img-1.jpg'),
    pixel: '1600x900',
    size: '1,5MB',
    price: '5$',
    format: 'JPG'
  },
  {
      id: '2',
      name: 'Mazda',
      img: require('./image/img-2.jpg'),
      pixel: '1600x900',
      size: '1,55MB',
      price: '2$',
      format: 'JPG'
  },
  {
      id: '3',
      name: 'Ford',
      img: require('./image/img-3.jpg'),
      pixel: '1600x900',
      size: '2,5MB',
      price: '1$',
      format: 'JPG'
  },
  {
      id: '4',
      name: 'Isuzu',
      img: require('./image/img-4.jpg'),
      pixel: '1600x900',
      size: '1,5MB',
      price: '5$',
      format: 'JPG'
  },
  {
      id: '5',
      name: 'Mercedes',
      img: require('./image/img-5.jpg'),
      pixel: '1600x900',
      size: '1,5MB',
      price: '4$',
      format: 'PNG'
  },
  {
      id: '6',
      name: 'BMW',
      img: require('./image/img-7.jpg'),
      pixel: '1600x900',
      size: '1,5MB',
      price: '5$',
      format: 'JPG'
  },
  {
      id: '7',
      name: 'Audi',
      img: require('./image/img-7.jpg'),
      pixel: '1600x900',
      size: '2,5MB',
      price: '3$',
      format: 'JPG'
  },
  {
      id: '8',
      name: 'Lamborghini',
      img: require('./image/img-8.jpg'),
      pixel: '1600x900',
      size: '1,5MB',
      price: '2$',
      format: 'JPG'
  },
  {
      id: '9',
      name: 'Chevrolet',
      img: require('./image/img-9.jpg'),
      pixel: '1600x900',
      size: '1,5MB',
      price: '6$',
      format: 'JPG'
  },
  {
      id: '10',
      name: 'Volvo',
      img: require('./image/img-10.jpg'),
      pixel: '1600x900',
      size: '1,5MB',
      price: '2$',
      format: 'PNG'
  },
  {
      id: '11',
      name: 'Jaguar',
      img: require('./image/img-11.jpg'),
      pixel: '1600x900',
      size: '3,5MB',
      price: '3$',
      format: 'JPG'
  },
  {
      id: '12',
      name: 'Chevrolet',
      img: require('./image/img-12.jpg'),
      pixel: '1600x900',
      size: '1,5MB',
      price: '1$',
      format: 'PNG'
  },
];

const formatData = (data, numColumns) => {
  const numberOfFullRows = Math.floor(data.length / numColumns);

  let numberOfElementsLastRow = data.length - (numberOfFullRows * numColumns);
  while (numberOfElementsLastRow !== numColumns && numberOfElementsLastRow !== 0) {
    data.push({ id: `blank-${numberOfElementsLastRow}`, empty: true });
    numberOfElementsLastRow++;
  }

  return data;
};

const numColumns = 2;

export class Home extends React.Component {
  renderItem = ({ item, index }) => {
    if (item.empty === true) {
      return <View style={[styles.item, styles.itemInvisible]} />;
    }
    return (
      <View
        style={styles.item}
      >
        <Image style={styles.img} source={item.img}/>
        <Text numberOfLines={1} style={styles.itemText}>{item.name}</Text>
        <Text style={{ color: 'green', marginBottom: 2 }}>Price: {item.price}</Text>
        <View style={[{ width: "60%", margin: 10, backgroundColor: "red" }]}>
          <Button title="Detail" onPress={() => this.props.navigation.navigate('Detail',{pItem:item})}/>
        </View>
      </View>
    );
  };

  render() {
    return (
      <View style={styles.wrap}>
        <TouchableOpacity style={styles.btnAdd} title="ADD VIP" onPress={() => this.props.navigation.navigate('Billing')}><Text style={styles.textBtn}>BUY VIP</Text></TouchableOpacity>
        <FlatList
          data={formatData(data, numColumns)}
          style={styles.container}
          renderItem={this.renderItem}
          numColumns={numColumns}
        />
      </View>
        
    );
  }
}

const styles = StyleSheet.create({
  wrap: {
    width: '100%',
    height: '100%'
  },
  container: {
    flex: 1,
    marginVertical: 20,
  },
  img: {
    width: '90%',
    height: '70%'
  },
  item: {
    backgroundColor: '#cae4e3',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    margin: 1,
    marginBottom: 1,
    height: 400,
  },
  itemInvisible: {
    backgroundColor: 'transparent',
  },
  textBtn: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  itemText: {
    color: '#000',
    fontWeight: 'bold',
    marginBottom: 6,
    marginTop: 5
  },
  btnAdd: {
    padding: 15,
    backgroundColor: '#3492ef',
  }
});