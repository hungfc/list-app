
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';

import {
    StyleSheet,
    TouchableOpacity,
    Text,
    Image,
    useColorScheme,
    View,
    FlatList,
    Button
} from 'react-native';

export class Detail extends Component {
    render() {
        const{pItem} =this.props.route.params;
        return (
            <View style={{width: '100%', height: 250}}>
              <View style={styles.wraplayout}>
                <View style={{width: '40%'}}>
                  <Image style={{width: '100%', height: 250}} source={pItem.img}/>
                </View>
                <View style={{width: '60%', paddingLeft: 10}}>
                  <Text style={styles.baseText} numberOfLines={2}>Name Image: {pItem.name}</Text>
                  <Text style={styles.baseText}>Display Resolution: {pItem.pixel}</Text>
                  <Text style={styles.baseText}>Size: {pItem.size}</Text>
                  <Text style={styles.baseText}>Type Of File: {pItem.format}</Text>
                </View>
              </View>
              <TouchableOpacity style={styles.btnAdd} title="ADD VIP" onPress={() => this.props.navigation.navigate('Billing')}><Text style={styles.textBtn}>BUY</Text></TouchableOpacity>
            </View>
        );
    }
};

const styles = StyleSheet.create({
    baseText: {
      fontWeight: 'bold',
      fontSize: 18,
    },
    wraplayout: {
      flexDirection: 'row',
      paddingRight:10,
      paddingLeft: 10,
      paddingTop: 10
    },
    textBtn: {
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center'
      },
      btnAdd: {
        padding: 15,
        backgroundColor: '#3492ef',
        width: '90%',
        alignSelf: 'center',
        marginTop: 20
      }
});