import React from 'react';
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Image,
    Button
} from 'react-native';
import { TouchableHighlight } from 'react-native-gesture-handler';

const woolyImages = [
    require("../image/img-1.jpg"),
    require("../image/img-2.jpg"),
    require("../image/img-3.jpg"),
    require("../image/img-4.jpg"),
    require("../image/img-5.jpg"),
    require("../image/img-6.jpg"),
    require("../image/img-7.jpg"),
    require("../image/img-8.jpg"),
    require("../image/img-9.jpg"),
    require("../image/img-10.jpg"),
    require("../image/img-11.jpg"),
    require("../image/img-12.jpg"),
  ];

Number.prototype.format = function(n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};
export class Product extends React.Component {
    capFirst(string) {
        if(!string){
            string ="";
        }
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    getRandomInt(min, max) {
          return Math.floor(Math.random() * (max - min)) + min;
    }

    generateName() {
        var name1=['Toyota',"Mercedes","Audi","adorable","Lamborghini","Volvo","Jaguar","Bentley","Mitsubishi","Chevrolet"];
        var name2=["1.0","2.0","3.0","4.0","2.1","2.3","3.2","4.1","2.5","3.5","2.3","4.5"];

        var name = this.capFirst(name1[this.getRandomInt(0, name1.length + 1)]) + ' ' + this.capFirst(name2[this.getRandomInt(0, name2.length + 1)]);
        return name;
    
    }

    generateImage() {
        var randomImage = woolyImages[Math.floor(Math.random() * woolyImages.length)];
        return randomImage;
    }
    randomIntFromInterval() {
        return Math.floor(Math.random() * 10)
    }
    render() {
        return (<View style={{ margin: 4, width: '46%' }}>
            <Image style={{ width: '100%', height: 300, }}
                resizeMode={"contain"}
                source={this.generateImage()}>
            </Image>
            <Text style={{ color: 'black', textAlign: 'center' }}>{this.generateName()}</Text>
            <Text style={{ color: 'green', textAlign: 'center' }}>Price: {this.randomIntFromInterval().format()} USD</Text>
            <Button onPress={() => {
                this.props.navigation.navigate('Detail')
            }} title={"Detail"}>

            </Button>
        </View>)
    }
}
