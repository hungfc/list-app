import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    View,
    Button,
    TextInput,
    Alert
} from 'react-native';

export class ChiTieu extends Component {

    state = {
        myTextInput: '',
        users: ['ĐI CHỢ  500K','DU LỊCH 5tr','MUA QUẦN ÁO  1Tr500','MUA TIVI 20Tr']
    }

    onPssassas = (event) => {
        this.setState({
            myTextInput:event
        })
    }

    onKdddd = () => {
        this.setState(prevState => {
            return {
                myTextInput: '',
                users: [prevState.myTextInput, ...prevState.users]
            }
        })

        Alert.alert('Success')
    }
    render() {
        return (
            <View style={styles.ghfgDrty}>
                <Button 
                activeOpacity={0.5}
                color='#3499FF'
                style={[styles.btn,{width:'100%', marginBottom: 50}]}
                onPress={()=>{
                this.props.navigation.navigate('ThanhToan')
                }}
                title="MUA VIP">
                </Button>
                <View style={styles.bmmSzxz}>
                    <TextInput
                        value={this.state.myTextInput}
                        style={styles.input}
                        onChangeText={this.onPssassas}
                    />
                    <TouchableOpacity style={[{ width: "30%", margin: 10, padding: 14, backgroundColor: '#3499FF' }]}
                        title="THEM"
                        onPress={this.onKdddd}
                    ><Text style={styles.mkDfgfff}>THÊM</Text>
                    </TouchableOpacity>
                </View>
                {
                    this.state.users.map( item => (
                        <Text style={styles.users} key={item}>{item}</Text>
                    ))
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    input: {
        fontSize:20,
        padding: 10,
        borderWidth: 1,
        width: '60%',
        backgroundColor: '#fff'
    },
    users: {
        padding: 10,
        marginBottom:10,
        backgroundColor: '#fff',
        borderWidth: 1,
        fontSize: 30,
        borderColor:'#cecece'
    },
    ghfgDrty: {
      width: '100%',
      backgroundColor:'#bdd7f0',
      height: '100%'
    },
    bmmSzxz: {
        display: 'flex',
        width: '100%',
        height: 100,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    mkDfgfff: {
      color: 'white',
      fontSize: 18,
      fontWeight: 'bold',
      textAlign: 'center'
    },
    cvvvvAuuu: {
      padding: 15,
      backgroundColor: '#bdd7f0',
    },
    btn: {
        width: '100%'
    }
  });
