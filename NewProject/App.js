// In App.js in a new project

import * as React from 'react';
import { View, Text, FlatList, Dimensions, StyleSheet, Image, Button} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Home from './Home';
import Detail from './Detail';
import Billing from './Billing';

  

// function HomeScreen() {
//   return (
//     <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
//       <Text>Home Screen</Text>
//     </View>
//   );
// }

const Stack = createNativeStackNavigator();

class App extends React.Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Home" navigation={this.props.navigation} component={Home} />
          <Stack.Screen name="Detail" component={Detail} />
          <Stack.Screen name="Billing" component={Billing} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

export default App;