// In Home.js in a new project

import * as React from 'react';
import { View, Text, FlatList, 
    Dimensions, StyleSheet, 
    Image, Button,TouchableOpacity,Alert } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import RNIap, {
    InAppPurchase,
    PurchaseError,
    SubscriptionPurchase,
    acknowledgePurchaseAndroid,
    consumePurchaseAndroid,
    finishTransaction,
    finishTransactionIOS,
    purchaseErrorListener,
    purchaseUpdatedListener,
  } from 'react-native-iap';

const itemSkus = [
    'android.test.purchased',
    'com.pack.13f2.vip1',
    'com.pack.13f2.vip2'
    // 'point_1000', '5000_point', // dooboolab
];

let purchaseUpdateSubscription;
let purchaseErrorSubscription;
class Billing extends React.Component {
    constructor(props) {
        super(props);
        // Don't call this.setState() here!
        this.state = { productList: [] };
    }
    requestPurchase = async (sku) =>{
        try {
          await RNIap.requestPurchase(sku.productId);
        } catch (err) {
          console.warn(err.code, err.message);
        }
      };
    renderItem = ({item}) =>{
        return(
            <TouchableOpacity onPress={()=>{
                console.log(item)
                this.requestPurchase(item)
            }}>
            <View style={styles.item}>
                <Text>{item.title}  {item.originalPriceAndroid}</Text>
            </View>
            </TouchableOpacity>)
    };
    async componentDidMount() {
        try {
            a = await RNIap.initConnection();
            if (Platform.OS === 'android') {
                await RNIap.flushFailedPurchasesCachedAsPendingAndroid();
            } else {
                await RNIap.clearTransactionIOS();
            }
        } catch (err) {
            alert('errr', err)
            console.warn(err.code, err.message);
        }
        purchaseUpdateSubscription = purchaseUpdatedListener(
            async (purchase) => {
                alert("Thanh toán thành công"+JSON.stringify(purchase))
                console.info('purchase', purchase);
                const receipt = purchase.transactionReceipt
                    ? purchase.transactionReceipt
                    : purchase.originalJson;
                console.info(receipt);
                if (receipt) {
                    try {
                        const ackResult = await finishTransaction(purchase);
                        // Alert.alert('ackResult',JSON.stringify(ackResult))
                        console.info('ackResult', ackResult);
                    } catch (ackErr) {
                        console.warn('ackErr', ackErr);
                    }

                    // this.setState({ receipt }, () => this.goNext());
                }
            },
        );

        purchaseErrorSubscription = purchaseErrorListener(
            (error) => {
                console.log('purchaseErrorListener', error);
                alert('Thanh toán lỗi', JSON.stringify(error));
            },
        );
        this.getItems();
    }

    getItems = async () => {
        try {
            const products = await RNIap.getProducts(itemSkus);
            // const products = await RNIap.getSubscriptions(itemSkus);
            console.log('Products', products);
            this.setState({ productList: products });
        } catch (err) {
            console.warn(err.code, err.message);
        }
    };

    render() {
        const{productList}=this.state;
        return (
            <View style={styles.itemwrap}>
                <FlatList
                    data={productList}
                    renderItem={this.renderItem}
                />
            </View>

        );
    }
}

const styles = StyleSheet.create({
    itemwrap: {
        width: '100%',
        height: '100%',
        padding: 20,
    },
    margin: {
        marginTop: 10
    },
    item: {
        backgroundColor: '#cae4e3',
        alignItems: 'center',
        justifyContent: 'center',
        height: 100,
        width: '100%',
        marginBottom: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
        borderRadius: 10,
    },
    image: {
        width: 100,
        height: 40,
        resizeMode: 'contain'
    },
    textBold: {
        fontWeight: 'bold',
        fontSize: 20
    }
});

export default Billing;