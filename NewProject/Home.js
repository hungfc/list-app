// In Home.js in a new project

import * as React from 'react';
import { View, Text, FlatList, Dimensions, StyleSheet, Image, Button} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Detail from './Detail';

  const data = [
    {
      id: '1',
      title: 'Chiêu Diêu',
      img: require('./assets/images/chieu-dieu.jpg'),
      price: '88.000đ',
      author: 'Bùi Đình Bách',
      category: 'Ngôn Tình'
    },
    {
      id: '2',
      title: 'Cửu Đỉnh Ký',
      img: require('./assets/images/cuu-dinh-ky.jpg'),
      price: '100000',
      author: 'Lê Anh Kỳ',
      category: 'Ngôn Tình'
    },
    {
      id: '3',
      title: 'Đế Vương Sủng Ái',
      img: require('./assets/images/de-vuong-sung-ai.jpg'),
      price: '100.000đ',
      author: 'Chu Chỉ Nhược',
      category: 'Ngôn Tình'
    },
    {
      id: '4',
      title: 'Dược Thần',
      img: require('./assets/images/duoc-than.jpg'),
      price: '120.000đ',
      author: 'Công Tôn Sách',
      category: 'Ngôn Tình'
    },
    {
      id: '5',
      title: 'Đường Chuyên',
      img: require('./assets/images/duong-chuyen.jpg'),
      price: '130.000đ',
      author: 'Lưu Bị',
      category: 'Ngôn Tình'
    },
    {
      id: '6',
      title: 'Gia Cát Linh Ân',
      img: require('./assets/images/gia-cat-linh-an.jpg'),
      price: '50.000đ',
      author: 'Tây Môn Khánh',
      category: 'Ngôn Tình'
    },
    {
      id: '7',
      title: 'Hào Môn Nhất Kiếm',
      img: require('./assets/images/hao-mon-nhat-kiem.jpg'),
      price: '110.000đ',
      author: 'Bùi Quang Sáng',
      category: 'Ngôn Tình'
    },
    {
      id: '8',
      title: 'Huyền Thiên Hồn Tôn',
      img: require('./assets/images/huyen-thien-hon-ton.jpg'),
      price: '100.000đ',
      author: 'Nguyễn Văn Ánh',
      category: 'Ngôn Tình'
    },
    {
      id: '9',
      title: 'Kiếm Nghịch Thượng Khung',
      img: require('./assets/images/kiem-nghich-thuong-khung.jpg'),
      price: '80.000đ',
      author: 'Nguyễn Văn An',
      category: 'Ngôn Tình'
    },
    {
      id: '10',
      title: 'Hi Du Hoa Tùng',
      img: require('./assets/images/phi-hi-du-hoa-tung.jpg'),
      price: '100.000đ',
      author: 'Nguyễn Nhật Ánh',
      category: 'Ngôn Tình'
    },
    {
      id: '11',
      title: 'Thiên Hạ Vô Song',
      img: require('./assets/images/thien-ha-vo-song.jpg'),
      price: '150.000đ',
      author: 'Nguyễn Quang Sáng',
      category: 'Ngôn Tình'
    },
  ];

const formatData = (data, numColumns) => {
  const numberOfFullRows = Math.floor(data.length / numColumns);

  let numberOfElementsLastRow = data.length - (numberOfFullRows * numColumns);
  while (numberOfElementsLastRow !== numColumns && numberOfElementsLastRow !== 0) {
    data.push({ id: `blank-${numberOfElementsLastRow}`, empty: true });
    numberOfElementsLastRow++;
  }

  return data;
};

const numColumns = 3;

class Home extends React.Component {
  renderItem = ({ item, index }) => {
    if (item.empty === true) {
      return <View style={[styles.item, styles.itemInvisible]} />;
    }
    return (
      <View
        style={styles.item}
      >
        <Image style={styles.img} source={item.img}/>
        <Text numberOfLines={1} style={styles.itemText}>{item.title}</Text>
        <Button title="Đặt mua" onPress={() => this.props.navigation.navigate('Detail',{pItem:item})}/>
      </View>
    );
  };

  render() {
    return (
      <View style={styles.wrap}>
        <Button title="Mua VIP" onPress={() => this.props.navigation.navigate('Billing')}/>
        <FlatList
          data={formatData(data, numColumns)}
          style={styles.container}
          renderItem={this.renderItem}
          numColumns={numColumns}
        />
      </View>
        
    );
  }
}

const styles = StyleSheet.create({
  wrap: {
    width: '100%',
    height: '100%'
  },
  container: {
    flex: 1,
    marginVertical: 20,
  },
  img: {
    width: '90%',
    height: '70%'
  },
  item: {
    backgroundColor: '#cae4e3',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    margin: 1,
    marginBottom: 10,
    height: 250,
  },
  itemInvisible: {
    backgroundColor: 'transparent',
  },
  itemText: {
    color: '#000',
    fontWeight: 'bold',
    marginBottom: 6,
    marginTop: 5
  },
});

export default Home;