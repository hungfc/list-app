// In Home.js in a new project

import * as React from 'react';
import { View, Text, FlatList, Dimensions, StyleSheet, Image, Button} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import RNIap, {
  InAppPurchase,
  PurchaseError,
  SubscriptionPurchase,
  acknowledgePurchaseAndroid,
  consumePurchaseAndroid,
  finishTransaction,
  finishTransactionIOS,
  purchaseErrorListener,
  purchaseUpdatedListener,
} from 'react-native-iap';
//test
const itemSkus =[
    'android.test.purchased',
    'android.test.canceled',
    'android.test.refunded',
    'android.test.item_unavailable',
    // 'point_1000', '5000_point', // dooboolab
  ];

let purchaseUpdateSubscription;
let purchaseErrorSubscription;

// const itemSubs = Platform.select({
//   ios: [
//     'com.cooni.point1000',
//     'com.cooni.point5000', // dooboolab
//   ],
//   android: [
//     'test.sub1', // subscription
//   ],
// });
class Detail extends React.Component {
  async componentDidMount(){
    try {
      a=await RNIap.initConnection();
      if (Platform.OS === 'android') {
        await RNIap.flushFailedPurchasesCachedAsPendingAndroid();
      } else {
        await RNIap.clearTransactionIOS();
      }
    } catch (err) {
      alert('errr',err)
      console.warn(err.code, err.message);
    }
    purchaseUpdateSubscription = purchaseUpdatedListener(
      async (purchase) => {
        console.info('purchase', purchase);
        const receipt = purchase.transactionReceipt
          ? purchase.transactionReceipt
          : purchase.originalJson;
        console.info(receipt);
        if (receipt) {
          try {
            const ackResult = await finishTransaction(purchase);
            console.info('ackResult', ackResult);
          } catch (ackErr) {
            console.warn('ackErr', ackErr);
          }

          this.setState({receipt}, () => this.goNext());
        }
      },
    );

    purchaseErrorSubscription = purchaseErrorListener(
      (error) => {
        console.log('purchaseErrorListener', error);
        alert('purchase error', JSON.stringify(error));
      },
    );
  }

  componentWillUnmount() {
    if (purchaseUpdateSubscription) {
      purchaseUpdateSubscription.remove();
      purchaseUpdateSubscription = null;
    }
    if (purchaseErrorSubscription) {
      purchaseErrorSubscription.remove();
      purchaseErrorSubscription = null;
    }
    RNIap.endConnection();
  }

  requestPurchase = async (sku) =>{
    try {

      const products = await RNIap.getProducts(itemSkus);

      await RNIap.requestPurchase(products[0].productId);
    } catch (err) {
      console.warn(err.code, err.message);
    }
  };

  render() {
    const{pItem} =this.props.route.params;
    debugger;
    return (
        <View style={{width: '100%', height: 250}}>
          <View style={styles.wraplayout}>
            <View style={{width: '40%'}}>
              <Image style={{width: '100%', height: 250}} source={pItem.img}/>
            </View>
            <View style={{width: '60%', paddingLeft: 10}}>
              <Text style={styles.baseText} numberOfLines={2}>Tên Truyện: {pItem.title}</Text>
              <Text style={styles.baseText}>Tác Giả: {pItem.author}</Text>
              <Text style={styles.baseText}>Thể Loại: {pItem.category}</Text>
              <Text style={styles.baseText}>Giá: {pItem.price}</Text>
            </View>
          </View>
          <Button style={{height: 200}} title={"Mua"} onPress={()=>{
            this.requestPurchase(itemSkus[0])
          }}></Button>
        </View>
    );
  }
}

const styles = StyleSheet.create({
    baseText: {
      fontWeight: 'bold',
      fontSize: 18,
    },
    wraplayout: {
      flexDirection: 'row',
      paddingRight:10,
      paddingLeft: 10,
      paddingTop: 10
    }
});

export default Detail;