import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
const menus = ['HOME', 'DETAIL', 'BUY VIP'];

const BottomTab = ({selected, onSelected}) => {
  return (
    <View style={styles.bottoms}>
      {menus.map((e, i) => {
        return (
          <TouchableOpacity key={e} onPress={() => onSelected(i)}>
            <Text>{e}</Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
};

export default BottomTab;

const styles = StyleSheet.create({
  bottoms: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 10,
    fontSize: 18,
    paddingHorizontal: 30,
    backgroundColor: '#E5CCFF',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.2,
    shadowRadius: 4,
    paddingBottom: 43,
  },
});
