import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Image,
  TouchableOpacity,
} from 'react-native';

const SearchScreen = () => {

  const [layouts, setLayout] = React.useState(null);

  return (
    <ImageBackground
      source={require('./assets/bg_welcome.png')}
      style={[styles.container]}>
      <View style={styles.wrapper}>
        <View style={styles.logoView}>
          <View onLayout={({nativeEvent}) => setLayout(nativeEvent?.layout)}>
            {layouts && (
              <Image
                source={require('./assets/card_welcome_1.png')}
                style={[
                  styles.cardImg1,
                  {width: layouts.width, height: layouts.height},
                ]}
                resizeMode="contain"
              />
            )}
            <Image source={require('./assets/card_welcome_2.png')} />
          </View>
        </View>
        <View style={styles.wrapText}>
          <Text style={styles.textDbbbTitle}>
            Payments anywhere and anytime easily
          </Text>
          <Text
            style={
              styles.textDesc
            }>{`Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book`}</Text>
          <View>
            <View
              style={styles.button}
              >
              <Text style={styles.buttonText}>Get Started</Text>
            </View>
            <View
              style={styles.button}
              >
              <Text style={styles.buttonText}>Login</Text>
            </View>
          </View>
        </View>
      </View>
    </ImageBackground>
  );
};

export default SearchScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  textDbbbTitle: {
    color: '#FFF',
    fontSize: 25,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  textDesc: {
    color: '#FFF',
    textAlign: 'center',
    fontSize: 14,
    marginVertical: 30,
  },
  wrapText: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 12,
    paddingBottom: 100,
    marginTop: 40,
  },
  cardImg1: {
    position: 'absolute',
    zIndex: 2,
    bottom: 30,
  },
  logoView: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 15,
  },
  button: {
    backgroundColor: '#FFF',
    paddingHorizontal: 30,
    paddingVertical: 12,
    borderRadius: 100,
    marginBottom: 10,
  },
  buttonText: {
    fontSize: 16,
    fontWeight: '700',
  },
});
