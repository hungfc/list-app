import React from 'react'
import {View, Text,Image } from 'react-native'

const About = ({navigation}) => {
    return(
        <View style={{
            flex:1,
            backgroundColor:"#FFF",
            padding: 20
            
        }}>
            <Text>An expense is the cost of operations that a company incurs to generate revenue. As the popular saying goes, “it costs money to make money.” Common expenses include payments to suppliers, employee wages, factory leases, and equipment depreciation. Businesses are allowed to write off tax-deductible expenses on their income tax returns to lower their taxable income and thus their tax liability. However, the Internal Revenue Service (IRS) has strict rules on which expenses businesses are allowed to claim as a deduction.</Text>
        </View>
    )
}
export default About;