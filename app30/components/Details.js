import React, {useRef, useMemo, useState, useEffect} from 'react';
import { FlatList, StyleSheet, Text, View, SafeAreaView } from 'react-native';
import ListItem from './ListItem';
import Chart from './Chart';
import {
  BottomSheetModal,
  BottomSheetModalProvider,
} from '@gorhom/bottom-sheet';
import { getMarketData } from '../services/cryptoService';

export default function Details(item) {

  return (
    <BottomSheetModalProvider>
    <SafeAreaView style={styles.container}>
    <View style={styles.titleWrapper}>
        <Text style={styles.largeTitle}>Name: Bitcoin</Text>
        <Text style={styles.largeTitle}>Price: 38900$</Text>
        <Text style={styles.largeTitle}>Symbol: BTC</Text>
        <Text style={styles.largeTitle}>Price Change Percentage: 5.67%</Text>
        </View>
    </SafeAreaView>
      </BottomSheetModalProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#99FFFF',
  },
  titleWrapper: {
    marginTop: 20,
    paddingHorizontal: 16,
  },
  largeTitle: {
    fontSize: 24,
    fontWeight: "bold",
  },
  divider: {
    height: StyleSheet.hairlineWidth,
    backgroundColor: '#A9ABB1',
    marginHorizontal: 16,
    marginTop: 16,
  },
  bottomSheet: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: -4,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
});
