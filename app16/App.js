/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import {View} from 'react-native';
import {Home} from './src/screens/Home';
import {Meditate} from './src/screens/Meditate';
import {Music} from './src/screens/Music';
import { ThanhToan } from './billing';
const Stack = createStackNavigator();
class App extends Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Home" options={{ title: 'Home' }} component={Home} />
          <Stack.Screen name="Meditate" component={Meditate} />
          <Stack.Screen name="Music" options={{ title: 'Music' }} component={Music} />
          <Stack.Screen name="ThanhToan" options={{ title: 'ThanhToan' }} component={ThanhToan} />
        </Stack.Navigator>
      </NavigationContainer>

    );
  }
};

export default App;