import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,

  Text,
  View,
  Image
} from 'react-native';

const About = ({ navigation }) => {
    return (
        <View style={{width:'99%',height:'99%', backgroundColor: '#A0C7EE', alignItems: 'center'}}>
          <View style={{flexDirection:'row'}}>
          <TouchableOpacity style={styles.vbnmvbSuiyui} title="ADD VIP" onPress={() => navigation.navigate('ThanhToan')}><Text style={styles.opiSqwe}>MUA VIP</Text></TouchableOpacity>
          <Text>   </Text>
          <TouchableOpacity style={styles.vbnmvbSuiyui} title="Lên Kế Hoạch" onPress={() => navigation.navigate('Home')}><Text style={styles.opiSqwe}>LÊN KẾ HOẠCH</Text></TouchableOpacity>
          </View>
          <View>
            <Text style={{padding:10, fontSize:23}}>
            
Personal Plan được coi như ứng dụng lập kế hoạch hoàn hảo nhất cho những người hay quên, “nước đến chân mới nhảy”. Bạn chẳng cần phải đau đầu nghĩ nhiều việc làm gì, tất cả hãy để Personal Plan lo. Ứng dụng sẽ tự động và không ngừng nhắc nhở các nhiệm vụ bạn đã đặt ra trước đó cho đến khi hoàn thành mới thôi.

Ưu điểm của ứng dụng này chính là tính đơn giản, hiệu quả. Chẳng cần phải tạo tài khoản, thiết lập ngày bắt đầu và kết thúc, chẳng cần phải sắp xếp mức độ ưu tiên, Personal Plan sẽ nhắc bạn mọi thứ.
            </Text>
          </View>
        </View>
    );
  
};
       
const styles = StyleSheet.create({
  btn: {
    height: 35,
    width: '100%',
    alignSelf: 'center',
    backgroundColor: '#00c40f',
    borderRadius: 0,
    borderWidth: 0,
  },
  txt: {
    fontSize:19,
  },
  image: {
    width: '100%',
    height: 330
  },
  opiSqwe: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  vbnmvbSuiyui: {
    padding: 15,
    width: '48%',
    backgroundColor: '#0080FF',
  }
});

export default About;