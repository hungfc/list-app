import React, {useState} from 'react';
import About from './src/screens/About';
import Home from './src/screens/Home';

import 'react-native-gesture-handler';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {ThanhToan} from './billing/index';

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="About" options={{ title: 'About' }} component={About} />
          <Stack.Screen name="Home" component={Home} />
          <Stack.Screen name="ThanhToan" options={{ title: 'ThanhToan' }} component={ThanhToan} />
        </Stack.Navigator>
      </NavigationContainer>
  );
}
