import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {ThanhToan} from './billing/index';
import Onboarding from './src/componets/Onboarding';

const Stack = createStackNavigator();
const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator headerMode={false} initialRouteName={'Spalsh'}>
        <Stack.Screen name="Spalsh" component={Onboarding} />
        <Stack.Screen name="ThanhToan" component={ThanhToan} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;