export default [
    {
        id: '1',
        title: 'Do not use fast food',
        description: 'Do not use fast food, ready food, canned food and fried food. Do not use alcohol and tobacco.',
        image: require('./asset/burger.png')
    },
    {
        id: '2',
        title: 'Use all green vegetables, tubers.',
        description: 'Using mainly food in its original form, still alive. Use all green vegetables, tubers.',
        image: require('./asset/fries.png')
    },
    {
        id: '3',
        title: 'Eat enough macronutrients',
        description: 'Eat enough macronutrients: Protein, fat, starch and fiber. Eat enough micronutrients: Vitamins and minerals.',
        image: require('./asset/noodels.png')
    },
    {
        id: '4',
        title: 'To ensure the necessary nutrients for the body',
        description: 'To ensure the necessary nutrients for the body, eating Eat Clean still has to meet 3 types of macronutrients: Starch, Protein and fat and micronutrients: Vitamins and minerals.',
        image: require('./asset/icecream.png')
    }
]