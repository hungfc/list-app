import React, {useState, useRef} from 'react';
import {FlatList, StyleSheet,TouchableOpacity, Text, View, Animated} from 'react-native';
import OnBoardingItem from './OnBoardingItem';
import data from '../data';
import Paginator from './Paginator';

const Onboarding = ({navigation}) => {
    const [currentIndex, setCurrentIndex] = useState(0);
    const scrollX = useRef(new Animated.Value(0)).current;
    const slidesRef = useRef(null);

    const viewableItemsChanged = useRef(({ viewableItems}) => {
        setCurrentIndex(viewableItems[0].index)
    }).current;

    const viewConfig = useRef({viewAreaCoveragePercentThreshold : 50}).current;
    
  return (
    <View style={styles.container}>
      <View style={{flex :3}}>
      <FlatList
        data={data}
        renderItem={({item}) => <OnBoardingItem item={item} />}
        horizontal
        showsHorizontalScrollIndicator={false}
        pagingEnabled
        bounces={false}
        keyExtractor={(item) => item.id}
        onScroll={Animated.event([{nativeEvent :{ contentOffset : {x : scrollX}}}],{
            useNativeDriver:false
        })}
        onViewableItemsChanged={viewableItemsChanged}
        viewabilityConfig={viewConfig}
        scrollEventThrottle={32}
        ref={slidesRef}
      />
      </View>
      <Paginator data={data} scrollX={scrollX}/>
      <TouchableOpacity style={{padding:15, backgroundColor: '#A0A0A0', width: '100%'}} onPress={() => navigation.navigate('ThanhToan')}>
          <Text style={{textAlign:'center'}}>GET VIP</Text>
        </TouchableOpacity>
    </View>
  );
};

export default Onboarding;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
