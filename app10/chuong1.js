import * as React from 'react';
import { View, Text, FlatList, Dimensions, StyleSheet, TouchableOpacity, Button, Image} from 'react-native';

export class Chuong1 extends React.Component {

  render() {
    return (
      <View style={styles.wraper}>
        <TouchableOpacity style={styles.btnGold} title="MUA VIP" onPress={() => this.props.navigation.navigate('Billing')}><Text style={styles.textBtn}>MUA VIP</Text></TouchableOpacity>
        <Text style={styles.base}>Đêm, đã tới.Một vầng trăng tròn dần dần hiện lên ở cuối đường chân trời. Trên ngọn núi phía sau Thiên Kiếm Tông có hai thiếu niên chừng mười ba mười bốn tuổi đang luyện kiếm. Đứa lớn hơn hai mắt có thần ánh, kiếm thế có vẻ linh động nhanh nhẹn còn đứa nhỏ hơn thì khí tức đã phần nào mệt mỏi, kiếm thế chậm chạp. Đứa lớn tên là Huyền Thiên còn đứa nhỏ là Hoàng Thạch đều là ngoại môn đệ tử Thiên Kiếm Tông. Hai đứa đang luyện cùng một loại kiếm pháp, Hoàng Giai trung phẩm kiếm kỹ Truy Phong Kiếm Pháp. Rất ít khi hai người cũng luyện xong một bộ kiếm pháp, lúc này cả hai đồng thời thu công. Hoàng Thạch nhỏ tuổi hơn thở hổn hển liên tục lấy hơi, hơi chút điều hòa lại khí tức rồi nói:</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wraper: {
    width: '100%',
    height: '100%',
    backgroundColor: '#EDBB99',
  },
  container: {
    flex: 1,
    marginVertical: 20,
  },
  image: {
    height: 230,
    width: 180,
    margin: 15
  },
  base: {
    fontWeight: 'bold',
    fontSize: 17,
    lineHeight: 23,
    padding: 15
  },
  item: {
    backgroundColor: '#EDBB99',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    margin: 1,
    marginBottom: 1,
    height: '100%',
  },
  styleInvisible: {
    backgroundColor: 'transparent',
  },
  btnAdd: {
    marginTop: 45,
    padding: 15,
    width: 100,
    backgroundColor: '#A04000',
  },
  textBtn: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  btnGold1: {
    padding: 15,
    width: '100%',
    backgroundColor: '#D89460',
  },
  btnGold: {
    padding: 15,
    marginBottom: 15,
    width: '100%',
    backgroundColor: '#A04000',
  }
});