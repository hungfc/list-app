/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';

import {
  StyleSheet
} from 'react-native';
import { Billing } from './billing';
import { Home } from './home'
import { Home1 } from './home1'
import { Home2 } from './home2'
import { Home3 } from './home3'
import { Chuong1 } from './chuong1'
import { Chuong2 } from './chuong2'
import { Chuong3 } from './chuong3'
import { Chuong4 } from './chuong4'

const Stack = createStackNavigator();
class App extends Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Home" options={{ title: 'Home' }} component={Home} />
          <Stack.Screen name="Home1" options={{ title: 'Home1' }} component={Home1} />
          <Stack.Screen name="Home2" options={{ title: 'Home2' }} component={Home2} />
          <Stack.Screen name="Home3" options={{ title: 'Home3' }} component={Home3} />
          <Stack.Screen name="Chuong1" options={{ title: 'Chuong1' }} component={Chuong1} />
          <Stack.Screen name="Chuong2" options={{ title: 'Chuong2' }} component={Chuong2} />
          <Stack.Screen name="Chuong3" options={{ title: 'Chuong3' }} component={Chuong3} />
          <Stack.Screen name="Chuong4" options={{ title: 'Chuong4' }} component={Chuong4} />
          <Stack.Screen name="Billing" options={{ title: 'Billing' }} component={Billing} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
};

const styles = StyleSheet.create({
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
