import * as React from 'react';
import { View, Text, StyleSheet, TouchableOpacity} from 'react-native';

export class Home3 extends React.Component {
  render() {
    return (
      <View style={styles.wraper}>
        <TouchableOpacity style={styles.btnGold} title="MUA VIP" onPress={() => this.props.navigation.navigate('Billing')}><Text style={styles.textBtn}>MUA VIP</Text></TouchableOpacity>
        <View
        style={styles.item}
      >
        <TouchableOpacity style={styles.btnAdd} title="Chương 1" onPress={() => this.props.navigation.navigate('Chuong1')}><Text style={styles.textBtn}>Chương 1: Kiếm ảnh hàn đàm</Text></TouchableOpacity>
        <TouchableOpacity style={styles.btnAdd} title="Chương 2" onPress={() => this.props.navigation.navigate('Chuong2')}><Text style={styles.textBtn}>Chương 2: Đánh bại Trương Hổ</Text></TouchableOpacity>
        <TouchableOpacity style={styles.btnAdd} title="Chương 3" onPress={() => this.props.navigation.navigate('Chuong3')}><Text style={styles.textBtn}>Chương 3: Vũ Kỹ Các</Text></TouchableOpacity>
        <TouchableOpacity style={styles.btnAdd} title="Chương 4" onPress={() => this.props.navigation.navigate('Chuong4')}><Text style={styles.textBtn}>Chương 4: Danh chấn ngoại môn</Text></TouchableOpacity>
        </View>
      </View>
        
    );
  }
}

const styles = StyleSheet.create({
  wraper: {
    width: '100%',
    height: '100%',
    backgroundColor: '#EDBB99',
  },
  container: {
    flex: 1,
    marginVertical: 20,
  },
  image: {
    height: 230,
    width: 180,
    margin: 15
  },
  base: {
    fontWeight: 'bold',
    fontSize: 17,
    lineHeight: 23
  },
  item: {
    backgroundColor: '#EDBB99',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    margin: 1,
    marginBottom: 1,
    height: '100%',
  },
  styleInvisible: {
    backgroundColor: 'transparent',
  },
  btnAdd: {
    marginTop: 45,
    padding: 15,
    width: 300,
    backgroundColor: '#A04000',
  },
  textBtn: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  btnGold1: {
    padding: 15,
    width: '100%',
    backgroundColor: '#A04000',
  },
  btnGold: {
    padding: 15,
    width: '100%',
    backgroundColor: '#A04000',
  }
});