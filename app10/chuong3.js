import * as React from 'react';
import { View, Text, FlatList, Dimensions, StyleSheet, TouchableOpacity, Button, Image} from 'react-native';

export class Chuong3 extends React.Component {

  render() {
    return (
      <View style={styles.wraper}>
        <TouchableOpacity style={styles.btnGold} title="MUA VIP" onPress={() => this.props.navigation.navigate('Billing')}><Text style={styles.textBtn}>MUA VIP</Text></TouchableOpacity>
        <Text style={styles.base}>Thật sự là kỳ quái, Hoàng Thiên sư huynh cùng Trương Hổ sư huynh vài ngày trước đã đánh qua lôi đài, khi đó Hoàng Thiên sư huynh mới ra được mười một chiêu thì thua trong tay Trương Hổ sư huynh ah, tại sao mới qua mấy ngày lại trở nên lợi hại như vậy? Ta nhớ được Hoàng Thiên sư huynh mới có tu vi võ giả tứ trọng thôi mà, hiện tại Hoàng Thiên sư huynh mới qua vài ngày đã khiến người ta lau mắt mà nhìn rồi.</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wraper: {
    width: '100%',
    height: '100%',
    backgroundColor: '#EDBB99',
  },
  container: {
    flex: 1,
    marginVertical: 20,
  },
  image: {
    height: 230,
    width: 180,
    margin: 15
  },
  base: {
    fontWeight: 'bold',
    fontSize: 17,
    lineHeight: 23,
    padding: 15
  },
  item: {
    backgroundColor: '#EDBB99',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    margin: 1,
    marginBottom: 1,
    height: '100%',
  },
  styleInvisible: {
    backgroundColor: 'transparent',
  },
  btnAdd: {
    marginTop: 45,
    padding: 15,
    width: 100,
    backgroundColor: '#A04000',
  },
  textBtn: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  btnGold1: {
    padding: 15,
    width: '100%',
    backgroundColor: '#D89460',
  },
  btnGold: {
    padding: 15,
    marginBottom: 15,
    width: '100%',
    backgroundColor: '#A04000',
  }
});