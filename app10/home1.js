import * as React from 'react';
import { View, Text, FlatList, Dimensions, StyleSheet, TouchableOpacity, Button, Image} from 'react-native';

export class Home1 extends React.Component {

  render() {
    return (
      <View style={styles.wraper}>
        <TouchableOpacity style={styles.btnGold} title="MUA VIP" onPress={() => this.props.navigation.navigate('Billing')}><Text style={styles.textBtn}>MUA VIP</Text></TouchableOpacity>
        <Text style={styles.textBtn}>Giới Thiệu</Text>
        <Text style={styles.base}>Sau thành công của Chân Tiên, tác giả EK tiếp tục khẳng định khả năng viết truyện huyền huyễn của mình qua tác phẩm mới Kiếm Nghịch Thương Khung. Với phong cách viết truyện cuốn hút, các trận chiến đấu kịch tính gay cấn, tình tiết logic hợp lý đan xen nhau tạo nút thắt mở ly kỳ cho câu chuyện, EK đã cuốn hút độc giả theo từng bước chân của nvc. Bên cạnh sự thành công của Kiếm Đạo Độc Tôn thì Kiếm Nghịch Thương Khung cũng là một trong những bộ Kiếm Tu xuất sắc với cách xây dựng hệ thống tu luyện hợp lý, sự trưởng thành của nvc không thiếu sự kỳ ngộ nhưng hiểm cảnh mà nvc phải vượt qua cũng không hề dễ dàng. Và bên cạnh "trai anh hùng" thì không thể thiếu được "gái thuyền quyên", EK không phải xây dựng một hình tượng nvc thuần tu luyện mà còn thể hiện tình cảm, sự ôn nhu của mình qua các đoạn nhân duyên trên bước đường trưởng thành</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wraper: {
    width: '100%',
    height: '100%',
    backgroundColor: '#EDBB99',
  },
  container: {
    flex: 1,
    marginVertical: 20,
  },
  image: {
    height: 230,
    width: 180,
    margin: 15
  },
  base: {
    fontWeight: 'bold',
    fontSize: 17,
    lineHeight: 23,
    padding: 15
  },
  item: {
    backgroundColor: '#EDBB99',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    margin: 1,
    marginBottom: 1,
    height: '100%',
  },
  styleInvisible: {
    backgroundColor: 'transparent',
  },
  btnAdd: {
    marginTop: 45,
    padding: 15,
    width: 100,
    backgroundColor: '#A04000',
  },
  textBtn: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  btnGold1: {
    padding: 15,
    width: '100%',
    backgroundColor: '#D89460',
  },
  btnGold: {
    padding: 15,
    marginBottom: 15,
    width: '100%',
    backgroundColor: '#A04000',
  }
});