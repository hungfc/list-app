import * as React from 'react';
import { View, Text, StyleSheet, TouchableOpacity} from 'react-native';

export class Home extends React.Component {
  render() {
    return (
      <View style={styles.wraper}>
        <TouchableOpacity style={styles.btnGold} title="MUA VIP" onPress={() => this.props.navigation.navigate('Billing')}><Text style={styles.textBtn}>MUA VIP</Text></TouchableOpacity>
        <View
        style={styles.item}
      >
        <TouchableOpacity style={styles.btnAdd} title="Giới Thiệu" onPress={() => this.props.navigation.navigate('Home1')}><Text style={styles.textBtn}>Giới Thiệu</Text></TouchableOpacity>
        <TouchableOpacity style={styles.btnAdd} title="Tổng Quan" onPress={() => this.props.navigation.navigate('Home2')}><Text style={styles.textBtn}>Tổng Quan</Text></TouchableOpacity>
        <TouchableOpacity style={styles.btnAdd} title="Đọc Truyện" onPress={() => this.props.navigation.navigate('Home3')}><Text style={styles.textBtn}>Đọc Truyện</Text></TouchableOpacity>
        </View>
      </View>
        
    );
  }
}

const styles = StyleSheet.create({
  wraper: {
    width: '100%',
    height: '100%',
    backgroundColor: '#EDBB99',
  },
  container: {
    flex: 1,
    marginVertical: 20,
  },
  image: {
    height: 230,
    width: 180,
    margin: 15
  },
  base: {
    fontWeight: 'bold',
    fontSize: 17,
    lineHeight: 23
  },
  item: {
    backgroundColor: '#EDBB99',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    margin: 1,
    marginBottom: 1,
    height: '100%',
  },
  styleInvisible: {
    backgroundColor: 'transparent',
  },
  btnAdd: {
    marginTop: 45,
    padding: 15,
    width: 300,
    backgroundColor: '#A04000',
  },
  textBtn: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  btnGold1: {
    padding: 15,
    width: '100%',
    backgroundColor: '#A04000',
  },
  btnGold: {
    padding: 15,
    width: '100%',
    backgroundColor: '#A04000',
  }
});