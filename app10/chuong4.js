import * as React from 'react';
import { View, Text, FlatList, Dimensions, StyleSheet, TouchableOpacity, Button, Image} from 'react-native';

export class Chuong4 extends React.Component {

  render() {
    return (
      <View style={styles.wraper}>
        <TouchableOpacity style={styles.btnGold} title="MUA VIP" onPress={() => this.props.navigation.navigate('Billing')}><Text style={styles.textBtn}>MUA VIP</Text></TouchableOpacity>
        <Text style={styles.base}>Trước mặt Huyền Thiên không có bí kíp, không có giá sách, hết thảy đều không có, trắng xoá, nhưng mà kiếm quang vào lúc này lại lóe lên, lóe lên tức thì, trong khoảnh khắc đó tâm của Huyền Thiên đập mạnh, một kiếm này giống như từ thiên ngoại hàng lâm, xuyên thẳng qua hư không, lại đi vào thiên ngoại, nhanh tới mức không tưởng tượng nổi, trong lòng có hiểu ra.</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wraper: {
    width: '100%',
    height: '100%',
    backgroundColor: '#EDBB99',
  },
  container: {
    flex: 1,
    marginVertical: 20,
  },
  image: {
    height: 230,
    width: 180,
    margin: 15
  },
  base: {
    fontWeight: 'bold',
    fontSize: 17,
    lineHeight: 23,
    padding: 15
  },
  item: {
    backgroundColor: '#EDBB99',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    margin: 1,
    marginBottom: 1,
    height: '100%',
  },
  styleInvisible: {
    backgroundColor: 'transparent',
  },
  btnAdd: {
    marginTop: 45,
    padding: 15,
    width: 100,
    backgroundColor: '#A04000',
  },
  textBtn: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  btnGold1: {
    padding: 15,
    width: '100%',
    backgroundColor: '#D89460',
  },
  btnGold: {
    padding: 15,
    marginBottom: 15,
    width: '100%',
    backgroundColor: '#A04000',
  }
});