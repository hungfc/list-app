import React from 'react';
import {SafeAreaView, Text} from 'react-native';
const HomeScreen = () => {
  return (
    <SafeAreaView
      style={{flex: 1, justifyContent: 'center', alignItems: 'center', padding: 15}}>
      <Text style={{fontSize: 22, lineHeight: 29}}>Kỹ sư phần mềm đang là một trong những nghề nhu cầu cao nhất hiện nay – ngay cả thực tập sinh tại các công ty công nghệ cũng có thể nhận một mức lương đáng ghen tị.

Giữa hàng triệu nhà phát triển với đủ mọi loại kỹ năng khác nhau, nhiều công ty công nghệ đang bắt đầu quay sang GitHub - startup 2 tỷ USD được mệnh danh là “Facebook của giới lập trình viên” - để tìm kiếm các nhân tài code.

Nếu bạn là một lập trình viên đang tìm kiếm cơ hội việc làm trong các công ty này thì hãy nghía xem danh sách các ngôn ngữ lập trình hot nhất trên GitHub dưới đây (Danh sách dựa trên thống kê thực của GitHub) để xem những ngôn ngữ nào đang được săn đón nhiều nhất nhé.</Text>
    </SafeAreaView>
  );
};

export default HomeScreen;
