import React from 'react';
import {
  SafeAreaView,
  Image,
  StyleSheet,
  FlatList,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  Dimensions,
} from 'react-native';

const {width, height} = Dimensions.get('window');

const COLORS = {primary: '#282534', white: '#fff'};

const slides = [
  {
    id: '1',
    image: require('../images/image1.png'),

    title: 'JavaScript',

    subtitle: 'Ngôn ngữ lập trình siêu phổ biến này thường được dùng vào thiết kế ứng dụng web.',
  },
  {
    id: '2',
    image: require('../images/image2.png'),

    title: 'PHP',

    subtitle: 'PHP đã quá phổ biến trong thiết kế website. Theo một ước tính không chính thức',
  },
  {
    id: '3',
    image: require('../images/image3.png'),

    title: 'Python',

    subtitle: 'Với xuất phát điểm từ năm 1989, cũng như Ruby, Python được ưa chuộng bởi tính dễ đọc.',
  },
];

const Slide = ({item}) => {
  return (
    <View style={{alignItems: 'center'}}>

      <Image
        source={item?.image}

        style={{height: '75%', width, resizeMode: 'contain'}}
      />
      <View>
        <Text style={styles.title}>{item?.title}</Text>

        <Text style={styles.subtitle}>{item?.subtitle}</Text>
      </View>
    </View>
  );
};

const OnboardingScreen = ({navigation}) => {
  const [currentSlideIndex, iuouiouiDweqweqw] = React.useState(0);

  const ref = React.useRef();

  const asdasdFyrtyrtyrty = e => {

    const contentOffsetX = e.nativeEvent.contentOffset.x;

    const currentIndex = Math.round(contentOffsetX / width);

    iuouiouiDweqweqw(currentIndex);
  };

  const asdasdVnbmbnmb = () => {
    const nextSlideIndex = currentSlideIndex + 1;
    if (nextSlideIndex != slides.length) {

      const offset = nextSlideIndex * width;

      ref?.current.scrollToOffset({offset});

      iuouiouiDweqweqw(currentSlideIndex + 1);
    }
  };

  const Footer = () => {
    return (
      <View
        style={{
          height: height * 0.25,
          justifyContent: 'space-between',
          paddingHorizontal: 20,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginTop: 20,
          }}>
          {slides.map((_, index) => (
            <View
              key={index}
              style={[
                styles.indicator,
                currentSlideIndex == index && {
                  backgroundColor: COLORS.white,
                  width: 25,
                },
              ]}
            />
          ))}
        </View>
        <View style={{marginBottom: 20}}>
          {currentSlideIndex == slides.length - 1 ? (
            <View style={{height: 50}}>
              <TouchableOpacity
                style={styles.btn}
                onPress={() => navigation.replace('HomeScreen')}>

                <Text style={{fontWeight: 'bold', fontSize: 15}}>
                 STARTED
                </Text>
              </TouchableOpacity>
            </View>
          ) : (
            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity
                activeOpacity={0.8}
                style={[
                  styles.btn,
                  {
                    borderColor: COLORS.white,
                    borderWidth: 1,

                    backgroundColor: 'transparent',
                  },
                ]}
                onPress={() => { navigation.navigate("ThanhToan") }}>
                <Text
                  style={{
                    fontWeight: 'bold',

                    fontSize: 15,

                    color: COLORS.white,

                  }}>
                  ADD VIP
                </Text>
              </TouchableOpacity>

              <View style={{width: 15}} />

              <TouchableOpacity
                activeOpacity={0.8}

                onPress={asdasdVnbmbnmb}
                style={styles.btn}>

                <Text
                  style={{
                    fontWeight: 'bold',

                    fontSize: 15,
                  }}>
                  NEXT
                </Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
      </View>
    );
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: COLORS.primary}}>
      <StatusBar backgroundColor={COLORS.primary} />
      <FlatList
        ref={ref}
        onMomentumScrollEnd={asdasdFyrtyrtyrty}
        contentContainerStyle={{height: height * 0.75}}
        showsHorizontalScrollIndicator={false}
        horizontal
        data={slides}
        pagingEnabled
        renderItem={({item}) => <Slide item={item} />}
      />
      <Footer />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  subtitle: {
    color: COLORS.white,
    fontSize: 13,
    marginTop: 10,
    maxWidth: '70%',
    textAlign: 'center',
    lineHeight: 23,
  },
  title: {
    color: COLORS.white,
    fontSize: 22,
    fontWeight: 'bold',
    marginTop: 20,
    textAlign: 'center',
  },
  image: {
    height: '100%',
    width: '100%',
    resizeMode: 'contain',
  },
  indicator: {
    height: 2.5,
    width: 10,
    backgroundColor: 'grey',
    marginHorizontal: 3,
    borderRadius: 2,
  },
  btn: {
    flex: 1,
    height: 50,
    borderRadius: 5,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
export default OnboardingScreen;
