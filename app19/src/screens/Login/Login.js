import React, { Component } from 'react';
import {View, Text, StyleSheet, Image,TouchableOpacity} from 'react-native';
import {colors} from '../../config/colors';
import {SecondaryButton} from '../../components/buttons/SecondaryButton';
import {PrimaryInput} from '../../components/forms/PrimaryInput';
import {PrimaryButton} from '../../components/buttons/PrimaryButton';
export class Login extends Component {
  render() {
  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.btn} title="ADD VIP" onPress={() => this.props.navigation.navigate('ThanhToan')}><Text style={styles.label}>ADD VIP</Text></TouchableOpacity>
      <View styles={styles.contentContainer}>
        <Text style={styles.heading}>Welcome Back!</Text>
        <Text style={styles.or}>LOG IN WITH EMAIL</Text>
        <View style={styles.inputItem}>
          <PrimaryInput placeHolder={'Email Address'} />
        </View>
        <View style={styles.inputItem}>
          <PrimaryInput placeHolder={'Password'} />
        </View>
        <View style={styles.loginBtnWrapper}>
          <TouchableOpacity
                onPress={() => this.props.navigation.navigate('SplashScreen')}
                style={styles.btn}>
                <Text style={styles.label}>Login</Text>
              </TouchableOpacity>
        </View>
        <Text style={styles.forgotPassword}>Forgot Password?</Text>
      </View>
      <View style={styles.footerWrapper}>
      </View>
    </View>
  );
  }
};

export const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
    padding: 20,
  },
  dfgdfgBsdfsd: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  hjkhkGrtrty: {
    padding: 15,
    width: '100%',
    backgroundColor: '#4C9900',
  },
  vector1: {
    position: 'absolute',
    left: -10,
    top: -5,
  },
  vector2: {
    position: 'absolute',
    right: -6,
  },
  vector3: {
    position: 'absolute',
    top: 90,
  },
  vector4: {
    position: 'absolute',
    right: 0,
    top: 90,
  },
  contentContainer: {
    flex: 1,
  },
  back: {
    marginTop: 50,
  },
  heading: {
    fontFamily: 'HelveticaNeue',
    fontSize: 30,
    fontWeight: '700',
    lineHeight: 40,
    textAlign: 'center',
    color: colors.heading,
  },
  btnWrapper: {
    marginTop: 30,
  },
  btnItemWrapper: {
    marginBottom: 20,
  },
  or: {
    textAlign: 'center',
    marginTop: 30,
    marginBottom: 30,
    fontFamily: 'HelveticaNeue',
    fontSize: 14,
    fontWeight: '700',
    lineHeight: 30,
    color: colors.gray,
  },
  inputItem: {
    marginBottom: 20,
  },
  loginBtnWrapper: {
    marginTop: 10,
  },
  forgotPassword: {
    fontFamily: 'HelveticaNeue',
    fontWeight: '400',
    fontSize: 14,
    textAlign: 'center',
    marginTop: 20,
  },
  footerText: {
    fontFamily: 'HelveticaNeue',
    fontWeight: '400',
    fontSize: 14,
  },
  footerText1: {
    color: colors.gray,
  },
  footerText2: {
    color: colors.primary,
  },
  footerWrapper: {
    position: 'absolute',
    bottom: 60,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  btn: {
    borderRadius: 30,
    backgroundColor: '#6666FF',
    marginBottom: 20
  },
  label: {
    textAlign: 'center',
    fontSize: 18,
    fontWeight: '400',
    fontFamily: 'HelveticaNeue',
    padding: 20,
    color: '#fff'
  }
});
