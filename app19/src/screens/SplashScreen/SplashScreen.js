import React, { Component } from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import {PrimaryButton} from '../../components/buttons/PrimaryButton';
import {styles} from './styles';
  export class SplashScreen extends Component {
    render() {
  return (
    <View style={styles.container}>
      <View style={styles.contentContainer}>
      <TouchableOpacity style={styles.btn} title="ADD VIP" onPress={() => this.props.navigation.navigate('ThanhToan')}><Text style={styles.label}>ADD VIP</Text></TouchableOpacity>
        <View style={styles.top}>
          <Image
            style={styles.welcomeImage}
            source={require('../../../assets/images/enjoy.png')}
          />
        </View>
        <View style={styles.bottom}>
          <Text style={styles.heading}>We are what we do</Text>
          <Text style={styles.subHeading}>
            Thousand of people are usign silent moon for smalls meditation{' '}
          </Text>
          <View style={styles.btnWrapper}>
          <TouchableOpacity
                onPress={() => this.props.navigation.navigate('CourseDetails')}
                style={styles.btn}>
                <Text style={styles.label}>CourseDetails</Text>
              </TouchableOpacity>
          </View>
          <View style={styles.btnWrapper}>
          <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Login')}
                style={styles.btn}>
                <Text style={styles.label}>Login</Text>
              </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
    }
};
