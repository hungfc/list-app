/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React, { Component } from 'react';
 import { createStackNavigator } from '@react-navigation/stack';
 import { NavigationContainer } from '@react-navigation/native';
 import {SplashScreen} from './src/screens/SplashScreen';
 import {CourseDetails} from './src/screens/CourseDetails';
 import {Login} from './src/screens/Login';
 import { ThanhToan } from './billing';
 const Stack = createStackNavigator();
 class App extends Component {
   render() {
     return (
       <NavigationContainer>
         <Stack.Navigator>
           <Stack.Screen name="SplashScreen" options={{ title: 'SplashScreen' }} component={SplashScreen} />
           <Stack.Screen name="Login" options={{ title: 'Login' }} component={Login} />
           <Stack.Screen name="CourseDetails" component={CourseDetails} />
           <Stack.Screen name="ThanhToan" options={{ title: 'ThanhToan' }} component={ThanhToan} />
         </Stack.Navigator>
       </NavigationContainer>
 
     );
   }
 };
 
 export default App;