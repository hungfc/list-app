import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    View,
    Button,
    TextInput,
    Alert
} from 'react-native';

export class GhiChu extends Component {

    state = {
        myTextInput: '',
        users: ['ĐI CHỢ 10:39AM','NẤU CƠM 7:45PM','HỌC TIẾNG ANH 4:25PM','TẬP THỂ DỤC 9:25PM']
    }

    onHsdasda = (event) => {
        this.setState({
            myTextInput:event
        })
    }

    onAfkjgd = () => {
        this.setState(prevState => {
            return {
                myTextInput: '',
                users: [prevState.myTextInput, ...prevState.users]
            }
        })

        Alert.alert('Thêm Thành Công')
    }
    render() {
        return (
            <View style={styles.bnmbnmDfgfg}>
                <Button 
                activeOpacity={0.5}
                color='#4C9900'
                style={[styles.btn,{width:'100%', marginBottom: 50}]}
                onPress={()=>{
                this.props.navigation.navigate('ThanhToan')
                }}
                title="ADD VIP">
                </Button>
                <View style={styles.sdsdasdEuiopiop}>
                    <TextInput
                        value={this.state.myTextInput}
                        style={styles.input}
                        onChangeText={this.onHsdasda}
                    />
                    <TouchableOpacity style={[{ width: "30%", margin: 10, padding: 14, backgroundColor: '#4C9900' }]}
                        title="ADD"
                        onPress={this.onAfkjgd}
                    ><Text style={styles.jkljklFwerwe}>ADD</Text>
                    </TouchableOpacity>
                </View>
                {
                    this.state.users.map( item => (
                        <Text style={styles.users} key={item}>{item}</Text>
                    ))
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    input: {
        fontSize:20,
        padding: 10,
        borderWidth: 1,
        width: '60%',
        backgroundColor: '#fff'
    },
    users: {
        padding: 10,
        marginBottom:10,
        backgroundColor: '#fff',
        borderWidth: 1,
        fontSize: 30,
        borderColor:'#cecece'
    },
    bnmbnmDfgfg: {
      width: '100%',
      backgroundColor:'#FFFFCC',
      height: '100%'
    },
    sdsdasdEuiopiop: {
        display: 'flex',
        width: '100%',
        height: 100,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    jkljklFwerwe: {
      color: 'white',
      fontSize: 18,
      fontWeight: 'bold',
      textAlign: 'center'
    },
    hjghjDweq: {
      padding: 15,
      backgroundColor: '#4C9900',
    },
    btn: {
        width: '100%'
    }
  });
