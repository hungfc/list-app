const shoes = [
    {
        _id: "1",
        productImage: require("./shoesimage/nano1.jpg"),
        productModel: "Men Training",

        productName: "Nanoo x1 lux",
        
        productPrice: 160,
        productOldPrice: 140,
        productFourImage: require("./shoesimage/nano4.jpg"),

        productSecondImage: require("./shoesimage/nano2.jpg"),

        productThirdImage: require("./shoesimage/nano3.jpg"),
    },
    {
        _id: "2",
        productImage: require("./shoesimage/leather1.jpg"),
        productModel: "Classsic",

        productName: "Classsic shoes",

        productPrice: 120,

        productOldPrice: 60,

        productFourImage: require("./shoesimage/leather4.jpg"),

        productSecondImage: require("./shoesimage/leather2.jpg"),

        productThirdImage: require("./shoesimage/leather3.jpg"),
    },
    {
        _id: "3",
        productImage: require("./shoesimage/nanox1.jpg"),
        productModel: "Men training",
        productName: "Reebook shoes",
        productPrice: 160,


        productOldPrice: 120,

        productFourImage: require("./shoesimage/nanox4.jpg"),

        productSecondImage: require("./shoesimage/nanox2.jpg"),

        productThirdImage: require("./shoesimage/nanox3.jpg"),
    },
    {
        _id: "4",
        productImage: require("./shoesimage/workoutplus1.jpg"),
        productModel: "Classsic",
        productName: "Workout shoes",
        productPrice: 90,
        productOldPrice: 70,
        productFourImage: require("./shoesimage/workoutplus4.jpg"),

        productSecondImage: require("./shoesimage/workoutplus2.jpg"),

        productThirdImage: require("./shoesimage/workoutplus3.jpg"),
    },
    {
        _id: "5",
        productImage: require("./shoesimage/braindead1.jpg"),
        productModel: "Men training",

        productName: "Zig ll shoes",

        productPrice: 120,
        productOldPrice: 110,

        productFourImage: require("./shoesimage/braindead4.jpg"),

        productSecondImage: require("./shoesimage/braindead2.jpg"),

        productThirdImage: require("./shoesimage/braindead3.jpg"),
    },
    {
        _id: "6",
        productImage: require("./shoesimage/clubshoes1.jpg"),
        productModel: "Men Classsic",
        productName: "Revenge shoes",

        productPrice: 60,

        productOldPrice: 45,

        productFourImage: require("./shoesimage/clubshoes4.jpg"),

        productSecondImage: require("./shoesimage/clubshoes2.jpg"),

        productThirdImage: require("./shoesimage/clubshoes3.jpg"),
    },
    {
        _id: "7",
        productImage: require("./shoesimage/billionaire1.jpg"),
        productModel: "Classsic",
        productName: "Billionaire shoes",

        productPrice: 210,
        productOldPrice: 180,

        productFourImage: require("./shoesimage/billionaire4.jpg"),

        productSecondImage: require("./shoesimage/billionaire2.jpg"),

        productThirdImage: require("./shoesimage/billionaire3.jpg"),
    },
    {
        _id: "8",
        productImage: require("./shoesimage/coastshoes1.jpg"),
        productModel: "Classic",
        productName: "Club shoes",

        productPrice: 80,
        productOldPrice: 30,

        productFourImage: require("./shoesimage/coastshoes1.jpg"),

        productSecondImage: require("./shoesimage/coastshoes1.jpg"),

        productThirdImage: require("./shoesimage/coastshoes1.jpg"),
    },
]
export default shoes;


