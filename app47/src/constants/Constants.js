import Colors from "./Colors"
import images from "./images"

export const dummyText = "Every time when going out, men just need to dress neatly, carry a pocket wallet with them, but sisters and women are different, with the essential needs of a girl when going out always need to carry. There are countless personal items such as eyeglasses, cosmetics, wallets, phones, etc., so an indispensable accessory for women is a branded handbag 2022, spacious for women to have. I can organize everything when I go out without much luggage."

export const bagsList = [
  { id: '101', title: 'Handbag 1', subtitle: 'New Design Hand Bag', description: dummyText, image: images.bag1, price: 325, bgColor: Colors.bag1Bg },
  { id: '102', title: 'Handbag 2', subtitle: 'New Design Hand Bag', description: dummyText, image: images.bag2, price: 456, bgColor: Colors.bag2Bg },
  { id: '103', title: 'Handbag 3', subtitle: 'New Design Hand Bag', description: dummyText, image: images.bag3, price: 123, bgColor: Colors.bag3Bg },
  { id: '104', title: 'Handbag 4', subtitle: 'New Design Hand Bag', description: dummyText, image: images.bag4, price: 785, bgColor: Colors.bag4Bg },
  { id: '105', title: 'Handbag 5', subtitle: 'New Design Hand Bag', description: dummyText, image: images.bag5, price: 125, bgColor: Colors.bag5Bg },
  { id: '106', title: "Handbag 6", subtitle: 'New Design Hand Bag', description: dummyText, image: images.bag6, price: 355, bgColor: Colors.bag6Bg },
  { id: '107', title: 'Handbag 7', subtitle: 'New Design Hand Bag', description: dummyText, image: images.bag7, price: 699, bgColor: Colors.bag7Bg },
  { id: '108', title: 'Handbag 8', subtitle: 'New Design Hand Bag', description: dummyText, image: images.bag8, price: 477, bgColor: Colors.bag8Bg },
  { id: '109', title: 'Handbag 9', subtitle: 'New Design Hand Bag', description: dummyText, image: images.bag9, price: 122, bgColor: Colors.bag9Bg },
  { id: '110', title: 'Handbag 10', subtitle: 'New Design Hand Bag', description: dummyText, image: images.bag10, price: 633, bgColor: Colors.bag10Bg },
  { id: '111', title: 'Handbag 11', subtitle: 'New Design Hand Bag', description: dummyText, image: images.bag11, price: 99, bgColor: Colors.bag11Bg },
]