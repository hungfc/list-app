import React from 'react'
import { View, StyleSheet, TouchableOpacity } from 'react-native'
import { Surface, Text, Title } from 'react-native-paper'
import { useNavigation } from "@react-navigation/native";

const IconSize = 24;

const AppHeader = ({
	style,
	title,
	rightComponent,
	headerBg = "white",
	iconColor = 'black',
	titleAlight
}) => {

	const LeftView = () => (
		<View style={styles.view}>
		</View>
	)
	const navigation = useNavigation();
	const RightView = () => (
		
		rightComponent ? rightComponent :
			<View style={[styles.view, styles.rightView]}>
				<TouchableOpacity style={styles.rowView} onPress={() => {navigation.navigate("ThanhToan")}}>
					<Text style={{color: '#CC0000',fontWeight: '700'}}>GET VIP</Text>
				</TouchableOpacity>
			</View>
	)
	const TitleView = () => (
		<View style={styles.titleView}>
			<Title style={{ color: iconColor, textAlign: titleAlight }}>{title}</Title>
		</View>
	)
	return (
		<Surface style={[styles.header, style, { backgroundColor: headerBg }]}>
			<LeftView />
			<TitleView />
			<RightView />
		</Surface>
	)
}

export default AppHeader

const styles = StyleSheet.create({
	header: {
		height: 50,
		elevation: 4,
		justifyContent: 'space-between',
		alignItems: 'center',
		flexDirection: 'row',
	},
	view: {
		marginHorizontal: 16,
		alignItems: 'center',
		flexDirection: 'row',
	},
	titleView: {
		flex: 1,
	},
	rightView: {
		justifyContent: 'flex-end',
	},
	rowView: {
		flexDirection: 'row',
		alignItems: 'center',
		marginRight: 10,
	}
})