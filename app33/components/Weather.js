import React, {Component} from 'react'
import {View, StyleSheet, SafeAreaView, 
    Text,
    Image, 
    ImageBackground,    
    FlatList} from 'react-native'
let fakedArray = [
    {
        areaName: 'Ha Noi Coast',
        dateTime: 'Sun, 11am',
        temperature: 23,
        icon: 'rain',
    },
    {
        areaName: 'Ho Chi Minh',
        dateTime: 'Sun, 11am',
        temperature: 22,
        icon: 'rain',
    },{
        areaName: 'Da Nang',
        dateTime: 'Sun, 11am',
        temperature: 18,
        icon: 'rain',
        background: 'stormBackground'
    },{
        areaName: 'Nha Trang',
        dateTime: 'Sun, 11am',
        temperature: 30,
        icon: 'rain',
        background: 'sunnyBackground'
    },

    
]
export default class Weather extends Component {
    render() {
        return <SafeAreaView style={styles.container}>
            <FlatList data={fakedArray}
                    renderItem = {WeatherItem}
                    ItemSeparatorComponent = {() => {
                        return <View style = {{height: 10, 
                            width: '100%', backgroundColor:'#000'}}>

                        </View>
                    }}
                    keyExtractor={(item, index) => `${index}`}
            />
        </SafeAreaView>
    }
}
const WeatherItem = ({item, index}) => {
    const {areaName, dateTime, temperature, icon, background} = item    
    return <ImageBackground 
    source={require('../assets/images/imagenew.jpg')}
        style={[styles.weatherItem, 
                    {backgroundColor: index % 2 === 0 ? 'powderblue': 'skyblue'}]}>
            <View style={[styles.subItem, {width: '35%',}]}>
                <Text style={styles.text}>{areaName.toUpperCase()}</Text>
                <Text style={styles.text}>{dateTime.toUpperCase()}</Text>
            </View>            
            <View style={[styles.subItem, {width: '30%'}]}>
                <Text style={[styles.text, {fontSize: 60}]}>{temperature}°</Text>
            </View>
    </ImageBackground>
}

const styles = StyleSheet.create({
    container: {
        flex: 1,           
    }, 
    weatherItem: {
        height: 100,          
        flexDirection: 'row',        
    }, 
    subItem: {
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column'
    },
    text: {
        color: 'white',
        fontSize: 20,
        width: '80%',
        textAlign: 'center'
    }
})