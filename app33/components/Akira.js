       import React from 'react';
       import {
         StyleSheet,
         TouchableOpacity,
         Text,
         View,
         Image
       } from 'react-native';
       const Akira = ({ navigation }) => {
           return (
               <View style={{width:'100%',height:'100%',justifyContent:'space-between', backgroundColor: '#fff', alignItems: 'center'}}>
                 <TouchableOpacity style={styles.btnAddd} title="ADD VIP" onPress={() => navigation.navigate('ThanhToan')}><Text style={styles.textBtnnn}>MUA VIP</Text></TouchableOpacity>
                 <Image style={styles.image} source={require('../assets/images/imagenew.jpg')}/>
                 <TouchableOpacity style={styles.btnAddd} title="Check Weather" onPress={() => navigation.navigate('HorizontalList')}><Text style={styles.textBtnnn}>CHECK WEATHER</Text></TouchableOpacity>
               </View>
           );
         
       };
              
       const styles = StyleSheet.create({
         btn: {
           height: 35,
           width: '100%',
           alignSelf: 'center',
           backgroundColor: '#00c40f',
           borderRadius: 0,
           borderWidth: 0,
         },
         txt: {
           fontSize:19,
         },
         image: {
           width: '100%',
           height: 330
         },
         textBtnnn: {
           color: 'white',
           fontSize: 18,
           fontWeight: 'bold',
           textAlign: 'center'
         },
         btnAddd: {
           padding: 15,
           width: '100%',
           backgroundColor: '#0080FF',
         }
       });

       export default Akira;