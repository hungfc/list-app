import React, {useState} from 'react';
import Akira from './components/Akira';
import HorizontalList from './components/HorizontalList';
import Weather from './components/Weather';

import 'react-native-gesture-handler';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {ThanhToan} from './billing/index';

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Akira" options={{ title: 'Akira' }} component={Akira} />
          <Stack.Screen name="HorizontalList" component={HorizontalList} />
          <Stack.Screen name="ThanhToan" options={{ title: 'ThanhToan' }} component={ThanhToan} />
        </Stack.Navigator>
      </NavigationContainer>
  );
}
