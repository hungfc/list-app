import * as React from 'react';
import { View, Text, FlatList, Dimensions, StyleSheet, TouchableOpacity, Button, Image} from 'react-native';

export class Tap3 extends React.Component {

  render() {
    return (
      <View style={styles.wraper}>
        <TouchableOpacity style={styles.btnVang} title="BUY VIP" onPress={() => this.props.navigation.navigate('Billing')}><Text style={styles.textCustomBtn}>BUY VIP</Text></TouchableOpacity>
        <Text style={styles.base}>Kiệt Sâm không nói gì, ánh mắt của hắn lại nhìn về đống dụng cụ trên bàn. Ánh mắt vốn bình thản, sau một khắc lại trở nên vô cùng nghiêm túc, phảng phất như giờ phút này những thứ trước mặt hắn không phải là những dụng cụ phối chế bình thường mà toàn là thiên tài địa bảo hiếm có trong thiên địa vậy.Hơi tiến về phía trước một bước, Kiệt Sâm cầm lấy không thuộc tính nguyên tố dịch trên bàn chậm rãi đổ vào trong một bình phối chế dịch trên bàn. Động tác của Kiệt Sâm rất chậm rãi, không chút nóng nảy nào. Không thuộc tính nguyên tố dịch ở trong không khí kéo thành một sợ tơ dài mỏng trong suốt chiết xạ ánh sáng óng ánh từ những tia nắng bên ngoài cửa sổ rọi vào trông vô cùng ưu nhã.</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wraper: {
    width: '100%',
    height: '100%',
    backgroundColor: '#FFCCFF',
  },
  container: {
    flex: 1,
    marginVertical: 20,
  },
  image: {
    height: 230,
    width: 180,
    margin: 15
  },
  base: {
    fontWeight: 'bold',
    fontSize: 17,
    lineHeight: 23,
    padding: 15
  },
  item: {
    backgroundColor: '#FFCCFF',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    margin: 1,
    marginBottom: 1,
    height: '100%',
  },
  styleHidden: {
    backgroundColor: 'transparent',
  },
  btnThem: {
    marginTop: 45,
    padding: 15,
    width: 100,
    backgroundColor: '#99004C',
  },
  textCustomBtn: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  btnVang1: {
    padding: 15,
    width: '100%',
    backgroundColor: '#D89460',
  },
  btnVang: {
    padding: 15,
    marginBottom: 15,
    width: '100%',
    backgroundColor: '#99004C',
  }
});