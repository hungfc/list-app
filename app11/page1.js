import * as React from 'react';
import { View, Text, FlatList, Dimensions, StyleSheet, TouchableOpacity, Button, Image} from 'react-native';

export class Page1 extends React.Component {

  render() {
    return (
      <View style={styles.wraper}>
        <TouchableOpacity style={styles.btnVang} title="BUY VIP" onPress={() => this.props.navigation.navigate('Billing')}><Text style={styles.textCustomBtn}>BUY VIP</Text></TouchableOpacity>
        <Text style={styles.textCustomBtn}>Introduce</Text>
        <Text style={styles.base}>Bạn đang đọc truyện Dược Thần là một truyện khá hấp dẫn của tác giả Ám Ma Sư, truyện được tác giả xây dựng một cách sống động, đầy những điều kì ảo, thực ảo đan xen nhau làm ta mê mải đi tìm đâu là điều cần phải hướng đến.Hắn được người đời công nhận là cường giả mạnh nhất đại lục. Là Linh Dược Thánh Sư duy nhất của đại lục Tư Đặc Ân. Kiệt Sâm! Một thiên tài xuất sắc của đại lục Tư Đặc Ân đã gặp phải một việc ngoài ý muốn trong một lần ra luyện dược. Hắn tiến vào thời không loạn lưu, trở về thời đại Linh Dược man hoang ba ngàn năm trước...</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wraper: {
    width: '100%',
    height: '100%',
    backgroundColor: '#FFCCFF',
  },
  item: {
    backgroundColor: '#FFCCFF',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    margin: 1,
    marginBottom: 1,
    height: '100%',
  },
  styleHidden: {
    backgroundColor: 'transparent',
  },
  container: {
    flex: 1,
    marginVertical: 20,
  },
  image: {
    height: 230,
    width: 180,
    margin: 15
  },
  base: {
    fontWeight: 'bold',
    fontSize: 17,
    lineHeight: 23,
    padding: 15
  },
  btnThem: {
    marginTop: 45,
    padding: 15,
    width: 100,
    backgroundColor: '#99004C',
  },
  textCustomBtn: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  btnVang1: {
    padding: 15,
    width: '100%',
    backgroundColor: '#D89460',
  },
  btnVang: {
    padding: 15,
    marginBottom: 15,
    width: '100%',
    backgroundColor: '#99004C',
  }
});