import * as React from 'react';
import { View, Text, FlatList, Dimensions, StyleSheet, TouchableOpacity, Button, Image} from 'react-native';

export class Page2 extends React.Component {
  render() {
    return (
      <View style={styles.wraper}>
        <TouchableOpacity style={styles.btnVang} title="BUY VIP" onPress={() => this.props.navigation.navigate('Billing')}><Text style={styles.textCustomBtn}>BUY VIP</Text></TouchableOpacity>
        <Text style={styles.textCustomBtn}>Overview</Text>
        <Text style={styles.base}>Tác Giả: Ám Ma Sư</Text>
        <Text style={styles.base}>Thể Loại: Tiên Hiệp, Dị Giới</Text>
        <Text style={styles.base}>Tình Trạng: Hoàn Thành</Text>
        <Text style={styles.base}>Lượt đọc: 16753</Text>
        <Text style={styles.base}>Đánh Giá: 8.8/10 từ 642 lượt</Text>
      </View>
        
    );
  }
}

const styles = StyleSheet.create({
  wraper: {
    width: '100%',
    height: '100%',
    backgroundColor: '#FFCCFF',
  },
  container: {
    flex: 1,
    marginVertical: 20,
  },
  image: {
    height: 230,
    width: 180,
    margin: 15
  },
  base: {
    fontWeight: 'bold',
    fontSize: 17,
    lineHeight: 23,
    padding: 15
  },
  item: {
    backgroundColor: '#FFCCFF',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    margin: 1,
    marginBottom: 1,
    height: '100%',
  },
  styleHidden: {
    backgroundColor: 'transparent',
  },
  btnThem: {
    marginTop: 45,
    padding: 15,
    width: 100,
    backgroundColor: '#99004C',
  },
  textCustomBtn: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  btnVang1: {
    padding: 15,
    width: '100%',
    backgroundColor: '#D89460',
  },
  btnVang: {
    padding: 15,
    marginBottom: 15,
    width: '100%',
    backgroundColor: '#99004C',
  }
});