/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';

import {
  StyleSheet
} from 'react-native';
import { Billing } from './billing';
import { Home } from './home'
import { Page1 } from './page1'
import { Page2 } from './page2'
import { Page3 } from './page3'
import { Tap1 } from './tap1'
import { Tap2 } from './tap2'
import { Tap3 } from './tap3'
import { Tap4 } from './tap4'

const Stack = createStackNavigator();
class App extends Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Home" options={{ title: 'Home' }} component={Home} />
          <Stack.Screen name="Page1" options={{ title: 'Page1' }} component={Page1} />
          <Stack.Screen name="Page2" options={{ title: 'Page2' }} component={Page2} />
          <Stack.Screen name="Page3" options={{ title: 'Page3' }} component={Page3} />
          <Stack.Screen name="Tap1" options={{ title: 'Tap1' }} component={Tap1} />
          <Stack.Screen name="Tap2" options={{ title: 'Tap2' }} component={Tap2} />
          <Stack.Screen name="Tap3" options={{ title: 'Tap3' }} component={Tap3} />
          <Stack.Screen name="Tap4" options={{ title: 'Tap4' }} component={Tap4} />
          <Stack.Screen name="Billing" options={{ title: 'Billing' }} component={Billing} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
};

const styles = StyleSheet.create({
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
