import * as React from 'react';
import { View, Text, FlatList, Dimensions, StyleSheet, TouchableOpacity, Button, Image} from 'react-native';

export class Tap4 extends React.Component {

  render() {
    return (
      <View style={styles.wraper}>
        <TouchableOpacity style={styles.btnVang} title="BUY VIP" onPress={() => this.props.navigation.navigate('Billing')}><Text style={styles.textCustomBtn}>BUY VIP</Text></TouchableOpacity>
        <Text style={styles.base}>Tên Kiệt Sâm trước kia ở phương diện tu luyện linh lực lại là một người rất có thiên phú, mười hai tuổi lần đầu tiếp xúc với linh lực, hắn chỉ dùng có một năm thời gian liền đột phá tới Tam giai linh lực rồi dùng thực lực đó tiến vào học viện Tây Tư này. Mà ở trong học viện, chỉ trong vòng nửa năm hắn liền đột phát hai cấp, một đường tiến thẳng tới cấp độ Ngũ giai linh lực, chuyện này lúc đó cũng khiến cho cả học viện chấn động không nhỏ.</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wraper: {
    width: '100%',
    height: '100%',
    backgroundColor: '#FFCCFF',
  },
  container: {
    flex: 1,
    marginVertical: 20,
  },
  image: {
    height: 230,
    width: 180,
    margin: 15
  },
  base: {
    fontWeight: 'bold',
    fontSize: 17,
    lineHeight: 23,
    padding: 15
  },
  item: {
    backgroundColor: '#FFCCFF',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    margin: 1,
    marginBottom: 1,
    height: '100%',
  },
  styleHidden: {
    backgroundColor: 'transparent',
  },
  btnThem: {
    marginTop: 45,
    padding: 15,
    width: 100,
    backgroundColor: '#99004C',
  },
  textCustomBtn: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  btnVang1: {
    padding: 15,
    width: '100%',
    backgroundColor: '#D89460',
  },
  btnVang: {
    padding: 15,
    marginBottom: 15,
    width: '100%',
    backgroundColor: '#99004C',
  }
});