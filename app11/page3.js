import * as React from 'react';
import { View, Text, StyleSheet, TouchableOpacity} from 'react-native';

export class Page3 extends React.Component {
  render() {
    return (
      <View style={styles.wraper}>
        <TouchableOpacity style={styles.btnVang} title="BUY VIP" onPress={() => this.props.navigation.navigate('Billing')}><Text style={styles.textCustomBtn}>BUY VIP</Text></TouchableOpacity>
        <View
        style={styles.item}
      >
        <TouchableOpacity style={styles.btnThem} title="Chương 1" onPress={() => this.props.navigation.navigate('Tap1')}><Text style={styles.textCustomBtn}>Chương 1: Kiệt Sâm Đại Sư</Text></TouchableOpacity>
        <TouchableOpacity style={styles.btnThem} title="Chương 2" onPress={() => this.props.navigation.navigate('Tap2')}><Text style={styles.textCustomBtn}>Chương 2: Thủ pháp thần kỳ</Text></TouchableOpacity>
        <TouchableOpacity style={styles.btnThem} title="Chương 3" onPress={() => this.props.navigation.navigate('Tap3')}><Text style={styles.textCustomBtn}>Chương 3: Trùng kích lục cấp</Text></TouchableOpacity>
        <TouchableOpacity style={styles.btnThem} title="Chương 4" onPress={() => this.props.navigation.navigate('Tap4')}><Text style={styles.textCustomBtn}>Chương 4: Khắc Lai Nhân tức giận</Text></TouchableOpacity>
        </View>
      </View>
        
    );
  }
}

const styles = StyleSheet.create({
  wraper: {
    width: '100%',
    height: '100%',
    backgroundColor: '#FFCCFF',
  },
  container: {
    flex: 1,
    marginVertical: 20,
  },
  image: {
    height: 230,
    width: 180,
    margin: 15
  },
  base: {
    fontWeight: 'bold',
    fontSize: 17,
    lineHeight: 23
  },
  item: {
    backgroundColor: '#FFCCFF',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    margin: 1,
    marginBottom: 1,
    height: '100%',
  },
  styleHidden: {
    backgroundColor: 'transparent',
  },
  btnThem: {
    marginTop: 45,
    padding: 15,
    width: 300,
    backgroundColor: '#99004C',
  },
  textCustomBtn: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  btnVang1: {
    padding: 15,
    width: '100%',
    backgroundColor: '#99004C',
  },
  btnVang: {
    padding: 15,
    width: '100%',
    backgroundColor: '#99004C',
  }
});