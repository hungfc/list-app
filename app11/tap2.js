import * as React from 'react';
import { View, Text, FlatList, Dimensions, StyleSheet, TouchableOpacity, Button, Image} from 'react-native';

export class Tap2 extends React.Component {

  render() {
    return (
      <View style={styles.wraper}>
        <TouchableOpacity style={styles.btnVang} title="BUY VIP" onPress={() => this.props.navigation.navigate('Billing')}><Text style={styles.textCustomBtn}>BUY VIP</Text></TouchableOpacity>
        <Text style={styles.base}>Ở thời kỳ thượng cổ, các loại dược tề trên đại lục hay là các loại dược liệu, khoáng thạch ... cơ hồ tràn ngập khắp nơi cho nên lúc đó chức nghiệp Linh Dược Sư cũng không mấy được coi trọng bởi vì con người lúc đó đối với những vật phẩm này còn chưa thấy được giá trị. Đến khi chức nghiệp Linh Dược Sư này chân chính đạt tới tầm cao thì những vật phẩm có giá trị đó đã bị diệt sạch hầu như không còn. Đối với Linh Dược Sư lúc này mà nói thì đây không khác gì một sự bi ai vô cùng!Hai mắt Kiệt Sâm chăm chú nhìn chất lỏng màu đỏ thẫm trước mặt. Dược tề tăng cường tinh thần lực trong người hắn không phát huy tác dụng cảm ứng sự biến hoá của đám chất lỏng tinh hoa hoả nguyên tố kia.</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wraper: {
    width: '100%',
    height: '100%',
    backgroundColor: '#FFCCFF',
  },
  container: {
    flex: 1,
    marginVertical: 20,
  },
  image: {
    height: 230,
    width: 180,
    margin: 15
  },
  base: {
    fontWeight: 'bold',
    fontSize: 17,
    lineHeight: 23,
    padding: 15
  },
  item: {
    backgroundColor: '#FFCCFF',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    margin: 1,
    marginBottom: 1,
    height: '100%',
  },
  styleHidden: {
    backgroundColor: 'transparent',
  },
  btnThem: {
    marginTop: 45,
    padding: 15,
    width: 100,
    backgroundColor: '#99004C',
  },
  textCustomBtn: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  btnVang1: {
    padding: 15,
    width: '100%',
    backgroundColor: '#D89460',
  },
  btnVang: {
    padding: 15,
    marginBottom: 15,
    width: '100%',
    backgroundColor: '#99004C',
  }
});