import * as React from 'react';
import { View, Text, StyleSheet, TouchableOpacity} from 'react-native';

export class Home extends React.Component {
  render() {
    return (
      <View style={styles.wraper}>
        <TouchableOpacity style={styles.btnVang} title="BUY VIP" onPress={() => this.props.navigation.navigate('Billing')}><Text style={styles.textCustomBtn}>BUY VIP</Text></TouchableOpacity>
        <View
        style={styles.item}
      >
        <TouchableOpacity style={styles.btnThem} title="Introduce" onPress={() => this.props.navigation.navigate('Page1')}><Text style={styles.textCustomBtn}>Introduce</Text></TouchableOpacity>
        <TouchableOpacity style={styles.btnThem} title="Overview" onPress={() => this.props.navigation.navigate('Page2')}><Text style={styles.textCustomBtn}>Overview</Text></TouchableOpacity>
        <TouchableOpacity style={styles.btnThem} title="Read Stories" onPress={() => this.props.navigation.navigate('Page3')}><Text style={styles.textCustomBtn}>Read Stories</Text></TouchableOpacity>
        </View>
      </View>
        
    );
  }
}

const styles = StyleSheet.create({
  wraper: {
    width: '100%',
    height: '100%',
    backgroundColor: '#FFCCFF',
  },
  container: {
    flex: 1,
    marginVertical: 20,
  },
  btnVang1: {
    padding: 15,
    width: '100%',
    backgroundColor: '#99004C',
  },
  btnVang: {
    padding: 15,
    width: '100%',
    backgroundColor: '#99004C',
  },
  image: {
    height: 230,
    width: 180,
    margin: 15
  },
  base: {
    fontWeight: 'bold',
    fontSize: 17,
    lineHeight: 23
  },
  item: {
    backgroundColor: '#FFCCFF',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    margin: 1,
    marginBottom: 1,
    height: '100%',
  },
  styleHidden: {
    backgroundColor: 'transparent',
  },
  btnThem: {
    marginTop: 45,
    padding: 15,
    width: 300,
    backgroundColor: '#99004C',
  },
  textCustomBtn: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center'
  }
});