import * as React from 'react';
import  {View,StyleSheet,Image,Text} from 'react-native';
import camera from '../assets/images/camera.png';
import { useNavigation } from '@react-navigation/native';
export const DetailScreen = () => {
  const nav = useNavigation();
  return (
          <View style={styles.container}>
                <View style={styles.imageTextContainer}>
                  <Image
                  style={styles.cameraimage}
                  source={camera}>
                  </Image>
                  <Text style={[styles.textstyle,{fontSize:20,marginTop:30, opacity:0.9}]}>Vintage, luxurious and classic... more than 100 different shooting angles have been set up at the Paris Garden studio in order to give young people the most unique, enriching and interesting experiences. There is also a classic train station, a place for photographers to unleash their creativity...</Text>
                  <Text style={[styles.textstyle,{fontSize:20,marginTop:10, opacity:0.9}]} >Price: 100$</Text>
                </View>
          </View>);
}

const styles = StyleSheet.create({
  textstyle:{
  fontFamily:'branding-sf', 
  letterSpacing:2, 
  color:"white", 
  padding: 10
  },
  ball:{
    position:"absolute",
    backgroundColor:"#FFD44A",
    left:100,
    top:-10,
    borderRadius:50,
    height:50,
    width:50
  },
  container: {
    flex:1,
    backgroundColor:"#FF9933",
  },
  imageTextContainer:{
    flex:8, 
    marginTop:-150,
    justifyContent:"center",
    alignItems:"center"
  },
  buttonContainer:{
    flexDirection: 'row'
  },
  buttonWrap: {
    width: '50%',
    marginBottom: 20
  }
});